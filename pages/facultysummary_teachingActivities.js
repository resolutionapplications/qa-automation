const pageCommands = {
  facultyTeachingActivities(){
    return this
      .waitForElementVisible('@teachingActivitiesLabel')
      .click('@teachingActivitiesLabel')
  },
  facultyTeachingActivitiesAdd() {
    return this
    .waitForElementVisible('@teachingActivitiesAddButton')
    .click('@teachingActivitiesAddButton')
  },
  facultyTeachingActivitiesAddBanner() {
    return this
    .waitForElementVisible('@teachingActivitiesAddBannerButton')
    .click('@teachingActivitiesAddBannerButton')
  },
  facultyTeachingActivitiesAddContent() {
    return this
    .waitForElementVisible('@typeTeaching')
    .click('@typeTeaching')
    .waitForElementVisible('@typeTeachingBanner')
    .click('@typeTeachingBanner')
    .click('@nextTeachingButton')
    .waitForElementVisible('@roleTeaching')
    .click('@roleTeaching')
    .waitForElementVisible('@roleTeachingPreceptor')
    .click('@roleTeachingPreceptor')
    .clearValue('@teachingActivitiesStartDate')
    .setValue('@teachingActivitiesStartDate',"01/01/2000")
    .clearValue('@teachingActivitiesEndDate')
    .setValue('@teachingActivitiesEndDate',"01/01/2010")
  },
  facultyTeachingActivitiesAddBannerContent() {
    return this
    .waitForElementVisible('@roleTeaching')
    .click('@roleTeaching')
    .waitForElementVisible('@roleTeachingMentor')
    .click('@roleTeachingMentor')
    .clearValue('@teachingActivitiesStartDate')
    .setValue('@teachingActivitiesStartDate',"01/01/2000")
    .clearValue('@teachingActivitiesEndDate')
    .setValue('@teachingActivitiesEndDate',"01/01/2010")
  },
  removeOneTeachingActivities() {
    return this
      .waitForElementVisible('@removeOneTeachingButton')
      .click('@removeOneTeachingButton')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    teachingActivitiesLabel: {
      selector: "a[href='teaching_activities.html?emplid=RA102153']"
    },
    teachingActivitiesAddButton: {
      selector: 'a[href="teaching_activities_type.html?emplid=RA102153"]'
    },
    teachingActivitiesAddBannerButton: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div[2]/div/div[3]/table/caption/a/img',
      locateStrategy:'xpath'
    },
    typeTeaching: {
      selector: '#type_chosen'
    },
    typeTeachingBanner: {
      selector: "//li[text()='Banner-UMC Phoenix Physician']",
      locateStrategy: 'xpath'
    },
    roleTeaching: {
        selector: '#validRole_chosen'
    },
    roleTeachingPreceptor: {
      selector: "//li[text()='Preceptor']",
      locateStrategy: 'xpath'
    },
    roleTeachingMentor: {
      selector: "//li[text()='Mentor']",
      locateStrategy: 'xpath'
    },
    teachingActivitiesStartDate: {
        selector: 'input[name="start"]'
    },
    teachingActivitiesEndDate: {
        selector: 'input[name="end"]'
    },
    nextTeachingButton: {
      selector: 'input[value="Next"]'
    },
    removeOneTeachingButton: {
      selector: 'img[src="images/icons/delete.png"]'
    }
  }
};
