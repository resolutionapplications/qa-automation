const pageCommands = {
  editResResearchSoftwareTitles() {
    return this
      .waitForElementVisible('@softwareTitlesEditLabel')
      .click('@softwareTitlesEditLabel')
  },
  editResResearchSoftwareTitlesAdd() {
    return this
    .waitForElementVisible('@softwareTitlesAddButton')
    .click('@softwareTitlesAddButton')
  },
  editResResearchSoftwareTitlesAddContent() {
      return this
      .waitForElementVisible('@softwareTitlesTitle')
      .clearValue( '@softwareTitlesTitle')
      .setValue( '@softwareTitlesTitle', "My title")
      .clearValue( '@softwareTitlesDescription')
      .setValue( '@softwareTitlesDescription', "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ")
      .clearValue( '@softwareTitlesInventors')
      .setValue( '@softwareTitlesInventors', "Et harum quidem rerum facilis est et expedita distinctio.")
      .clearValue( '@softwareTitlesDateReleased')
      .setValue( '@softwareTitlesDateReleased', "01/01/2007")
      .clearValue( '@softwareTitlesDistributionList')
      .setValue( '@softwareTitlesDistributionList', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. ")
      .click('@softwareTitlesInternationalCbox')
      .waitForElementVisible('@softwareTitlesInternationalCboxCountry')
      .clearValue( '@softwareTitlesInternationalCboxCountry')
      .setValue( '@softwareTitlesInternationalCboxCountry', "Country")
      .click('@softwareTitlesInterdisciplinaryCbox')
      .click('@softwareTitlesCommunityEngagementCbox')
      .click('@softwareTitlesPublishCbox')
      .click('@softwareTitlesPublishCbox')
  },
  editResResearchSoftwareTitlesSort() {
    return this
      .waitForElementVisible('@softwareTitlesSortButton')
      .click('@softwareTitlesSortButton')
    },
  editResResearchSoftwareTitlesShowHide() {
    return this
      .waitForElementVisible('@softwareTitlesShowHideButton')
      .click('@softwareTitlesShowHideButton')
  },
  editResResearchSoftwareTitlesShowHideSelect() {
    return this
      .waitForElementVisible('@softwareTitlesShowHideBox')
      .click('@softwareTitlesShowHideBox')
    },
  removeOneResearchSoftwareTitles() {
    return this
      .waitForElementVisible('@removeOneResearchSoftwareTitlesButton')
      .click('@removeOneResearchSoftwareTitlesButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    softwareTitlesEditLabel: {
      selector: 'a[href="index.html#software"]'
    },
    softwareTitlesAddButton: {
      selector: 'a[href="/software/edit.html"]'
    },
    softwareTitlesSortButton: {
      selector: 'a[href="/software/sort.html"]'
    },
    softwareTitlesShowHideButton: {
      selector: 'a[href="/software/visible.html"]'
    },
    removeOneResearchSoftwareTitlesButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    //
    softwareTitlesTitle: {
      selector: '#title'
    },
    softwareTitlesDescription: {
      selector: '#description'
    },
    softwareTitlesInventors: {
      selector: '#inventors'
    },
    softwareTitlesDateReleased: {
      selector: '#releaseDate'
    },
    softwareTitlesDistributionList: {
      selector: '#distribution'
    },
    //
    softwareTitlesInternationalCbox: {
      selector: '#checked_0'
    },
    softwareTitlesInternationalCboxCountry: {
      selector: '#value_0'
    },
    softwareTitlesInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    softwareTitlesCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    softwareTitlesPublishCbox: {
      selector: '#displayed1'
    },
    softwareTitlesShowHideBox: {
      selector: 'input[name="list[1].displayed"]'
    }


  }
};
