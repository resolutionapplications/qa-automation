const pageCommands = {
  clickAdditionalInfo(){
    return this
      .waitForElementVisible('@additionalInfoFirstLabel')
      .click('@additionalInfoFirstLabel')
  },
  fillAdditionalInfo() {
    return this
      .waitForElementVisible('@question1')
      .click('@question1')
      .waitForElementVisible('@question2')
      .click('@question2')
  },
  checkIsAdditionalInfoOpen() {
    return this
      .waitForElementVisible('@appAdditionalInfoLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    additionalInfoFirstLabel: {
      selector: "a[href='additional_information.html']"
    },
    appAdditionalInfoLabel: {
      selector: "//h2[text()='Additional Information']",
      locateStrategy: 'xpath'
    },
    question1: {
      selector: '#content_container > div > div:nth-child(2) > div > div > div > form > div.enclose > table > tbody > tr:nth-child(1) > td:nth-child(1) > label:nth-child(2) > input[type="radio"]'
    },
    question2: {
      selector: '#content_container > div > div:nth-child(2) > div > div > div > form > div.enclose > table > tbody > tr:nth-child(2) > td:nth-child(1) > label:nth-child(2) > input[type="radio"]'
    },



  }
};
