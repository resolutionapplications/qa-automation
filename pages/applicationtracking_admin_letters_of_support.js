const pageCommands = {
  clickLettersOfSupport(){
    return this
      .waitForElementVisible('@letterOfSupportFirstLabel')
      .click('@letterOfSupportFirstLabel')
  },
    fillLettersOfSupportReviewer1() {
     this
      .waitForElementVisible('@letterOfSupportReviewer1Name')
      .clearValue('@letterOfSupportReviewer1Name')
      .setValue('@letterOfSupportReviewer1Name',"Namen")
      .clearValue('@letterOfSupportReviewer1Title')
      .setValue('@letterOfSupportReviewer1Title',"My title")
      .waitForElementVisible('@letterOfSupportReviewer1Rank')
      .click('@letterOfSupportReviewer1Rank')
      .waitForElementVisible('@letterOfSupportReviewer1RankAssistant')
      .click('@letterOfSupportReviewer1RankAssistant')
      .waitForElementVisible('@letterOfSupportReviewer1Email')
      .clearValue('@letterOfSupportReviewer1Email')
      .setValue('@letterOfSupportReviewer1Email',"My@gmail.com")
      .clearValue('@letterOfSupportReviewer1Phone')
      .setValue('@letterOfSupportReviewer1Phone',"061-061-0611")
      .clearValue('@letterOfSupportReviewer1Institution')
      .setValue('@letterOfSupportReviewer1Institution',"My institution")
      this.api.pause(2000);
      this.waitForElementVisible('@letterOfSupportReviewer1Degree')
      .click('@letterOfSupportReviewer1Degree')
      .click('@letterOfSupportReviewer1Degree')
      .waitForElementVisible('@letterOfSupportReviewerDegreeAA')
      .click('@letterOfSupportReviewerDegreeAA')
      return this;
  },
    fillLettersOfSupportReviewer2() {
     return this
      .waitForElementVisible('@letterOfSupportReviewer2Name')
      .clearValue('@letterOfSupportReviewer2Name')
      .setValue('@letterOfSupportReviewer2Name',"Namen")
      .clearValue('@letterOfSupportReviewer2Title')
      .setValue('@letterOfSupportReviewer2Title',"My title")
      .waitForElementVisible('@letterOfSupportReviewer2Rank')
      .click('@letterOfSupportReviewer2Rank')
      .waitForElementVisible('@letterOfSupportReviewer2RankAssociate')
      .click('@letterOfSupportReviewer2RankAssociate')
      .waitForElementVisible('@letterOfSupportReviewer2Email')
      .clearValue('@letterOfSupportReviewer2Email')
      .setValue('@letterOfSupportReviewer2Email',"My@gmail.com")
      .clearValue('@letterOfSupportReviewer2Phone')
      .setValue('@letterOfSupportReviewer2Phone',"061-061-0611")
      .clearValue('@letterOfSupportReviewer2Institution')
      .setValue('@letterOfSupportReviewer2Institution',"My institution")
      .waitForElementVisible('@letterOfSupportReviewer2Degree')
      .click('@letterOfSupportReviewer2Degree')
      .waitForElementVisible('@letterOfSupportReviewer2DegreeAA')
      .click('@letterOfSupportReviewer2DegreeAA')
      
  },
    fillLettersOfSupportReviewer3() {
     return this
      .waitForElementVisible('@letterOfSupportReviewer3Name')
      .clearValue('@letterOfSupportReviewer3Name')
      .setValue('@letterOfSupportReviewer3Name',"Namen")
      .clearValue('@letterOfSupportReviewer3Title')
      .setValue('@letterOfSupportReviewer3Title',"My title")
      .waitForElementVisible('@letterOfSupportReviewer3Rank')
      .click('@letterOfSupportReviewer3Rank')
      .waitForElementVisible('@letterOfSupportReviewer3RankProfessor')
      .click('@letterOfSupportReviewer3RankProfessor')
      .waitForElementVisible('@letterOfSupportReviewer3Email')
      .clearValue('@letterOfSupportReviewer3Email')
      .setValue('@letterOfSupportReviewer3Email',"My@gmail.com")
      .clearValue('@letterOfSupportReviewer3Phone')
      .setValue('@letterOfSupportReviewer3Phone',"061-061-0611")
      .clearValue('@letterOfSupportReviewer3Institution')
      .setValue('@letterOfSupportReviewer3Institution',"My institution")
      .waitForElementVisible('@letterOfSupportReviewer3Degree')
      .click('@letterOfSupportReviewer3Degree')
      .waitForElementVisible('@letterOfSupportReviewer3DegreeAA')
      .click('@letterOfSupportReviewer3DegreeAA')
      
  },
  checkIsLetterOfSupportOpen() {
    return this
      .waitForElementVisible('@appLettersOfSupportLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    letterOfSupportFirstLabel: {
      selector: "a[href='letters_of_support.html']"
    },
    appLettersOfSupportLabel: {
      selector: "//h2[text()='Letters of Support']",
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer1Name: {
      selector: '#name'
    },
    letterOfSupportReviewer1Title: {
      selector: '#title'
    },
    letterOfSupportReviewer1Rank: {
      selector: '#rank_chosen'
    },
    letterOfSupportReviewer1RankAssistant: {
      selector: "//li[text()='Assistant Professor']",
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer1Email: {
      selector: '#email'
    },
    letterOfSupportReviewer1Phone: {
      selector: '#phone'
    },
    letterOfSupportReviewer1Institution: {
      selector: '#institution'
    },
    letterOfSupportReviewer1Degree: {
      selector: '#degree_chosen'
    },
    letterOfSupportReviewerDegreeAA: {
      selector: "//li[text()='AA']",
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer2Name: {
      selector: 'input[name="los[1].ext.name"]'
    },
    letterOfSupportReviewer2Title: {
      selector: 'input[name="los[1].ext.title"]'
    },
    letterOfSupportReviewer2Rank: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[2]/table/tbody/tr[3]/td/div',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer2RankAssociate: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[2]/table/tbody/tr[3]/td/div/div/ul/li[2]',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer2Email: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[2]/table/tbody/tr[4]/td/input',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer2Phone: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[2]/table/tbody/tr[5]/td/input',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer2Institution: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[2]/table/tbody/tr[6]/td/input',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer2Degree: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[2]/table/tbody/tr[7]/td/div',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer2DegreeAA: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[2]/table/tbody/tr[7]/td/div/div/ul/li[2]',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer3Name: {
      selector: 'input[name="los[2].ext.name"]'
    },
    letterOfSupportReviewer3Title: {
      selector: 'input[name="los[2].ext.title"]'
    },
    letterOfSupportReviewer3Rank: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[3]/table/tbody/tr[3]/td/div',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer3RankProfessor: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[3]/table/tbody/tr[3]/td/div/div/ul/li[3]',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer3Email: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[3]/table/tbody/tr[4]/td/input',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer3Phone: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[3]/table/tbody/tr[5]/td/input',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer3Institution: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[3]/table/tbody/tr[6]/td/input',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer3Degree: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[3]/table/tbody/tr[7]/td/div',
      locateStrategy: 'xpath'
    },
    letterOfSupportReviewer3DegreeAA: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/div[3]/table/tbody/tr[7]/td/div/div/ul/li[2]',
      locateStrategy: 'xpath'
    },
    
  }
};
