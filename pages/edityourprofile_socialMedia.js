const pageCommands = {
  editSocialMediaAdd() {
    return this
      .waitForElementVisible('@socialMediaAddButton')
      .click('@socialMediaAddButton')
    },
    editSocialMediaAddContent() {
    return this
      .waitForElementVisible('@socialMediaType')
      .click('@socialMediaType')
      .waitForElementVisible('@socialMediaTypeFacebook')
      .click('@socialMediaTypeFacebook')
      .waitForElementVisible('@socialMediaUrl')
      .setValue('@socialMediaUrl',"http://www.url.com")
    },
  editSocialMediaDelete() {
    return this
      .waitForElementVisible('@socialMediaDeleteButton')
      .click('@socialMediaDeleteButton')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    socialMediaAddButton: {
      selector: 'a[href="/iconlink/edit.html"]'
    },
    socialMediaDeleteButton: {
      selector: '#leftcol > div > ul:nth-child(5) > li > p > a:nth-child(3) > img'
    },
    socialMediaType: {
      selector: '#type'
    },
    socialMediaTypeFacebook: {
      selector: "//option[text()='Facebook']",
      locateStrategy: 'xpath'
    },
    socialMediaUrl: {
      selector: '#url'
    }
  }
};
