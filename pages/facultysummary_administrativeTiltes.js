const pageCommands = {
  facultyAdministrativeTitles() {
    return this
      .waitForElementVisible('@administrativeTitlesLabel')
      .click('@administrativeTitlesLabel')
  },
  facultyAdministrativeTitlesAdd() {
    return this
    .waitForElementVisible('@administrativeTitlesAddButton')
    .click('@administrativeTitlesAddButton')
  },
  facultyAdministrativeTitlesAddContent() {
      return this
      .waitForElementVisible('@administrativeTitlesLevel')
      .click( '@administrativeTitlesLevel')
      .waitForElementVisible( '@administrativeTitlesLevelDepartment')
      .click( '@administrativeTitlesLevelDepartment')
      .waitForElementVisible( '@administrativeTitlesDepartment')
      .click( '@administrativeTitlesDepartment')
      .waitForElementVisible( '@administrativeTitlesDepartmentDoA')
      .click( '@administrativeTitlesDepartmentDoA')
      .waitForElementVisible( '@administrativeTitlesTitle')
      .click( '@administrativeTitlesTitle')
      .waitForElementVisible( '@administrativeTitlesTitleDean')
      .click( '@administrativeTitlesTitleDean')
      .clearValue( '@administrativeTitlesEffective')
      .setValue( '@administrativeTitlesEffective', "01/01/2000")
      .clearValue('@administrativeTitlesDescription')
      .setValue('@administrativeTitlesDescription',"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
      .clearValue( '@administrativeTitlesEnd')
      .setValue('@administrativeTitlesEnd',"01/01/2010")
  },
  removeOneAdministrativeTitles() {
    return this
      .waitForElementVisible('@removeOneAdministrativeTitlesButton')
      .click('@removeOneAdministrativeTitlesButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    administrativeTitlesLabel: {
      selector: 'a[href="title.html?emplid=RA102153"]'
    },
    administrativeTitlesAddButton: {
      selector: 'a[href="title_edit.html?emplid=RA102153"]'
    },
    administrativeTitlesLevel: {
      selector: '#level_id_chosen'
    },
    administrativeTitlesLevelDepartment: {
      selector: "//li[text()='Department']",
      locateStrategy: 'xpath'
    },
     administrativeTitlesDepartment: {
      selector: '#department_id_chosen'
    },
    administrativeTitlesDepartmentDoA: {
      selector: "//li[text()='Department of Anesthesiology']",
      locateStrategy: 'xpath'
    },
     administrativeTitlesTitle: {
      selector: '#title_id_chosen'
    },
     administrativeTitlesTitleDean: {
       selector: "//li[text()='Dean']",
      locateStrategy: 'xpath'
    },
   administrativeTitlesEffective: {
      selector: '#effective'
    },
    administrativeTitlesDescription: {
      selector: '#description'
    },
    administrativeTitlesEnd: {
      selector: '#end'
    },
     removeOneAdministrativeTitlesButton: {
      selector: 'img[src="images/icons/delete.png"]'
    }

  }
};
