const pageCommands = {
  editMentoring() {
    return this
    .waitForElementVisible('@mentoringEditLabel')
    .click('@mentoringEditLabel')
  },
  editMentoringAdd() {
    return this
    .waitForElementVisible('@mentoringAddButton')
    .click('@mentoringAddButton')
  },
  editMentoringAddContent() {
      return this
      .waitForElementVisible('@mentoringMentoringActivity')
      .clearValue('@mentoringMentoringActivity')
      .setValue('@mentoringMentoringActivity',"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.")
      .clearValue( '@mentoringStartDate')
      .setValue( '@mentoringStartDate', "01/01/2000")
      .clearValue( '@mentoringEndDate')
      .setValue( '@mentoringEndDate', "01/01/2020")
      .clearValue('@mentoringHours')
      .setValue('@mentoringHours',"6")
      .click('@mentoringInternationalCbox')
      .waitForElementVisible('@mentoringInternationalCboxCountry')
      .clearValue( '@mentoringInternationalCboxCountry')
      .setValue( '@mentoringInternationalCboxCountry', "Country")
      .click('@mentoringInterdisciplinaryCbox')
      .click('@mentoringCommunityEngagementCbox')
      .click('@mentoringPublishCbox')
      .click('@mentoringPublishCbox')

  },
  editMentoringSort() {
    return this
      .waitForElementVisible('@mentoringSortButton')
      .click('@mentoringSortButton')
    },
  editMentoringShowHide() {
    return this
      .waitForElementVisible('@mentoringShowHideButton')
      .click('@mentoringShowHideButton')
  },
  editMentoringShowHideSelect() {
    return this
      .waitForElementVisible('@mentoringShowHideBox')
      .click('@mentoringShowHideBox')
    },
  removeOneMentoring() {
    return this
      .waitForElementVisible('@removeOneMentoringButton')
      .click('@removeOneMentoringButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    mentoringEditLabel: {
      selector:'a[href="index.html#mentor"]'
    },
    mentoringAddButton: {
      selector: 'a[href="/mentor/edit.html"]'
    },
    mentoringSortButton: {
      selector: 'a[href="/mentor/sort.html"]'
    },
    mentoringShowHideButton: {
      selector: 'a[href="/mentor/visible.html"]'
    },
    mentoringMentoringActivity: {
      selector: "#servicePerformed"
    },
    mentoringStartDate: {
      selector: '#startDate'
    },
    mentoringEndDate: {
      selector: '#endDate'
    },
    mentoringHours: {
      selector: '#hours'
    },
    mentoringInternationalCbox: {
      selector: '#checked_0'
    },
    mentoringInternationalCboxCountry: {
      selector: '#value_0'
    },
    mentoringInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    mentoringCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    mentoringPublishCbox: {
      selector: '#displayed1'
    },
    mentoringShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
    removeOneMentoringButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
