const pageCommands = {
  facultyAcademicSummary() {
    return this
      .waitForElementVisible('@facultyAcademicSummaryLabel')
      .click('@facultyAcademicSummaryLabel')
  },
  facultyAcademicSummaryAddContent() {
    return this
      .waitForElementVisible('@facultyAcademicSummaryContent')
      .clearValue('@facultyAcademicSummaryContent')
      .setValue('@facultyAcademicSummaryContent',"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
      
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    facultyAcademicSummaryLabel: {
      selector: 'a[href="academic_summary.html?emplid=RA102153"]'
    },
    facultyAcademicSummaryContent: {
      selector: '#summary'
    },
     resSummarySaveButton: {
      selector: 'input[type=submit]'
    }
  }
};
