const pageCommands = {
  userIDs(){
    return this
      .waitForElementVisible('@userIDsLabel')
      .click('@userIDsLabel')
  },
  facultyUserIDsAdd() {
    return this
    .waitForElementVisible('@userIDsAddButton')
    .click('@userIDsAddButton')
  },
  facultyUserIDsAddContent() {
    return this
    .waitForElementVisible('@userIDsSingle')
    .clearValue('@userIDsSingle')
    .setValue('@userIDsSingle',"Single ID")
    .clearValue('@userIDsUPIN')
    .setValue('@userIDsUPIN',"12345678")
    .clearValue('@userIDsJobber')
    .setValue('@userIDsJobber',"PMC1234567")
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    userIDsLabel: {
      selector: "a[href='config_user_id.html?emplid=RA102153']"
    },
    userIDsAddButton: {
      selector: 'a[href="config_user_id_edit.html?emplid=RA102153"]'
    },
    userIDsSingle: {
        selector: '#ssoID'
    },
    userIDsUPIN: {
        selector: 'input[name="userIDs[0].typeValue"]'
    },
    userIDsJobber: {
        selector: 'input[name="userIDs[1].typeValue"]'
    },
  }
};
