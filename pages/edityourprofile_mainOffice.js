const pageCommands = {
  editMainOffice() {
    return this
    .waitForElementVisible('@mainOfficeEditLabel')
    .click('@mainOfficeEditLabel')
  },
  editMainOfficeAdd() {
    return this
    .waitForElementVisible('@mainOfficeEditButton')
    .click('@mainOfficeEditButton')
  },
  editMainOfficeAddContent() {
      return this
      .waitForElementVisible('@mainOfficeBuildingNumber')
      .click('@mainOfficeBuildingNumber')
      .waitForElementVisible('@mainOfficeBuildingNumberChipeta')
      .click('@mainOfficeBuildingNumberChipeta')
      .clearValue('@mainOfficeRoom')
      .setValue('@mainOfficeRoom',"My room")
      .clearValue( '@mainOfficePhone')
      .setValue( '@mainOfficePhone', "061-061-0610")
      .clearValue( '@mainOfficeFax')
      .setValue( '@mainOfficeFax', "777-888-999")
      .clearValue('@mainOfficeSimpleSchedule')
      .setValue('@mainOfficeSimpleSchedule',"My simple Schedule")

  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    mainOfficeEditLabel: {
      selector:'a[href="index.html#office"]'
    },
    mainOfficeEditButton: {
      selector: 'a[href="/office/edit.html"]'
    },
    mainOfficeBuildingNumber: {
      selector: 'select[name="building.number"]'
    },
    mainOfficeBuildingNumberChipeta: {
      selector: "//option[text()='295 Chipeta Way']",
      locateStrategy: 'xpath'
    },
    mainOfficeRoom: {
      selector: '#room'
    },
   mainOfficePhone: {
      selector: "#phone"
    },
    mainOfficeFax: {
      selector: '#fax'
    },
    mainOfficeSimpleSchedule: {
      selector: '#simpleSchedule'
    }
  }
};
