const pageCommands = {
  facultySignificantAcc(){
    return this
      .waitForElementVisible('@significantAccLabel')
      .click('@significantAccLabel')
  },
  facultySignificantAccAdd() {
    return this
    .waitForElementVisible('@significantAccAddButton')
    .click('@significantAccAddButton')
  },
  facultySignificantAccAddContent() {
    return this
      .waitForElementVisible('@titleSignificantAcc')
      .clearValue('@titleSignificantAcc')
      .setValue('@titleSignificantAcc',"My title")
      .clearValue('@descriptionSignificantAcc')
      .setValue('@descriptionSignificantAcc',`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam ac sem id dapibus. Donec ut vehicula leo. Nulla auctor mi velit, non suscipit justo semper at.`)
      .clearValue('@dateSignificantAcc')
      .setValue('@dateSignificantAcc',"11/01/2000")

  },
  removeOneSignificantAcc() {
    return this
      .waitForElementVisible('@removeOneSignificantAccButton')
      .click('@removeOneSignificantAccButton')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    significantAccLabel: {
      selector: "a[href='accomplishments.html?emplid=RA102153']"
    },
    significantAccAddButton: {
      selector: 'a[href="accomplishment_edit.html?emplid=RA102153"]'
    },
    titleSignificantAcc: {
      selector: '#title'
    },
    descriptionSignificantAcc: {
      selector: '#description'
    },
    dateSignificantAcc: {
      selector: '#date'
    },

    removeOneSignificantAccButton: {
      selector: 'img[src="images/icons/delete.png"]'
    }


  }
};
