const pageCommands = {
  facultyDemographics(){
    return this
      .waitForElementVisible('@demographicsLabel')
      .click('@demographicsLabel')
  },
  facultyDemographicsEditPersonal() {
    return this
    .waitForElementVisible('@demographicsEditPersonalButton')
    .click('@demographicsEditPersonalButton')
  },
  facultyDemographicsEditCitizenship() {
    return this
    .waitForElementVisible('@demographicsEditCitizenshipButton')
    .click('@demographicsEditCitizenshipButton')
  },
  facultyDemographicsAddName() {
    return this
    .waitForElementVisible('@demographicsAddNameButton')
    .click('@demographicsAddNameButton')
  },
  facultyDemographicsAddContentName() {
    return this
      .waitForElementVisible('@demographicsType')
      .click('@demographicsType')
      .waitForElementVisible('@demographicsTypePrevious')
      .click('@demographicsTypePrevious')
      .clearValue('@demographicsFirstName')
      .setValue('@demographicsFirstName',"Namen")
      .clearValue('@demographicsMiddleName')
      .setValue('@demographicsMiddleName',"Middlem")
      .clearValue('@demographicsLastName')
      .setValue('@demographicsLastName',"Surnames")
  },
  facultyDemographicsEditPersonalContent() {
    return this
      .clearValue('@demographicsDateOfBirth')
      .setValue('@demographicsDateOfBirth',"11/06/1991")
      .waitForElementVisible('@demographicsGender')
      .click('@demographicsGender')
      .waitForElementVisible('@demographicsGenderMale')
      .click('@demographicsGenderMale')
      .waitForElementVisible('@demographicsEthnicity')
      .click('@demographicsEthnicity')
      .waitForElementVisible('@demographicsEthnicityAmerican')
      .click('@demographicsEthnicityAmerican')
      .waitForElementVisible('@demographicsHighestDegree')
      .click('@demographicsHighestDegree')
      .waitForElementVisible('@demographicsHighestDegreeDoctorate')
      .click('@demographicsHighestDegreeDoctorate')
  },
  facultyDemographicsEditCitizenshipContent() {
    return this
      .waitForElementVisible('@demographicsPermanentResident')
      .click('@demographicsPermanentResident')
      .waitForElementVisible('@demographicsCountry')
      .click('@demographicsCountry')
      .waitForElementVisible('@demographicsCountryBosnia')
      .click('@demographicsCountryBosnia')
      .click('@demographicsPreformingServices')
      .click('@demographicsPreformingServices')
      .click('@demographicsVisaType')
      .waitForElementVisible('@demographicsVisaTypeH1')
      .click('@demographicsVisaTypeH1')
      .clearValue('@demographicsVisaStatusDate')
      .setValue('@demographicsVisaStatusDate',"01/01/2000")
      .clearValue('@demographicsVisaExpiration')
      .setValue('@demographicsVisaExpiration',"01/01/2020")
  },
  facultyDemographicsNames() {
    return this
      .waitForElementVisible('@demographicsNamesButton')
      .click('@demographicsNamesButton')
  },
  facultyDemographicsBack() {
    return this
      .waitForElementVisible('@demographicsBackButton')
      .click('@demographicsBackButton')
  },
  removeOneDemographics() {
    return this
      .waitForElementVisible('@removeOneDemographicsButton')
      .click('@removeOneDemographicsButton')
  },
  facultyDemographicsSave() {
    return this
      .waitForElementVisible('@saveButton')
      .click('@saveButton')
  },
  facultyDemographicsCancel() {
    return this
      .waitForElementVisible('@cancelButton')
      .click('@cancelButton')
  }

};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
   demographicsLabel: {
      selector: "a[href='demographics.html?emplid=RA102153']"
    },
    demographicsAddNameButton: {
      selector: "a[href='demographics_name_edit.html?emplid=RA102153']"
    },
    demographicsEditPersonalButton: {
      selector:'a[href="demographics_personal_edit.html?emplid=RA102153"]'
    },
    demographicsEditCitizenshipButton: {
      selector:'a[href="demographics_citizenship_edit.html?emplid=RA102153"]'
    },
    demographicsType: {
      selector: '#type_id_chosen'
    },
    demographicsTypePrevious: {
      selector: "//li[text()='PREVIOUS']",
      locateStrategy: 'xpath'
    },
    demographicsFirstName: {
      selector: '#first'
    },
    demographicsMiddleName: {
      selector: '#middle'
    },
    demographicsLastName: {
      selector: '#last'
    },
    demographicsDateOfBirth: {
      selector: '#dateOfBirth'
    },
    demographicsGender: {
      selector: '#gender_id_chosen'
    },
    demographicsGenderMale: {
      selector: "//li[text()='Male']",
      locateStrategy: 'xpath'
    },
    demographicsEthnicity: {
      selector: '#ethnicity_id_chosen'
    },
    demographicsEthnicityAmerican: {
      selector: "//li[text()='American Indian or Alaska Native (not Hispanic or Latino)']",
      locateStrategy: 'xpath'
    },
    demographicsHighestDegree: {
      selector: '#highestDegree_id_chosen'
    },
    demographicsHighestDegreeDoctorate: {
      selector: "//li[text()='Doctorate (Academic)']",
      locateStrategy: 'xpath'
    },
    demographicsPermanentResident: {
      selector: '#permanentResident2'
    },
    demographicsCountry: {
      selector: '#country_select_chosen'
    },
    demographicsCountryBosnia: {
      selector: "//li[text()='Bosnia and Herzegovina']",
      locateStrategy: 'xpath'
    },
    demographicsPreformingServices: {
      selector: '#performingServices1'
    },
    demographicsVisaType: {
      selector: 'select[name="visaType.id"]'
    },
    demographicsVisaTypeH1: {
      selector: "//option[text()='H1']",
      locateStrategy: 'xpath'
    },
    demographicsVisaStatusDate: {
      selector: '#visaStatus'
    },
    demographicsVisaExpiration: {
      selector: '#visaExpire'
    },
   demographicsNamesButton: {
      selector: 'a[href="demographics_names.html?emplid=RA102153"]'
    },
    demographicsBackButton: {
      selector:'input[value="Back"]'
    },
    removeOneDemographicsButton: {
      selector:'img[src="images/icons/delete.png"]'
    },
    cancelButton: {
      selector:'#content_container > div > div:nth-child(2) > div > div.content-padder > div > form > div.button-bar.buffer-bottom > input[type="button"]:nth-child(2)'
    },
    saveButton: {
      selector:'#content_container > div > div:nth-child(2) > div > div.content-padder > div > form > div.button-bar.buffer-bottom > input[type="submit"]:nth-child(1)'
    }

  }
};
