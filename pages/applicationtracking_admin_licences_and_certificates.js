const pageCommands = {
  clickLicencesCertificates(){
    return this
      .waitForElementVisible('@licencesCertificatesFirstLabel')
      .click('@licencesCertificatesFirstLabel')
  },
  plusLicencesCertificates() {
    return this
    .waitForElementVisible('@licencesCertificatesEditButton')
    .click('@licencesCertificatesEditButton')
  },
  fillLicencesCertificates() {
    return this
    .waitForElementVisible('@typeLicences')
    .click('@typeLicences')
    .waitForElementVisible('@typeLicencesCertificates')
    .click('@typeLicencesCertificates')
    .clearValue('@titleLicences')
    .setValue('@titleLicences',"My licence title")
    .clearValue('@issuerLicences')
    .setValue('@issuerLicences',"My issuer")
    .clearValue('@stateLicences')
    .setValue('@stateLicences',"My state")
    .click('@countryLicences')
    .waitForElementVisible('@countryLicencesUS')
    .click('@countryLicencesUS')
    .clearValue('@licenceNumber')
    .setValue('@licenceNumber',"4564846")
    .clearValue('@dateIssuedLicences')
    .setValue('@dateIssuedLicences',"11/01/2010")
    .clearValue('@expirationDateLicences')
    .setValue('@expirationDateLicences',"11/01/2019")
    .click('@licenseStatusLicences')
    .waitForElementVisible('@licenseStatusLicencesCurrent')
    .click('@licenseStatusLicencesCurrent')
    .click('@fieldLicences')
    .waitForElementVisible('@fieldLicencesAcu')
    .click('@fieldLicencesAcu')
  },
  cancelLicencesCertificates() {
    return this
      .waitForElementVisible('@cancelLicencesCertificatesButton')
      .click('@cancelLicencesCertificatesButton')
  },
   removeOneLicencesCertificates() {
    return this
      .waitForElementVisible('@removeOneLicencesCertificatesButton')
      .click('@removeOneLicencesCertificatesButton')
  },
  saveLicencesCertificates() {
    return this
      .waitForElementVisible('@saveLicencesCertificatesButton')
      .click('@saveLicencesCertificatesButton')
  },
  saveAndAddLicencesCertificates() {
    return this
      .waitForElementVisible('@saveAndAddLicencesCertificatesButton')
      .click('@saveAndAddLicencesCertificatesButton')
  },

  checkIsLicencesCertificatesOpen() {
    return this
      .waitForElementVisible('@appLicencesCertificatesLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    licencesCertificatesFirstLabel: {
      selector: "a[href='licenses_and_certs.html']"
    },
    appLicencesCertificatesLabel: {
      selector: "//h2[text()='Licenses and Certificates']",
      locateStrategy: 'xpath'
    },
    licencesCertificatesEditButton: {
      selector: 'a[href="license_and_cert_edit.html"]'
    },
    typeLicences: {
      selector: '#type_id_chosen'
    },
    typeLicencesCertificates: {
      selector: "//li[text()='Consulting Matrix National Certificate']",
      locateStrategy: 'xpath'
    },
    titleLicences: {
      selector: '#title'
    },
    issuerLicences: {
      selector: '#issuer'
    },
    stateLicences: {
      selector: '#state'
    },
    countryLicences: {
      selector: '#country_id_chosen'
    },
    countryLicencesUS: {
      selector: "//li[text()='United States']",
      locateStrategy: 'xpath'
    },
    licenceNumber: {
      selector: '#number'
    },
    dateIssuedLicences: {
      selector: '#issued'
    },
    expirationDateLicences: {
      selector: '#expired'
    },
    licenseStatusLicences: {
      selector: '#status_id_chosen'
    },
    licenseStatusLicencesCurrent: {
      selector: "//li[text()='Current']",
      locateStrategy: 'xpath'
    },
    fieldLicences: {
      selector: '#field_id_chosen'
    },
    fieldLicencesAcu: {
      selector: "//li[text()='Acupuncturist']",
      locateStrategy: 'xpath'
    },
    verifiedLicences: {
      selector: "input[id='verified1']"
    },
     removeOneLicencesCertificatesButton: {
       selector: 'img[src="../images/icons/delete.png"]'
     },
    cancelLicencesCertificatesButton: {
      selector: 'input[value="Cancel"]'
    },
    saveLicencesCertificatesButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddLicencesCertificatesButton: {
      selector: 'input[value="Save & Add Another"]'
    },


  }
};
