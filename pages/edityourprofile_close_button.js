const pageCommands = {
  editProfileClose() {
    return this
      .waitForElementVisible('@closeButton')
      .click('@closeButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    closeButton: {
      selector: '#cboxClose'
    },
  }
};
