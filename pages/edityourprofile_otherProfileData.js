const pageCommands = {
  editServiceOther() {
    return this
      .waitForElementVisible('@serviceOtherEditLabel')
      .click('@serviceOtherEditLabel')
  },
  editServiceOtherAdd() {
    return this
      .waitForElementVisible('@serviceOtherEditContentButton')
      .click('@serviceOtherEditContentButton')
    },
  editServiceOtherAddContent() {
      return this
        .waitForElementVisible('@serviceOtherTitle')
        .clearValue('@serviceOtherTitle')
        .setValue('@serviceOtherTitle',"Other (Service)")
  },
  editServiceOtherDelete() {
    return this
      .waitForElementVisible('@serviceOtherDeleteContentButton')
      .click('@serviceOtherDeleteContentButton')
    },

};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
   serviceOtherEditLabel: {
      selector: 'a[href="index.html#profileotherservice"]'
    },
    serviceOtherEditContentButton: {
      selector: 'a[href="/profileotherservice/edit.html"]'
    },
    serviceOtherDeleteContentButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    serviceOtherTitle: {
      selector: '#title'
    }
  }
};
