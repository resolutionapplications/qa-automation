const pageCommands = {
  facultySearchString() {
    return this
      .waitForElementVisible('@facultySearchStringLabel')
      .click('@facultySearchStringLabel')
  },
  facultySearchStringAdd() {
    return this
      .waitForElementVisible('@facultySearchStringAddButton')
      .click('@facultySearchStringAddButton')
  },
  facultySearchStringAddContent() {
    return this
      .waitForElementVisible('@facultySearchString')
      .clearValue('@facultySearchString')
      .setValue('@facultySearchString',"Lorem ipsum dolor sit amet, consectetur adipiscing elit")
      .click('@facultySearchStringActive')
  },
  removeOneSearchString() {
    return this
      .waitForElementVisible('@removeOneSearchStringButton')
      .click('@removeOneSearchStringButton')
  },
  saveAndAddAnother() {
    return this
      .waitForElementVisible('@saveAndAddAnotherButton')
      .click('@saveAndAddAnotherButton')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    facultySearchStringLabel: {
      selector: 'a[href="search_strings.html?emplid=RA102153"]'
    },
    facultySearchStringAddButton: {
      selector: 'a[href="search_strings_edit.html?emplid=RA102153"]'
    },
    facultySearchString: {
      selector: '#string'
    },
    facultySearchStringActive: {
      selector: '#active'
    },
    removeOneSearchStringButton: {
      selector:'img[src="images/icons/delete.png"]'
    },
    saveAndAddAnotherButton: {
      selector:'input[name="_another"]'
    }
  }
};
