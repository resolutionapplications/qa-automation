const pageCommands = {
  clickSiteSettings(){
    return this
      .waitForElementVisible('@siteSettingsFirstLabel')
      .click('@siteSettingsFirstLabel')
  },
  fillSiteSettings() {
    return this
      .waitForElementVisible('@academicMedicine')
      .click('@academicMedicine')
      .click('@accomodateStudents')
      .click('@clinicHoursSun')
      .click('@clinicHoursMon')      
      .click('@clinicHoursTue')
      .click('@clinicHoursWed')
      .click('@clinicHoursThu')
      .click('@clinicHoursFri')
      .click('@clinicHoursSat')
  },
  checkIsSiteSettingsOpen() {
    return this
      .waitForElementVisible('@appSiteSettings')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    siteSettingsFirstLabel: {
      selector: "a[href='site_setting.html']"
    },
    appSiteSettings: {
      selector: "//h1[text()='Site Setting']",
      locateStrategy: 'xpath'
    },
    academicMedicine: {
      selector: 'input[value="118064"]'
    },
    accomodateStudents: {
      selector: '#accomodateStudents1'
    },
    clinicHoursSun: {
      selector: 'input[name="days[0].morning"]'
    },
    clinicHoursMon: {
      selector: 'input[name="days[1].morning"]'
    },
    clinicHoursTue: {
      selector: 'input[name="days[2].morning"]'
    },
    clinicHoursWed: {
      selector: 'input[name="days[3].morning"]'
    },
    clinicHoursThu: {
      selector: 'input[name="days[4].morning"]'
    },
    clinicHoursFri: {
      selector: 'input[name="days[5].morning"]'
    },
    clinicHoursSat: {
      selector: 'input[name="days[6].morning"]'
    },




  }
};
