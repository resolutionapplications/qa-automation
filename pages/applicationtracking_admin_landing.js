const pageCommands = {
  clickApp() {
     this
      .waitForElementVisible('@workflow')
      .click('@workflow')
      this.api.pause(2000);
      this.waitForElementVisible('@workflowAudit')
      .click('@workflowAudit')
      .waitForElementVisible('@appLink')
      .click('@appLink')
      return this;
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    appLink: {
      selector: "//a[text()='RA102153']",
      locateStrategy: 'xpath'
    },
    workflow: {
      selector: "#out_workflow_chosen"
    },
    workflowAudit: {
      selector: "//li[text()='Audit Faculty']",
      locateStrategy: 'xpath'
    },
  }
};
