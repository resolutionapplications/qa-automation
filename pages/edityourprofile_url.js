const pageCommands = {
  editURL() {
    return this
      .waitForElementVisible('@urlEditButton')
      .click('@urlEditButton')
  },
  editURLDeleteAlias() {
    return this
      .waitForElementVisible('@urlRemoveAliasCbox')
      .click('@urlRemoveAliasCbox')
  },
  editURLAlias() {
    return this
      .waitForElementVisible('@urlAlias')
      .clearValue('@urlAlias')
      .setValue('@urlAlias',"tyler")
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    urlEditButton: {
      selector: 'a[href="/alias/edit.html"]'
    },
    urlRemoveAliasCbox: {
      selector: '#removeAlias'
    },
    urlAlias: {
      selector: '#alias'
    },
  }
};
