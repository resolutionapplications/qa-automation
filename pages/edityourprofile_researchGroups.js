const pageCommands = {
  editResResearchGroups() {
    return this
      .waitForElementVisible('@researchGroupsEditLabel')
      .click('@researchGroupsEditLabel')
  },
  editResResearchGroupsAdd() {
    return this
    .waitForElementVisible('@researchGroupsAddButton')
    .click('@researchGroupsAddButton')
  },
  editResResearchGroupsAddContent() {
      return this
      .waitForElementVisible('@researchGroupsGroupMemberName')
      .clearValue( '@researchGroupsGroupMemberName')
      .setValue( '@researchGroupsGroupMemberName', "Name")
      .click('@researchGroupsRole')
      .waitForElementVisible('@researchGroupsRolePostdoc')
      .click('@researchGroupsRolePostdoc')
      .clearValue( '@researchGroupsDepartment')
      .setValue( '@researchGroupsDepartment', "Department name")
      .clearValue( '@researchGroupsStartDate')
      .setValue( '@researchGroupsStartDate', "01/01/2000")
      .clearValue( '@researchGroupsEndDate')
      .setValue( '@researchGroupsEndDate', "01/01/2020")
      .clearValue( '@researchGroupsURL')
      .setValue( '@researchGroupsURL', "http://www.url.com")
      .clearValue( '@researchGroupsAwards')
      .setValue( '@researchGroupsAwards', "Award, Scholarship, Stipend")
      .clearValue( '@researchGroupsComments')
      .setValue( '@researchGroupsComments', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.")
      .click('@researchGroupsInternationalCbox')
      .waitForElementVisible('@researchGroupsInternationalCboxCountry')
      .clearValue( '@researchGroupsInternationalCboxCountry')
      .setValue( '@researchGroupsInternationalCboxCountry', "Country")
      .click('@researchGroupsInterdisciplinaryCbox')
      .click('@researchGroupsCommunityEngagementCbox')
      .click('@researchGroupsPublishCbox')
      .click('@researchGroupsPublishCbox')
  },
  editResResearchGroupsSort() {
    return this
      .waitForElementVisible('@researchGroupsSortButton')
      .click('@researchGroupsSortButton')
    },
  editResResearchGroupsShowHide() {
    return this
      .waitForElementVisible('@researchGroupsShowHideButton')
      .click('@researchGroupsShowHideButton')
  },
  editResResearchGroupsShowHideSelect() {
    return this
      .waitForElementVisible('@researchGroupsShowHideBox')
      .click('@researchGroupsShowHideBox')
    },
 removeOneResearchGroups() {
    return this
      .waitForElementVisible('@removeOneResearchGroupsButton')
      .click('@removeOneResearchGroupsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    researchGroupsEditLabel: {
      selector: 'a[href="index.html#research_groups"]'
    },
    researchGroupsAddButton: {
      selector: 'a[href="/research_groups/edit.html"]'
    },
    researchGroupsSortButton: {
      selector: 'a[href="/research_groups/sort.html"]'
    },
    researchGroupsShowHideButton: {
      selector: 'a[href="/research_groups/visible.html"]'
    },
    removeOneResearchGroupsButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    //
    researchGroupsGroupMemberName: {
      selector: '#name'
    },
    researchGroupsRole: {
      selector: '#role'
    },
    researchGroupsRolePostdoc: {
      selector: "//option[text()='Postdoc']",
      locateStrategy: 'xpath'
    },
    researchGroupsDepartment: {
      selector: '#departmentProgram'
    },
    researchGroupsURL: {
      selector: '#url'
    },
    researchGroupsStartDate: {
      selector: '#start'
    },
    researchGroupsEndDate: {
      selector: '#end'
    },
    researchGroupsAwards: {
      selector: '#awardsScholarshipsStipends'
    },
    researchGroupsComments: {
      selector: '#comments'
    },
    //
    researchGroupsInternationalCbox: {
      selector: '#checked_0'
    },
    researchGroupsInternationalCboxCountry: {
      selector: '#value_0'
    },
    researchGroupsInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    researchGroupsCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    researchGroupsPublishCbox: {
      selector: '#displayed1'
    },
    researchGroupsShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    }


  }
};
