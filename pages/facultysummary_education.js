const pageCommands = {
  facultyEducation(){
    return this
      .waitForElementVisible('@educationLabel')
      .click('@educationLabel')
  },
  facultyEducationAdd() {
    return this
    .waitForElementVisible('@educationAddButton')
    .click('@educationAddButton')
  },
  facultyEducationAddContent() {
     this
    .waitForElementVisible('@degreeType')
    .click('@degreeType')
    .waitForElementVisible('@degreeTypeDEGREE')
    .click('@degreeTypeDEGREE')
    this.api.pause(2000);
    this.waitForElementVisible('@degreeID')
    .click('@degreeID')
    .waitForElementVisible('@degreeIDAA')
    .click('@degreeIDAA')
    .waitForElementVisible('@subjectSpecialty')
    .click('@subjectSpecialty')
    .waitForElementVisible('@subjectSpecialty')
    .clearValue('@subjectSpecialtyInput')
    .setValue('@subjectSpecialtyInput',"c")
    this.api.pause(2000);
    this.waitForElementVisible('@subjectSpecialtyInputClick')
    .click('@subjectSpecialtyInputClick')
    .waitForElementVisible('@institutionID')
    .click('@institutionID')
    .waitForElementVisible('@institutionIDInput')
    .setValue('@institutionIDInput',"Aalborg");
    this.api.pause(2000);
    this.api.keys(this.api.Keys.ENTER);
    this.waitForElementVisible('@collegeName')
    .clearValue('@collegeName')
    .setValue('@collegeName',"Mycollege")
    .click('@startedMonth')
    .waitForElementVisible('@startedMonthJan')
    .click('@startedMonthJan')
    .clearValue('@startedYear')
    .setValue('@startedYear',"2000")
    .click('@issuedMonth')
    .waitForElementVisible('@issuedMonthFeb')
    .click('@issuedMonthFeb')
    .clearValue('@issuedYear')
    .setValue('@issuedYear',"2004")
    .click('@highestDegreeCheckbox')
    return this;
  },
  removeOneEducation() {
    return this
      .waitForElementVisible('@removeOneEducationButton')
      .click('@removeOneEducationButton')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    educationLabel: {
      selector: "a[href='education.html?emplid=RA102153']"
    },
    educationAddButton: {
      selector: 'a[href="education_edit.html?emplid=RA102153"]'
    },
    degreeType: {
      selector: '#degree_type_chosen'
    },
    degreeTypeDEGREE: {
      selector: "//li[text()='DEGREE']",
      locateStrategy: 'xpath'
    },
    degreeID: {
      selector: '#degree_id_chosen'
    },
    degreeIDAA: {
      selector: "//li[text()='AA']",
      locateStrategy: 'xpath'
    },
    subjectSpecialty: {
       selector: '//*[@id="s2id_subject.id"]',
      locateStrategy:'xpath'
    },
    subjectSpecialtyInput: {
      selector: '#s2id_autogen2_search'
    },
    subjectSpecialtyInputClick: {
      selector: '#select2-result-label-3'
    },
    institutionID: {
      selector: '//*[@id="s2id_institution.id"]',
      locateStrategy:'xpath'
    },
    institutionIDInput: {
      selector: 'input[id="s2id_autogen1_search"]'
    },
    institutionIDInputAalborg: {
    //  selector: '#select2-results-lable-104'
      selector: "#select2-results-1 > li >div"
    },
    collegeName: {
      selector: 'input[name="collegeName"]'
    },
    startedMonth:{
      selector: 'select[name="startedMonth"]'
    },
    startedMonthJan: {
      selector: 'select[name="startedMonth"] option[value="1"]'
    },
    startedYear: {
      selector: 'input[name="startedYear"]'
    },
    issuedMonth:{
      selector: 'select[name="issuedMonth"]'
    },
    issuedMonthFeb: {
      selector: 'select[name="issuedMonth"] option[value="2"]'
    },
    issuedYear: {
      selector: 'input[name="issuedYear"]'
    },
    highestDegreeCheckbox:{
      selector: '#highDegree'
    },
    removeOneEducationButton: {
      selector: 'img[src="images/icons/delete.png"]'
    }


  }
};
