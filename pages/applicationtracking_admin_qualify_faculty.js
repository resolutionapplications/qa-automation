const pageCommands = {
  clickQualifyFaculty(){
    return this
      .waitForElementVisible('@qualifyFacultyFirstLabel')
      .click('@qualifyFacultyFirstLabel')
  },
  plusQualifyFaculty(){
    return this
      .waitForElementVisible('@qualifyFacultyEditButton')
      .click('@qualifyFacultyEditButton')
  },
 fillQualifyFaculty() {
    return this
    .waitForElementVisible('@qualifyContentArea')
    .click('@qualifyContentArea')
    .waitForElementVisible('@qualifyInput')    
    .setValue('@qualifyInput',"t")
    .waitForElementVisible('@qualifyInputClick')
    .click('@qualifyInputClick') 
    .waitForElementVisible('@qualifyPath1')
    .click('@qualifyPath1')
    .click('@qualifyHLCType0')
  },
 removeOneQualifyFaculty(){
    return this
      .waitForElementVisible('@removeOneQualifyFacultyButton')
      .click('@removeOneQualifyFacultyButton')
  },
  saveQualifyFaculty() {
    return this
      .waitForElementVisible('@saveQualifyFacultyButton')
      .click('@saveQualifyFacultyButton')
  },
  saveAndAddQualifyFaculty() {
    return this
      .waitForElementVisible('@saveAndAddQualifyFacultyButton')
      .click('@saveAndAddQualifyFacultyButton')
  },
  cancelQualifyFaculty() {
    return this
      .waitForElementVisible('@cancelQualifyFacultyButton')
      .click('@cancelQualifyFacultyButton')
  },
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    qualifyFacultyFirstLabel: {
      selector: "a[href='qualify_faculty.html']"
    },
    appProffesionalExperienceLabel: {
      selector: "//h1[text()='Qualify Faculty']",
      locateStrategy: 'xpath'
    },
    qualifyFacultyEditButton: {
      selector: "a[href='qualify_faculty_edit.html']"
    },
    qualifyContentArea: {
      selector: '#cars_chosen'
    },
    qualifyInput: {
      selector: '#cars_chosen > div > div > input[type="text"]'
    },
    qualifyInputClick: {
      selector: "#cars_chosen > div > ul > li:nth-child(1)"
    },
    qualifyPath1: {
      selector: 'input[name="carpId"]'
    },
    qualifyHLCType0: {
      selector: '#content_container > div > div:nth-child(2) > div > div.content-padder > div > form > fieldset > table > tbody > tr:nth-child(2) > td > fieldset > table > tbody > tr > td > input[type="radio"]'
    },
     removeOneQualifyFacultyButton: {
       selector: 'img[src="../images/icons/delete.png"]'
     },
    cancelQualifyFacultyButton: {
      selector: 'input[value="Cancel"]'
    },
    saveQualifyFacultyButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddQualifyFacultyButton: {
      selector: 'input[value="Save & Add Another"]'
    },
  }
};
