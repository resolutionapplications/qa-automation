const pageCommands = {
  editBioBiography() {
    return this
      .waitForElementVisible('@biographyEditLabel')
      .click('@biographyEditLabel')
  },
  editBioBiographyContentClick() {
    return this
      .waitForElementVisible('@biographyEditContentButton')
      .click('@biographyEditContentButton')
    },

  editBioBiographyDelete() {
    return this
      .waitForElementVisible('@biographyDeleteButton')
      .click('@biographyDeleteButton')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    biographyEditLabel: {
      selector: 'a[href="index.html#bio"]'
    },
    biographyEditContentButton: {
      selector: 'a[href="/bio/edit.html"]'
    },
    biographyDeleteButton: {
      selector: 'a[href="/bio/delete.html?id=0"]'
    },
    biographyContent: {
      //selector: '#cke_1_contents'
      selector: '#cke_content'
      //#cboxOverlay      #colorbox

    },
  }
};
