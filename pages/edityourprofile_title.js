const pageCommands = {
  // Edit icon/pencil 1
  editTitleOne() {
    return this
      .waitForElementVisible('@titleOneEditButton')
      .click('@titleOneEditButton')
  },
  // Edit icon/pencil 2
  editTitleTwo() {
    return this
      .waitForElementVisible('@titleTwoEditButton')
      .click('@titleTwoEditButton')
  },
  // Edit icon/pencil 3
  editTitleThree() {
    return this
      .waitForElementVisible('@titleThreeEditButton')
      .click('@titleThreeEditButton')
  },
 //
  editTitleNoPhoto() {
    return this
      .waitForElementVisible('@removePhotoCbx')
      .click('@removePhotoCbx')
  },
  editTitleWithPhoto() {
    return this
      .waitForElementVisible('@browsePhotoButton')
      // .setValue('input[type="file"]', require('path').resolve('C:/Users/User/Desktop/resoapps/nw/tests/profileImage.jpg'))
      // driver.setValue('input#fileUpload', __dirname + '\\testfile.txt')
  },
  editTitleFill() {
    return this
      .waitForElementVisible('@photoName')
      .clearValue('@photoName')
      .setValue('@photoName',"Tyler's Photo")
      .clearValue('@firstName')
      .setValue('@firstName',"Tyler")
      .clearValue('@middleName')
      .setValue('@middleName',"A")
      .clearValue('@lastName')
      .setValue('@lastName',"Pingree")
      .clearValue('@suffixName')
      .setValue('@suffixName',"S")
      .clearValue('@websiteName')
      .setValue('@websiteName',"http://ProminentAlternateWebsite.biz")

  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    titleOneEditButton: {
      selector: 'a[href="/listing/edit.html"]'
    },
    titleTwoEditButton: {
      selector: '//*[@id="profile"]/div/ul/li[3]/a[2]/img',
      locateStrategy: 'xpath'
    },
    titleThreeEditButton: {
      selector: '//*[@id="maincol"]/div/div[1]/a/img',
      locateStrategy: 'xpath'
    },
    browsePhotoButton: {
      selector: 'input[type="file"]'
    },
    removePhotoCbx: {
      selector: '#removePortrait'
    },
    photoName: {
      selector: 'input[name="mugshot.altText"]'
    },
    firstName:  {
      selector: 'input[name="preferredName.firstName"]'
    },
    middleName:  {
      selector: 'input[name="preferredName.middleName"]'
    },
    lastName:  {
      selector: 'input[name="preferredName.lastName"]'
    },
    suffixName:  {
      selector: 'input[name="preferredName.suffix"]'
    },
    websiteName: {
      selector: 'input[name="preferredWebAddress"]'
    }



  }
};
