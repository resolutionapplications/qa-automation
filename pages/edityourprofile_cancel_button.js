const pageCommands = {
  editProfileCancel() {
    return this
      .waitForElementVisible('@cancelButton')
      .click('@cancelButton')
  },
   editProfileCancelButton() {
    return this
      .waitForElementVisible('@cancelButtonName')
      .click('@cancelButtonName')
  },
  editProfileCancelButtonXpath() {
    return this
      .waitForElementVisible('@cancelButtonXpath')
      .click('@cancelButtonXpath')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    cancelButton: {
      selector: '#_cancel'
    },
    cancelButtonName: {
      selector: 'input[name="_cancel"]'
    },
    cancelButtonXpath: {
      selector: '//*[@id="displayform"]/fieldset[2]/input[7]',
      locateStrategy: 'xpath'
    }
  }
};
