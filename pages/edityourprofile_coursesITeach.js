const pageCommands = {
  editTeaCourses() {
    return this
      .waitForElementVisible('@coursesEditLabel')
      .click('@coursesEditLabel')
  },
  editTeaCoursesAdd() {
    return this
      .waitForElementVisible('@coursesAddButton')
      .click('@coursesAddButton')
  },
  editTeaCoursesAddContentOne() {
      return this
        .waitForElementVisible('@courseTitle')
        .clearValue('@courseTitle')
        .setValue('@courseTitle',"Title of the course")
        .clearValue('@courseNumber')
        .setValue('@courseNumber',"5231")
        .clearValue('@courseDescription')
        .setValue('@courseDescription',"Short description of the course 5231")
        .clearValue('@courseURL')
        .setValue('@courseURL',"http://www.course.com")
        .setValue('@courseSyllabus', __dirname + '\\AJH-Scrummaster.pdf')
        .click('@coursePublishCbox')
        .click('@coursePublishCbox')
  },
  editTeaCoursesAddContentTwo() {
      return this
        .waitForElementVisible('@courseTitle')
        .clearValue('@courseTitle')
        .setValue('@courseTitle',"Title of the course")
        .clearValue('@courseNumber')
        .setValue('@courseNumber',"5231")
        .clearValue('@courseDescription')
        .setValue('@courseDescription',"Short description of the course 5231")
        .clearValue('@courseURL')
        .setValue('@courseURL',"http://www.course.com")
        .click('@courseSyllabusDeleteCbox')
        .click('@coursePublishCbox')
        .click('@coursePublishCbox')
  },
  editTeaCoursesSort() {
    return this
      .waitForElementVisible('@coursesSortButton')
      .click('@coursesSortButton')
    },
  editTeaCoursesShowHide() {
    return this
      .waitForElementVisible('@coursesShowHideButton')
      .click('@coursesShowHideButton')
  },
  editTeaCoursesShowHideSelect() {
    return this
      .waitForElementVisible('@coursesShowHideBox')
      .click('@coursesShowHideBox')
    },
  removeOneCourses() {
    return this
      .waitForElementVisible('@removeOneCoursesButton')
      .click('@removeOneCoursesButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    coursesEditLabel: {
      selector: 'a[href="index.html#coursesteach"]'
    },
    coursesAddButton: {
      selector: 'a[href="/coursesteach/edit.html"]'
    },
    coursesSortButton: {
      selector: 'a[href="/coursesteach/sort.html"]'
    },
    coursesShowHideButton: {
      selector: 'a[href="/coursesteach/visible.html"]'
    },
   removeOneCoursesButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    courseTitle: {
      selector: '#courseTitle'
    },
    courseNumber: {
      selector: '#courseNumber'
    },
    courseDescription: {
      selector: '#description'
    },
    courseURL: {
      selector: '#url'
    },
    courseSyllabus: {
      selector: 'input[type="file"]'
    },
    courseSyllabusDeleteCbox: {
      selector: '#deleteSyllabus1'
    },
    coursePublishCbox: {
      selector: '#displayed1'
    },
    coursesShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    }

  }
};
