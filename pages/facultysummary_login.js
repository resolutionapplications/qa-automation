const pageCommands = {
  loginToFaculty(username, password) {
    return this
      .waitForElementVisible('@emailInput')
      .clearValue('@emailInput')
      .setValue('@emailInput', username)
      .clearValue('@passInput')
      .setValue('@passInput', password)
      .waitForElementVisible('@loginButton')
      .click('@loginButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    emailInput: {
      selector: 'input[name="user"]'
    },
    passInput: {
      selector: 'input[name="password"]'
    },
    loginButton: {
      selector: 'input[type=submit]'
    }
  }
};
