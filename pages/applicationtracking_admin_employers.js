const pageCommands = {
  clickEmployers(){
    return this
      .waitForElementVisible('@employersFirstLabel')
      .click('@employersFirstLabel')
  },
  plusEmployers() {
    return this
    .waitForElementVisible('@employersEditButton')
    .click('@employersEditButton')
  },
  fillEmployers() {
    return this
    .waitForElementVisible('@institutionEmployers')
    .click('@institutionEmployers')
    .waitForElementVisible('@institutionEmployersOther')
    .click('@institutionEmployersOther')
  },
  cancelEmployers() {
    return this
      .waitForElementVisible('@cancelEmployersButton')
      .click('@cancelEmployersButton')
  },
  saveEmployers() {
    return this
      .waitForElementVisible('@saveEmployersButton')
      .click('@saveEmployersButton')
  },
  removeOneEmployer() {
    return this
      .waitForElementVisible('@removeOneEmployerButton')
      .click('@removeOneEmployerButton')
  },
  checkIsEmployersOpen() {
    return this
      .waitForElementVisible('@appEmployersLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    employersFirstLabel: {
      selector: "a[href='affiliations.html']"
    },
    appEmployersLabel: {
      selector: "//h1[text()='Employers & Affiliations']",
      locateStrategy: 'xpath'
    },
    employersEditButton: {
      selector: 'a[href="affiliations_affiliates_edit.html"]'
    },
    institutionEmployers: {
      selector: '#institution_id_chosen'
    },
    institutionEmployersOther: {
      selector: "//li[text()='Other']",
      locateStrategy: 'xpath'
    },
    removeOneEmployerButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
    cancelEmployersButton: {
      selector: 'input[value="Cancel"]'
    },
    saveEmployersButton: {
      selector: 'input[value="Save"]'
    },



  }
};
