const pageCommands = {
  editResResearchPatents() {
    return this
      .waitForElementVisible('@patentsEditLabel')
      .click('@patentsEditLabel')
  },
  editResResearchPatentsAdd() {
    return this
    .waitForElementVisible('@patentsAddButton')
    .click('@patentsAddButton')
  },
  editResResearchPatentsAddContent() {
      return this
      .waitForElementVisible('@patentsName')
      .clearValue( '@patentsName')
      .setValue( '@patentsName', "Name of the patent")
      .clearValue( '@patentsNumber')
      .setValue( '@patentsNumber', "0123456789")
      .click('@patentsStatus')
      .waitForElementVisible('@patentsStatusPublished')
      .click('@patentsStatusPublished')
      .click('@patentsType')
      .waitForElementVisible('@patentsTypeParent')
      .click('@patentsTypeParent')
      .clearValue( '@patentsFileDate')
      .setValue( '@patentsFileDate', "01/01/2000")
      .clearValue( '@patentsIssueDate')
      .setValue( '@patentsIssueDate', "01/01/2020")
      .clearValue( '@patentsAssignee')
      .setValue( '@patentsAssignee', "Patents: Assignee")
      .clearValue( '@patentsInventors')
      .setValue( '@patentsInventors', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.")
      .clearValue( '@patentsCountryName')
      .setValue( '@patentsCountryName', "Country name")
      .click('@patentsInternationalCbox')
      .waitForElementVisible('@patentsInternationalCboxCountry')
      .clearValue( '@patentsInternationalCboxCountry')
      .setValue( '@patentsInternationalCboxCountry', "Country")
      .click('@patentsInterdisciplinaryCbox')
      .click('@patentsCommunityEngagementCbox')
      .click('@patentsPublishCbox')
  },
  editResResearchPatentsSort() {
    return this
      .waitForElementVisible('@patentsSortButton')
      .click('@patentsSortButton')
    },
  editResResearchPatentsShowHide() {
    return this
      .waitForElementVisible('@patentsShowHideButton')
      .click('@patentsShowHideButton')
  },
  editResResearchPatentsShowHideSelect() {
    return this
      .waitForElementVisible('@patentsShowHideBox')
      .click('@patentsShowHideBox')
    },
  removeOneResearchPatents() {
    return this
      .waitForElementVisible('@removeOneResearchPatentsButton')
      .click('@removeOneResearchPatentsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    patentsEditLabel: {
      selector: 'a[href="index.html#ptnt"]'
    },
    patentsAddButton: {
      selector: 'a[href="/patent/edit.html"]'
    },
    patentsSortButton: {
      selector: 'a[href="/patent/sort.html"]'
    },
    patentsShowHideButton: {
      selector: 'a[href="/patent/visible.html"]'
    },
    removeOneResearchPatentsButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    //
    patentsName: {
      selector: '#patentName'
    },
    patentsNumber: {
      selector: '#patentNumber'
    },
    patentsStatus: {
      selector: '#status'
    },
    patentsStatusPublished: {
      selector: "//option[text()='Published']",
      locateStrategy: 'xpath'
    },
    patentsType: {
      selector: '#patentTypeId'
    },
    patentsTypeParent: {
      selector: "//option[text()='Parent/Utility']",
      locateStrategy: 'xpath'
    },
    patentsFileDate: {
      selector: '#fileDate'
    },
    patentsIssueDate: {
      selector: '#issueDate'
    },
    patentsAssignee: {
      selector: '#assignee'
    },
    patentsInventors: {
      selector: '#inventors'
    },
    patentsCountryName: {
      selector: '#countryAutocomp'
    },
    //
    patentsInternationalCbox: {
      selector: '#checked_0'
    },
    patentsInternationalCboxCountry: {
      selector: '#value_0'
    },
    patentsInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    patentsCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    patentsPublishCbox: {
      selector: '#displayable1'
    },
    patentsShowHideBox: {
      selector: 'input[name="list[0].displayable"]'
    }


  }
};
