const pageCommands = {
  clickWMedServices(){
    return this
      .waitForElementVisible('@wMedServicesFirstLabel')
      .click('@wMedServicesFirstLabel')
  },
  fillWMedServices() {
    return this
    .waitForElementVisible('@chb_AdmissionsInterviewer')
    .click('@chb_AdmissionsInterviewer')
    .click('@chb_ClinicalResearch')
    .click('@chb_CurriculumDevelopment')
    .click('@chb_EducationalResearch')
    .click('@chb_HostElectiveStudents')
    .click('@chb_LaboratoryResearch')
    .click('@chb_Mentoring')
    .click('@chb_Precepting')
    .click('@chb_TeachingResidents')
    .click('@chb_TeachingStudents')
    .click('@chb_Toxicology')
    .click('@chb_WMedCommittees')
  },
  checkIsWMedServicesOpen() {
    return this
      .waitForElementVisible('@appWMedServicesLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    wMedServicesFirstLabel: {
      selector: "a[href='areas_of_interest.html']"
    },
    appWMedServicesLabel: {
      selector: "//h2[text()='Service Interests']",
      locateStrategy: 'xpath'
    },
    chb_AdmissionsInterviewer: {
      selector: 'input[name="questions[1].value"]'
    },
    chb_ClinicalResearch: {
      selector: 'input[name="questions[2].value"]'
    },
    chb_CurriculumDevelopment: {
      selector: 'input[name="questions[3].value"]'
    },
    chb_EducationalResearch: {
      selector: 'input[name="questions[4].value"]'
    },
    chb_HostElectiveStudents: {
      selector: 'input[name="questions[5].value"]'
    },
    chb_LaboratoryResearch: {
      selector: 'input[name="questions[6].value"]'
    },
    chb_Mentoring: {
      selector: 'input[name="questions[7].value"]'
    },
    chb_Precepting: {
      selector: 'input[name="questions[8].value"]'
    },
    chb_TeachingResidents: {
      selector: 'input[name="questions[9].value"]'
    },
    chb_TeachingStudents: {
      selector: 'input[name="questions[10].value"]'
    },
    chb_Toxicology: {
      selector: 'input[name="questions[11].value"]'
    },
    chb_WMedCommittees: {
      selector: 'input[name="questions[12].value"]'
    },
  }
};
