const pageCommands = {
  editOutreach() {
    return this
    .waitForElementVisible('@outreachEditLabel')
    .click('@outreachEditLabel')
  },
  editOutreachAdd() {
    return this
    .waitForElementVisible('@outreachAddButton')
    .click('@outreachAddButton')
  },
  editOutreachAddContent() {
      return this
      .waitForElementVisible('@outreachMentoringActivity')
      .clearValue('@outreachMentoringActivity')
      .setValue('@outreachMentoringActivity',"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.")
      .clearValue( '@outreachStartDate')
      .setValue( '@outreachStartDate', "01/01/2000")
      .clearValue( '@outreachEndDate')
      .setValue( '@outreachEndDate', "01/01/2020")
      .clearValue('@outreachHours')
      .setValue('@outreachHours',"6")
      .click('@outreachInternationalCbox')
      .waitForElementVisible('@outreachInternationalCboxCountry')
      .clearValue( '@outreachInternationalCboxCountry')
      .setValue( '@outreachInternationalCboxCountry', "Country")
      .click('@outreachInterdisciplinaryCbox')
      .click('@outreachCommunityEngagementCbox')
      .click('@outreachPublishCbox')
      .click('@outreachPublishCbox')

  },
  editOutreachSort() {
    return this
      .waitForElementVisible('@outreachSortButton')
      .click('@outreachSortButton')
    },
  editOutreachShowHide() {
    return this
      .waitForElementVisible('@outreachShowHideButton')
      .click('@outreachShowHideButton')
  },
  editOutreachShowHideSelect() {
    return this
      .waitForElementVisible('@outreachShowHideBox')
      .click('@outreachShowHideBox')
    },
  removeOneOutreach() {
    return this
      .waitForElementVisible('@removeOneOutreachButton')
      .click('@removeOneOutreachButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    outreachEditLabel: {
      selector:'a[href="index.html#outreach"]'
    },
    outreachAddButton: {
      selector: 'a[href="/outreach/edit.html"]'
    },
    outreachSortButton: {
      selector: 'a[href="/outreach/visible.html"]'
    },
    outreachShowHideButton: {
      selector: 'a[href="/mentor/visible.html"]'
    },
    outreachMentoringActivity: {
      selector: "#servicePerformed"
    },
    outreachStartDate: {
      selector: '#startDate'
    },
    outreachEndDate: {
      selector: '#endDate'
    },
    outreachHours: {
      selector: '#hours'
    },
    outreachInternationalCbox: {
      selector: '#checked_0'
    },
    outreachInternationalCboxCountry: {
      selector: '#value_0'
    },
    outreachInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    outreachCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    outreachPublishCbox: {
      selector: '#displayed1'
    },
    outreachShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
    removeOneOutreachButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
