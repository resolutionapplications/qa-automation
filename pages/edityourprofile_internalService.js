const pageCommands = {
  editInternalService() {
    return this
    .waitForElementVisible('@internalServiceLabel')
    .click('@internalServiceLabel')
  },
  editInternalServiceAdd() {
    return this
    .waitForElementVisible('@internalServiceAddButton')
    .click('@internalServiceAddButton')
  },
  editInternalServiceAddContent() {
      return this
      .waitForElementVisible('@internalServiceStatus')
      .click('@internalServiceStatus')
      .waitForElementVisible('@internalServiceStatusCollege')
      .click('@internalServiceStatusCollege')
      .waitForElementVisible('@internalServiceAssignment')
      .clearValue('@internalServiceAssignment')
      .setValue('@internalServiceAssignment',"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.")
      .clearValue('@internalServiceRole')
      .setValue('@internalServiceRole',"My role")
      .clearValue( '@internalServiceStartDate')
      .setValue( '@internalServiceStartDate', "01/01/2000")
      .clearValue( '@internalServiceEndDate')
      .setValue( '@internalServiceEndDate', "01/01/2020")
      .clearValue('@internalServiceHours')
      .setValue('@internalServiceHours',"6")
      .click('@internalServiceInternationalCbox')
      .waitForElementVisible('@internalServiceInternationalCboxCountry')
      .clearValue( '@internalServiceInternationalCboxCountry')
      .setValue( '@internalServiceInternationalCboxCountry', "Country")
      .click('@internalServiceInterdisciplinaryCbox')
      .click('@internalServiceCommunityEngagementCbox')
      .click('@internalServicePublishCbox')
      .click('@internalServicePublishCbox')

  },
  editInternalServiceSort() {
    return this
      .waitForElementVisible('@internalServiceSortButton')
      .click('@internalServiceSortButton')
    },
  editInternalServiceShowHide() {
    return this
      .waitForElementVisible('@internalServiceShowHideButton')
      .click('@internalServiceShowHideButton')
  },
  editInternalServiceShowHideSelect() {
    return this
      .waitForElementVisible('@internalServiceShowHideBox')
      .click('@internalServiceShowHideBox')
    },
  removeOneInternalService() {
    return this
      .waitForElementVisible('@removeOneInternalServiceButton')
      .click('@removeOneInternalServiceButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    internalServiceLabel: {
      selector:'a[href="index.html#intservice"]'
    },
    internalServiceAddButton: {
      selector: 'a[href="/intservice/edit.html"]'
    },
    internalServiceSortButton: {
      selector: 'a[href="/intservice/sort.html"]'
    },
    internalServiceShowHideButton: {
      selector: 'a[href="/intservice/visible.html"]'
    },
    internalServiceStatus: {
      selector: "#level"
    },
    internalServiceStatusCollege: {
       selector: "//option[text()='College']",
      locateStrategy: 'xpath'
    },
    internalServiceAssignment: {
      selector: "#servicePerformed"
    },
    internalServiceRole: {
      selector: "#partPlayed"
    },
    internalServiceStartDate: {
      selector: '#startDate'
    },
    internalServiceEndDate: {
      selector: '#endDate'
    },
    internalServiceHours: {
      selector: '#hours'
    },
    internalServiceInternationalCbox: {
      selector: '#checked_0'
    },
    internalServiceInternationalCboxCountry: {
      selector: '#value_0'
    },
    internalServiceInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    internalServiceCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    internalServicePublishCbox: {
      selector: '#displayed1'
    },
    internalServiceShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
    removeOneInternalServiceButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
