const pageCommands = {
  editBioAffiliations() {
    return this
      .waitForElementVisible('@affiliationsEditLabel')
      .click('@affiliationsEditLabel')
  },
  editBioAffiliationsAdd() {
    return this
      .waitForElementVisible('@affiliationsAddButton')
      .click('@affiliationsAddButton')
    },
    editBioAffiliationsAddContent() {
      return this
        .waitForElementVisible('@affiliationName')
        .clearValue('@affiliationName')
        .setValue('@affiliationName',"Organization Name")
        .clearValue('@affiliationAbbr')
        .setValue('@affiliationAbbr',"Abbr")
        .click('@affiliationOrganizationType')
        .clearValue('@affiliationRole')
        .setValue('@affiliationRole',"Role")
        .clearValue('@affiliationStartDate')
        .setValue('@affiliationStartDate',"10/10/2000")
        .clearValue('@affiliationEndDate')
        .setValue('@affiliationEndDate',"10/10/2005")
        .click('@affiliationPublishCbox')
      },
  editBioAffiliationsSort() {
    return this
      .waitForElementVisible('@affiliationsSortButton')
      .click('@affiliationsSortButton')
    },
  editBioAffiliationsShowHide() {
    return this
      .waitForElementVisible('@affiliationsShowHideButton')
      .click('@affiliationsShowHideButton')
  },
  editBioAffiliationsShowHideSelect() {
    return this
      .waitForElementVisible('@affiliationShowHideBox')
      .click('@affiliationShowHideBox')
  },
  removeOneAffiliations() {
    return this
      .waitForElementVisible('@removeOneAffiliationsButton')
      .click('@removeOneAffiliationsButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    affiliationsEditLabel: {
      selector: 'a[href="index.html#affiliations"]'
    },
    affiliationsAddButton: {
      selector: 'a[href="/affiliations/edit.html"]'
    },
    affiliationsSortButton: {
      selector: 'a[href="/affiliations/sort.html"]'
    },
    affiliationsShowHideButton: {
      selector: 'a[href="/affiliations/visible.html"]'
    },
    affiliationName: {
      selector: '#organization'
    },
    affiliationAbbr: {
      selector: '#abbreviation'
    },
    affiliationOrganizationType: {
      selector: '#type1'
    },
    affiliationCompanyType: {
      selector: '#type2'
    },
    affiliationRole: {
      selector: '#role'
    },
    affiliationStartDate: {
      selector: '#startDate'
    },
    affiliationEndDate: {
      selector: '#endDate'
    },
    affiliationPublishCbox: {
      selector: '#displayed1'
    },
    removeOneAffiliationsButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    affiliationShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    }
  }
};
