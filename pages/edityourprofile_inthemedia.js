const pageCommands = {
  editBioMedia() {
    return this
      .waitForElementVisible('@mediaEditLabel')
      .click('@mediaEditLabel')
  },
  editBioMediaAdd() {
    return this
      .waitForElementVisible('@mediaAddButton')
      .click('@mediaAddButton')
    },
    editBioMediaAddContent() {
      return this
        .waitForElementVisible('@mediaDate')
        .clearValue('@mediaDate')
        .setValue('@mediaDate',"01/07/2000")
        .clearValue('@mediaExposure')
        .setValue('@mediaExposure',"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum nunc a condimentum semper. Phasellus sed metus sem. Maecenas odio odio, eleifend sit amet mattis eget, laoreet id lorem.")
        .clearValue('@mediaURL')
        .setValue('@mediaURL',"http://www.resoulutionapplications.com")
        .click('@mediaInternationalCbox')
        .waitForElementVisible('@mediaInternationalCountry')
        .clearValue('@mediaInternationalCountry')
        .setValue('@mediaInternationalCountry',"Sri Lanka")
        .click('@mediaInterdisciplinaryCbox')
        .click('@mediaCommunityCbox')
        .click('@mediaPublish')
        .click('@mediaPublish')
      },
  editBioMediaSort() {
    return this
      .waitForElementVisible('@mediaSortButton')
      .click('@mediaSortButton')
    },
  editBioMediaShowHide() {
    return this
      .waitForElementVisible('@mediaShowHideButton')
      .click('@mediaShowHideButton')
  },
  editBioMediaShowHideSelect() {
    return this
      .waitForElementVisible('@mediaShowHideBox')
      .click('@mediaShowHideBox')
  },
  removeOneMedia() {
    return this
      .waitForElementVisible('@removeOneMediaButton')
      .click('@removeOneMediaButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    mediaEditLabel: {
      selector: 'a[href="index.html#exposure"]'
    },
    mediaAddButton: {
      selector: 'a[href="/exposure/edit.html"]'
    },
    mediaSortButton: {
      selector: 'a[href="/exposure/sort.html"]'
    },
    mediaShowHideButton: {
      selector: 'a[href="/exposure/visible.html"]'
    },

    mediaDate: {
      selector: '#startDate'
    },
    mediaExposure: {
      selector: '#exposure'
    },
    mediaURL: {
      selector: '#url'
    },
    mediaInternationalCbox: {
      selector: '#checked_0'
    },
    mediaInternationalCountry: {
      selector: '#value_0'
    },
    mediaInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    mediaCommunityCbox: {
      selector: '#checked_2'
    },
    mediaPublish: {
      selector: '#displayed1'
    },
    mediaShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
    removeOneMediaButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    }

  }
};
