const pageCommands = {
  editFacPracticeHistory() {
    return this
      .waitForElementVisible('@practiceHistoryEditLabel')
      .click('@practiceHistoryEditLabel')
  },
  editFacPracticeHistoryAdd() {
    return this
    .waitForElementVisible('@practiceHistoryAddButton')
    .click('@practiceHistoryAddButton')
  },
  editFacPracticeHistoryAddContent() {
      return this
      .waitForElementVisible('@practiceHistoryDescription')
      .clearValue( '@practiceHistoryDescription')
      .setValue( '@practiceHistoryDescription', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.")
      .clearValue( '@practiceHistoryDate')
      .setValue( '@practiceHistoryDate', "01/01/2000")
      .clearValue( '@practiceHistoryURL')
      .setValue( '@practiceHistoryURL', "http://www.url.com")
      .click('@practiceHistoryInternationalCbox')
      .waitForElementVisible('@practiceHistoryInternationalCboxCountry')
      .clearValue( '@practiceHistoryInternationalCboxCountry')
      .setValue( '@practiceHistoryInternationalCboxCountry', "Country")
      .click('@practiceHistoryCommunityEngagementCbox')
  },
  editFacPracticeHistorySort() {
    return this
      .waitForElementVisible('@practiceHistorySortButton')
      .click('@practiceHistorySortButton')
    },
  editFacPracticeHistoryShowHide() {
    return this
      .waitForElementVisible('@practiceHistoryShowHideButton')
      .click('@practiceHistoryShowHideButton')
  },
   editFacPracticeHistoryShowHideSelect() {
    return this
      .waitForElementVisible('@practiceHistoryShowHideBox')
      .click('@practiceHistoryShowHideBox')
    },
    removeOnePracticeHistory() {
    return this
      .waitForElementVisible('@removeOnePracticeHistoryButton')
      .click('@removeOnePracticeHistoryButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    practiceHistoryEditLabel: {
      selector: 'a[href="index.html#practicehist"]'
    },
    practiceHistoryAddButton: {
      selector: 'a[href="/practicehist/edit.html"]'
    },
    practiceHistorySortButton: {
      selector: 'a[href="/practicehist/sort.html"]'
    },
    practiceHistoryShowHideButton: {
      selector: 'a[href="/practicehist/visible.html"]'
    },
    //
    practiceHistoryDescription: {
      selector: '#description'
    },
    practiceHistoryURL: {
      selector: '#url'
    },
    practiceHistoryDate: {
      selector: '#date'
    },
    //
    practiceHistoryInternationalCbox: {
      selector: '#checked_0'
    },
    practiceHistoryInternationalCboxCountry: {
      selector: '#value_0'
    },
    practiceHistoryCommunityEngagementCbox: {
      selector: '#checked_1'
    },
    practiceHistoryShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
     removeOnePracticeHistoryButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
