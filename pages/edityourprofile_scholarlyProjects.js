const pageCommands = {
  editTeaScholarly() {
    return this
      .waitForElementVisible('@scholarlyEditLabel')
      .click('@scholarlyEditLabel')
  },
  editTeaScholarlyAdd() {
    return this
      .waitForElementVisible('@scholarlyAddButton')
      .click('@scholarlyAddButton')
  },
  editTeaScholarlyAddContent() {
      return this
        .waitForElementVisible('@scholarlyProject')
        .clearValue('@scholarlyProject')
        .setValue('@scholarlyProject',"Short description of the scholarly project")
        .clearValue('@scholarlyStartDate')
        .setValue('@scholarlyStartDate',"01/08/2000")
        .clearValue('@scholarlyEndDate')
        .setValue('@scholarlyEndDate',"05/12/2000")
        .click('@scholarlyAddMoreStudentsButton')
        .waitForElementVisible('@scholarlyStudent1')
        .clearValue('@scholarlyStudent1')
        .setValue('@scholarlyStudent1',"Name1 Surname1")
        .waitForElementVisible('@scholarlyStudent2')
        .clearValue('@scholarlyStudent2')
        .setValue('@scholarlyStudent2',"Name2 Surname2")
        .waitForElementVisible('@scholarlyStudent3')
        .clearValue('@scholarlyStudent3')
        .setValue('@scholarlyStudent3',"Name1 Surname3")
        .click('@scholarlyInternationalCbox')
        .waitForElementVisible('@scholarlyInternationalCountry')
        .clearValue('@scholarlyInternationalCountry')
        .setValue('@scholarlyInternationalCountry',"Country")
        .click('@scholarlyInterdisciplinaryCbox')
        .click('@scholarlyCommunityEngagementCbox')
  },
  editTeaScholarlySort() {
    return this
      .waitForElementVisible('@scholarlySortButton')
      .click('@scholarlySortButton')
    },
  editTeaScholarlyShowHide() {
    return this
      .waitForElementVisible('@scholarlyShowHideButton')
      .click('@scholarlyShowHideButton')
  },
  editTeaScholarlyCancel() {
    return this
      .waitForElementVisible('@scholarlyCancelButton')
      .click('@scholarlyCancelButton')
  },
   editTeaScholarlyShowHideSelect() {
    return this
      .waitForElementVisible('@scholarlyShowHideBox')
      .click('@scholarlyShowHideBox')
    },
  removeOneScholarly() {
    return this
      .waitForElementVisible('@removeOneScholarlyButton')
      .click('@removeOneScholarlyButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    scholarlyEditLabel: {
      selector: 'a[href="index.html#projects"]'
    },
    scholarlyAddButton: {
      selector: 'a[href="/projects/edit.html"]'
    },
    scholarlySortButton: {
      selector: 'a[href="/projects/sort.html"]'
    },
    scholarlyShowHideButton: {
      selector: 'a[href="/projects/visible.html"]'
    },
   removeOneScholarlyButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    //
    scholarlyProject: {
      selector: '#description'
    },
    scholarlyStartDate: {
      selector: '#startDate'
    },
    scholarlyEndDate: {
      selector: '#endDate'
    },
    scholarlyStudent1: {
      selector: '#students0'
    },
    scholarlyStudent2: {
      selector: '#students1'
    },
    scholarlyStudent3: {
      selector: '#students2'
    },
    scholarlyAddMoreStudentsButton: {
      selector: '#applybtn'
    },
    scholarlyInternationalCbox: {
      selector: '#checked_0'
    },
    scholarlyInternationalCountry: {
      selector: '#value_0'
    },
    scholarlyInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    scholarlyCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    scholarlyCancelButton: {
      selector: 'input[name="_cancel"]'
    },
    scholarlyShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    }

  }
};
