const pageCommands = {
  save() {
    return this
    .waitForElementVisible('@saveButton')
    .click('@saveButton')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    saveButton: {
      selector: 'input[value="Save"]'
    },



  }
};
