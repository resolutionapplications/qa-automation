const pageCommands = {
  clickTrackRank(){
    return this
      .waitForElementVisible('@trackRankFirstLabel')
      .click('@trackRankFirstLabel')
  },
  plusTrackRank() {
    return this
    .waitForElementVisible('@trackRankEditButton')
    .click('@trackRankEditButton')
  },
  fillTrackRank() {
     this
    .waitForElementVisible('@departmentTrack')
    .click('@departmentTrack')
    .waitForElementVisible('@deparmentTrackRank')
    .click('@deparmentTrackRank')
    this.api.pause(2000);
    this.waitForElementVisible('@typeTrack')
    .click('@typeTrack')
    .waitForElementVisible('@typeTrackRank')
    .click('@typeTrackRank')
    this.api.pause(2000);
    this.waitForElementVisible('@track')
    .click('@track')
    .waitForElementVisible('@trackClinical')
    .click('@trackClinical')
    this.api.pause(2000);
    this.waitForElementVisible('@rank')
    .click('@rank')
    .waitForElementVisible('@rankTrackRank')
    .click('@rankTrackRank')
    return this;
  },
  removeOneTrackRank() {
    return this
      .waitForElementVisible('@removeOneTrackRankButton')
      .click('@removeOneTrackRankButton')
  },
  cancelTrackRank() {
    return this
      .waitForElementVisible('@cancelTrackRankButton')
      .click('@cancelTrackRankButton')
  },
  saveTrackRank() {
    return this
      .waitForElementVisible('@saveTrackRankButton')
      .click('@saveTrackRankButton')
  },
  saveAndAddTrackRank() {
    return this
      .waitForElementVisible('@saveAndAddTrackRankButton')
      .click('@saveAndAddTrackRankButton')
  },

  checkIsTrackRankOpen() {
    return this
      .waitForElementVisible('@appTrackRankLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    trackRankFirstLabel: {
      selector: 'a[href="track_rank_specialty.html"]'
    },
    appTrackRankLabel: {
      selector: "//h2[text()='Track & Rank']",
      locateStrategy: 'xpath'
    },
    trackRankEditButton: {
      selector: 'a[href="track_rank_edit.html"]'
    },
    departmentTrack: {
      selector: '#department_id_chosen'
    },
    deparmentTrackRank: {
      selector: "//li[text()='Department of Anesthesiology']",
      locateStrategy: 'xpath'
    },
    typeTrack: {
      selector: '#type_id_chosen'
    },
    typeTrackRank: {
      selector: '//li[text()="Emeritus/Emerita"]',
      locateStrategy: 'xpath'
    },
    track: {
      selector: '#track_id_chosen'
    },
    trackClinical: {
      selector: "//li[text()='Clinical']",
      locateStrategy: 'xpath'
    },
    rank: {
      selector: '#rank_id_chosen'
    },
    rankTrackRank: {
      selector: '//li[text()="Clinical Associate Professor, Emerita"]',
      locateStrategy: 'xpath'
    },
     removeOneTrackRankButton: {
       selector: 'img[src="../images/icons/delete.png"]'
     },
    cancelTrackRankButton: {
      selector: 'input[value="Cancel"]'
    },
    saveTrackRankButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddTrackRankButton: {
      selector: 'input[value="Save & Add Another"]'
    },
  }
};
