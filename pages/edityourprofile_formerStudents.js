const pageCommands = {
  editGradFormerStudents() {
    return this
      .waitForElementVisible('@formerStudentsEditLabel')
      .click('@formerStudentsEditLabel')
  },
  editGradFormerStudentsAdd() {
    return this
    .waitForElementVisible('@formerStudentsAddButton')
    .click('@formerStudentsAddButton')
  },
  editGradFormerStudentsAddContent() {
      return this
      .waitForElementVisible('@formerStudentsStudentName')
      .clearValue( '@formerStudentsStudentName')
      .setValue( '@formerStudentsStudentName', "Name Surname")
      .clearValue( '@formerStudentsURL')
      .setValue( '@formerStudentsURL', "http://www.url.com")
      .click('@formerStudentsCommiteeRole')
      .click('@formerStudentsProjectType')
      .click('@formerStudentsStatus')
      .click('@formerStudentsGraduationSemester')
      .clearValue( '@formerStudentsExpectedGraduation')
      .setValue( '@formerStudentsExpectedGraduation', "2020")
      .click('@formerStudentsExpectedDegree')
      .waitForElementVisible('@formerStudentsExpectedDegreeAuD')
      .click('@formerStudentsExpectedDegreeAuD')
      .click('@formerStudentsTypeOfSupportRA')
      .click('@formerStudentsTypeOfSupportOther')
      .clearValue( '@formerStudentsSupportSource')
      .setValue( '@formerStudentsSupportSource', "My student support source")
      .clearValue( '@formerStudentsCoAdvisor')
      .setValue( '@formerStudentsCoAdvisor', "My Co-Advisor")
      .click('@formerStudentsNonUStudent1')
      .waitForElementVisible('@formerStudentsNonUStudentInstitution')
      .clearValue( '@formerStudentsNonUStudentInstitution')
      .setValue( '@formerStudentsNonUStudentInstitution', "My institution")
      .clearValue( '@formerStudentssComments')
      .setValue( '@formerStudentssComments', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.")
      .click('@formerStudentsInternationalCbox')
      .waitForElementVisible('@formerStudentsInternationalCboxCountry')
      .clearValue( '@formerStudentsInternationalCboxCountry')
      .setValue( '@formerStudentsInternationalCboxCountry', "Country")
      .click('@formerStudentsInterdisciplinaryCbox')
      .click('@formerStudentsCommunityEngagementCbox')
      .click('@formerStudentsPublishCbox')
  },
  editGradFormerStudentsSort() {
    return this
      .waitForElementVisible('@formerStudentsSortButton')
      .click('@formerStudentsSortButton')
    },
  editGradFormerStudentsShowHide() {
    return this
      .waitForElementVisible('@formerStudentsShowHideButton')
      .click('@formerStudentsShowHideButton')
  },
  editGradFormerStudentsShowHideSelect() {
    return this
      .waitForElementVisible('@formerStudentsShowHideBox')
      .click('@formerStudentsShowHideBox')
    },
  removeOneFormerStudents() {
    return this
      .waitForElementVisible('@removeOneFormerStudentsButton')
      .click('@removeOneFormerStudentsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    formerStudentsEditLabel: {
      selector: 'a[href="index.html#formerstudents"]'
    },
    formerStudentsAddButton: {
      selector: 'a[href="/formerstudents/edit.html"]'
    },
    formerStudentsSortButton: {
      selector: 'a[href="/formerstudents/sort.html"]'
    },
    formerStudentsShowHideButton: {
      selector: 'a[href="/formerstudents/visible.html"]'
    },
    removeOneFormerStudentsButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    formerStudentsStudentName: {
      selector: '#name'
    },
    formerStudentsURL: {
      selector: '#url'
    },
    formerStudentsCommiteeRole: {
      selector: '#committeeRole2'
    },
    formerStudentsProjectType: {
      selector: '#projectType2'
    },
    formerStudentsStatus: {
      selector: '#status2'
    },
    formerStudentsGraduationSemester: {
      selector: "#graduationSemester3"
    },
    formerStudentsExpectedGraduation: {
      selector: '#expectedGraduation'
    },
    formerStudentsExpectedDegree: {
      selector: '#degree'
    },
    formerStudentsExpectedDegreeAuD: {
      selector: "//option[text()='Doctor of Audiology (Au.D.)']",
      locateStrategy: 'xpath'
    },
    formerStudentsTypeOfSupportRA: {
      selector: 'input[name="supportTypesList[0].selected"]'
    },
    formerStudentsTypeOfSupportOther: {
      selector: 'input[name="supportTypesList[3].selected"]'
    },
    formerStudentsSupportSource: {
      selector: '#supportSource'
    },
    formerStudentsCoAdvisor: {
      selector: '#coAdvisor'
    },
    formerStudentsNonUStudent1: {
      selector: '#nonUStudent1'
    },
    formerStudentsNonUStudentInstitution: {
      selector: '#nonUStudentInstitution'
    },
    formerStudentssComments: {
      selector: '#comment'
    },
    //
    formerStudentsInternationalCbox: {
      selector: '#checked_0'
    },
    formerStudentsInternationalCboxCountry: {
      selector: '#value_0'
    },
    formerStudentsInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    formerStudentsCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    formerStudentsPublishCbox: {
      selector: '#displayable1'
    },
    formerStudentsShowHideBox: {
      selector: 'input[name="list[0].displayable"]'
    }


  }
};
