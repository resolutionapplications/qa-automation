const pageCommands = {
  editMoreLinksAdd() {
    return this
      .waitForElementVisible('@moreLinksAddButton')
      .click('@moreLinksAddButton')
    },
    editMoreLinksAddContent() {
    return this
      .waitForElementVisible('@moreLinksTitle')
      .setValue('@moreLinksTitle',"Title")
      .waitForElementVisible('@moreLinksUrl')
      .setValue('@moreLinksUrl',"http://www.url.com")
    },
  editMoreLinksDelete() {
    return this
      .waitForElementVisible('@moreLinksDeleteButton')
      .click('@moreLinksDeleteButton')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    moreLinksAddButton: {
      selector: 'a[href="/profilelink/edit.html"]'
    },
    moreLinksDeleteButton: {
      selector: '#leftcol > div > ul:nth-child(3) > li > a:nth-child(3) > img'
    },
    moreLinksTitle: {
      selector: '#title'
    },
    moreLinksUrl: {
      selector: '#url'
    }
  }
};
