const pageCommands = {
  editResEquipment() {
    return this
      .waitForElementVisible('@researchEquipmentEditLabel')
      .click('@researchEquipmentEditLabel')
  },
  editResEquipmentAdd() {
    return this
    .waitForElementVisible('@researchEquipmentAddButton')
    .click('@researchEquipmentAddButton')
  },
  editResEquipmentAddContent() {
      return this
      .waitForElementVisible('@researchEquipmentEquipmentAndTesting')
      .clearValue( '@researchEquipmentEquipmentAndTesting')
      .setValue( '@researchEquipmentEquipmentAndTesting', "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
      .clearValue( '@researchEquipmentPrimaryContact')
      .setValue( '@researchEquipmentPrimaryContact', "Name Name")
      .clearValue( '@researchEquipmentContactPhoneNumber')
      .setValue( '@researchEquipmentContactPhoneNumber', "000111222333")
      .clearValue( '@researchEquipmentCampusAddress')
      .setValue( '@researchEquipmentCampusAddress', "Address Address 77")
  },
  editResEquipmentSort() {
    return this
      .waitForElementVisible('@researchEquipmentSortButton')
      .click('@researchEquipmentSortButton')
    },
    editResEquipmentDelete() {
    return this
      .waitForElementVisible('@researchEquipmentDeleteContentButton')
      .click('@researchEquipmentDeleteContentButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    researchEquipmentEditLabel: {
      selector: 'a[href="index.html#equipment"]'
    },
    researchEquipmentAddButton: {
      selector: 'a[href="/equipment/edit.html"]'
    },
    researchEquipmentSortButton: {
      selector: 'a[href="/equipment/sort.html"]'
    },
    researchEquipmentDeleteContentButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    //
    researchEquipmentEquipmentAndTesting: {
      selector: '#summary'
    },
    researchEquipmentPrimaryContact: {
      selector: '#contact'
    },
    researchEquipmentContactPhoneNumber: {
      selector: '#phone'
    },
    researchEquipmentCampusAddress: {
      selector: '#address'
    },

    }
};
