const pageCommands = {
  editResResearch() {
    return this
      .waitForElementVisible('@researchEditLabel')
      .click('@researchEditLabel')
  },
  editResResearchAdd() {
    return this
    .waitForElementVisible('@researchAddButton')
    .click('@researchAddButton')
  },
  editResResearchAddContent() {
      return this
      .waitForElementVisible('@researchProjectTitle')
      .clearValue( '@researchProjectTitle')
      .setValue( '@researchProjectTitle', "Title of the project")
      .clearValue( '@researchProjectNumber')
      .setValue( '@researchProjectNumber', "13")
      .clearValue( '@researchPI')
      .setValue( '@researchPI', "PI Name Surname")
      .clearValue( '@researchCoPls')
      .setValue( '@researchCoPls', "Co PI One, Co Pi Two")
      .click('@researchYourRoleOnProject')
      .waitForElementVisible('@researchYourRoleOnProjectPI')
      .click('@researchYourRoleOnProjectPI')
      .clearValue( '@researchURL')
      .setValue( '@researchURL', "http://www.url.com")
      .clearValue( '@researchFundingStartDate')
      .setValue( '@researchFundingStartDate', "01/01/2000")
      .clearValue( '@researchFundingEndDate')
      .setValue( '@researchFundingEndDate', "01/01/2020")
      .clearValue( '@researchComments')
      .setValue( '@researchComments', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.")
      .click('@researchInternationalCbox')
      .waitForElementVisible('@researchInternationalCboxCountry')
      .clearValue( '@researchInternationalCboxCountry')
      .setValue( '@researchInternationalCboxCountry', "Country")
      .click('@researchInterdisciplinaryCbox')
      .click('@researchCommunityEngagementCbox')
      .click('@researchPublishCbox')
  },
  editResResearchSort() {
    return this
      .waitForElementVisible('@researchSortButton')
      .click('@researchSortButton')
    },
  editResResearchShowHide() {
    return this
      .waitForElementVisible('@researchShowHideButton')
      .click('@researchShowHideButton')
  },
  editResResearchShowHideSelect() {
    return this
      .waitForElementVisible('@researchShowHideBox')
      .click('@researchShowHideBox')
    },
  removeOneResearch() {
    return this
      .waitForElementVisible('@removeOneResearchButton')
      .click('@removeOneResearchButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    researchEditLabel: {
      selector: 'a[href="index.html#projects_unfunded"]'
    },
    researchAddButton: {
      selector: 'a[href="/projects_unfunded/edit.html"]'
    },
    researchSortButton: {
      selector: 'a[href="/projects_unfunded/sort.html"]'
    },
    researchShowHideButton: {
      selector: 'a[href="/projects_unfunded/visible.html"]'
    },
     removeOneResearchButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    //
    researchProjectTitle: {
      selector: '#projectTitle'
    },
    researchProjectNumber: {
      selector: '#projectNumber'
    },
    researchPI: {
      selector: '#principalInvestigator'
    },
    researchCoPls: {
      selector: '#coPrincipalInvestigators'
    },
    researchYourRoleOnProject: {
      selector: '#projectRole'
    },
    researchYourRoleOnProjectPI: {
      selector: "//option[text()='PI']",
      locateStrategy: 'xpath'
    },
    researchURL: {
      selector: '#url'
    },
    researchFundingStartDate: {
      selector: '#fundingPeriodStart'
    },
    researchFundingEndDate: {
      selector: '#fundingPeriodEnd'
    },
    researchComments: {
      selector: '#comments'
    },
    //
    researchInternationalCbox: {
      selector: '#checked_0'
    },
    researchInternationalCboxCountry: {
      selector: '#value_0'
    },
    researchInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    researchCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    researchPublishCbox: {
      selector: '#displayed1'
    },
     researchShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    }

  }
};
