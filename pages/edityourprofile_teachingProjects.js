const pageCommands = {
  editTeaTeachingProjects() {
    return this
      .waitForElementVisible('@teachingProjectsEditLabel')
      .click('@teachingProjectsEditLabel')
  },
  editTeaTeachingProjectsAdd() {
    return this
      .waitForElementVisible('@teachingProjectsAddButton')
      .click('@teachingProjectsAddButton')
  },
  editTeaTeachingProjectsAddContent() {
      return this
        .waitForElementVisible('@teachingProjectsProjectTitle')
        .clearValue('@teachingProjectsProjectTitle')
        .setValue('@teachingProjectsProjectTitle',"Project Title")
        .clearValue('@teachingProjectsProjectLead')
        .setValue('@teachingProjectsProjectLead',"Lead Name")
        .click('@teachingProjectsAddMoreCollaboratorsButton')
        .waitForElementVisible('@teachingProjectsCollaborator1')
        .clearValue('@teachingProjectsCollaborator1')
        .setValue('@teachingProjectsCollaborator1',"Name1 Surname1")
        .waitForElementVisible('@teachingProjectsCollaborator2')
        .clearValue('@teachingProjectsCollaborator2')
        .setValue('@teachingProjectsCollaborator2',"Name2 Surname2")
        .clearValue('@teachingProjectsRAsSupported')
        .setValue('@teachingProjectsRAsSupported',"2")
        .clearValue('@teachingProjectsSourceOfFunds')
        .setValue('@teachingProjectsSourceOfFunds',"My source")
        .clearValue('@teachingProjectsStartDate')
        .setValue('@teachingProjectsStartDate',"01/01/2001")
        .clearValue('@teachingProjectsEndDate')
        .setValue('@teachingProjectsEndDate',"01/12/2001")
        .clearValue('@teachingProjectsTotalProjectBudget')
        .setValue('@teachingProjectsTotalProjectBudget',"10000")
        .clearValue('@teachingProjectsYourShareOfTotalProjectBudget')
        .setValue('@teachingProjectsYourShareOfTotalProjectBudget',"3000")
        .clearValue('@teachingProjectsTotalExpendituresForFARPeriod')
        .setValue('@teachingProjectsTotalExpendituresForFARPeriod',"1000")
        .clearValue('@teachingProjectsYourShareOfExpendituresInDollars')
        .setValue('@teachingProjectsYourShareOfExpendituresInDollars',"800")
        .clearValue('@teachingProjectsComments')
        .setValue('@teachingProjectsComments',"Comments")
        .click('@teachingProjectsInternationalCbox')
        .waitForElementVisible('@teachingProjectsInternationalCountry')
        .clearValue('@teachingProjectsInternationalCountry')
        .setValue('@teachingProjectsInternationalCountry',"Country")
        .click('@teachingProjectsInterdisciplinaryCbox')
        .click('@teachingProjectsCommunityEngagementCbox')
        .click('@teachingProjectsPublishCbox')
        .click('@teachingProjectsPublishCbox')
  },
  editTeaTeachingProjectsSort() {
    return this
      .waitForElementVisible('@teachingProjectsSortButton')
      .click('@teachingProjectsSortButton')
    },
  editTeaTeachingProjectsShowHide() {
    return this
      .waitForElementVisible('@teachingProjectsShowHideButton')
      .click('@teachingProjectsShowHideButton')
  },
   editTeaTeachingProjectsShowHideSelect() {
    return this
      .waitForElementVisible('@teachingProjectsShowHideSelect')
      .click('@teachingProjectsShowHideSelect')
  },
  editTeaTeachingProjectsCancel() {
    return this
      .waitForElementVisible('@teachingProjectsCancelButton')
      .click('@teachingProjectsCancelButton')
  },
 removeOneTeachingProjects() {
    return this
      .waitForElementVisible('@removeOneTeachingProjectsButton')
      .click('@removeOneTeachingProjectsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    teachingProjectsEditLabel: {
      selector: 'a[href="index.html#teachprj"]'
    },
    teachingProjectsAddButton: {
      selector: 'a[href="/teachprj/edit.html"]'
    },
    teachingProjectsSortButton: {
      selector: 'a[href="/teachprj/sort.html"]'
    },
    teachingProjectsShowHideButton: {
      selector: 'a[href="/teachprj/visible.html"]'
    },
    removeOneTeachingProjectsButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    //
    teachingProjectsProjectTitle: {
      selector: '#title'
    },
    teachingProjectsProjectLead: {
      selector: '#lead'
    },
    teachingProjectsCollaborator1: {
      selector: '#collaborators0'
    },
    teachingProjectsCollaborator2: {
      selector: '#collaborators1'
    },
    teachingProjectsCollaborator3: {
      selector: '#collaborators2'
    },
    teachingProjectsAddMoreCollaboratorsButton: {
      selector: '#applybtn'
    },
    teachingProjectsRAsSupported: {
      selector: '#numberSupported'
    },
    teachingProjectsSourceOfFunds: {
      selector: '#sourceOfFunds'
    },
    teachingProjectsStartDate: {
      selector: '#startDate'
    },
    teachingProjectsEndDate: {
      selector: '#endDate'
    },
    teachingProjectsTotalProjectBudget: {
      selector: '#totalFunding'
    },
    teachingProjectsYourShareOfTotalProjectBudget: {
      selector: '#totalShareOfFunding'
    },
    teachingProjectsTotalExpendituresForFARPeriod: {
      selector: '#totalExpenditureForPeriod'
    },
    teachingProjectsYourShareOfExpendituresInDollars: {
      selector: '#totalShareOfExpenditures'
    },
    teachingProjectsComments: {
      selector: '#comments'
    },
    teachingProjectsInternationalCbox: {
      selector: '#checked_0'
    },
    teachingProjectsInternationalCountry: {
      selector: '#value_0'
    },
    teachingProjectsInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    teachingProjectsCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    teachingProjectsPublishCbox: {
      selector: '#displayed1'
    },
    teachingProjectsCancelButton: {
      selector: 'input[name="_cancel"]'
    },
    teachingProjectsShowHideSelect: {
      selector: 'input[name="list[0].displayed"]'
    },

  }
};
