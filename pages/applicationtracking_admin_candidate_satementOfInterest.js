const pageCommands = {
  clickCandidateStatement(){
    return this
      .waitForElementVisible('@candidateStatementFirstLabel')
      .click('@candidateStatementFirstLabel')
  },
  fillCandidateStatement() {
    return this
      .waitForElementVisible('@summaryCandidateStatement')
      .clearValue('@summaryCandidateStatement')
      .setValue('@summaryCandidateStatement',`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam ac sem id dapibus. Donec ut vehicula leo. Nulla auctor mi velit, non suscipit justo semper at. Nunc dolor tellus, varius at urna nec, gravida luctus felis. Donec neque ex, gravida eu lacus in, eleifend varius purus. Aliquam consequat, quam eu pharetra fringilla, justo nisl lacinia risus, ac lobortis purus tortor ac enim. Mauris sit amet laoreet arcu. Mauris venenatis egestas imperdiet. In semper pellentesque elit et scelerisque. Nulla euismod sapien fermentum odio lacinia interdum. Morbi rhoncus iaculis consequat. Fusce et fermentum augue, sit amet aliquam dui. Integer placerat sapien in felis convallis, a feugiat dui maximus. Maecenas a condimentum dui, eget fermentum velit. Fusce ante metus, varius ac justo id, aliquam mattis ex. Cras volutpat velit diam.`)
  },
  checkIsCandidateStatementOpen() {
    return this
      .waitForElementVisible('@appCandidateStatementLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    candidateStatementFirstLabel: {
      selector: "a[href='academic_interests.html']"
    },
    appCandidateStatementLabel: {
      selector: "//h2[text()='Candidate Statement of Interest']",
      locateStrategy: 'xpath'
    },
    summaryCandidateStatement: {
      selector: '#summary'
    },
  }
};
