const pageCommands = {
  clickPastAcademic(){
    return this
      .waitForElementVisible('@pastAcademicFirstLabel')
      .click('@pastAcademicFirstLabel')
  },
  plusPastAcademic() {
    return this
    .waitForElementVisible('@pastAcademicEditButton')
    .click('@pastAcademicEditButton')
  },
  fillPastAcademic() {
    return this
    .waitForElementVisible('@institutionPastAcademic')
    .click('@institutionPastAcademic')
    .waitForElementVisible('@institutionInputPastAcademic')
    .setValue('@institutionInputPastAcademic',"univ")
    .waitForElementVisible('@institutionAalborgPastAcademic')
    .click('@institutionAalborgPastAcademic')
    .waitForElementVisible('@rankPastAcademic')
    .click('@rankPastAcademic')
    .waitForElementVisible('@rankPastAcademicAssistant')
    .click('@rankPastAcademicAssistant')
    .waitForElementVisible('@startDatePastAcademic')
    .clearValue('@startDatePastAcademic')
    .setValue('@startDatePastAcademic',"06/06/2015")
    .waitForElementVisible('@endDatePastAcademic')
    .clearValue('@endDatePastAcademic')
    .setValue('@endDatePastAcademic',"11/01/2016")

  },
  cancelPastAcademic() {
    return this
      .waitForElementVisible('@cancelPastAcademicButton')
      .click('@cancelPastAcademicButton')
  },
  savePastAcademic() {
    return this
      .waitForElementVisible('@savePastAcademicButton')
      .click('@savePastAcademicButton')
  },
  saveAndAddPastAcademic() {
    return this
      .waitForElementVisible('@saveAndAddPastAcademicButton')
      .click('@saveAndAddPastAcademicButton')
  },
  removeOnePastAcademic() {
    return this
      .waitForElementVisible('@removeOnePastAcademicButton')
      .click('@removeOnePastAcademicButton')
  },

  checkIsPastAcademicOpen() {
    return this
      .waitForElementVisible('@appPastAcademicLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
  pastAcademicFirstLabel: {
      selector: 'a[href="past_appointments.html"]'
    },
    appPastAcademicLabel: {
      selector: "//h2[text()='Past Academic Appointments']",
      locateStrategy: 'xpath'
    },
    pastAcademicEditButton: {
      selector: 'a[href="past_appointment_edit.html"]'
    },
    institutionPastAcademic: {
      selector: '//*[@id="s2id_institution.id"]',
      locateStrategy:'xpath'
    },
    institutionInputPastAcademic: {
      selector: 'input[id="s2id_autogen1_search"]'
    },
    institutionAalborgPastAcademic: {
     selector: '#select2-result-label-2'
    },
    rankPastAcademic: {
      selector: '#rank_id_chosen'
    },
    rankPastAcademicAssistant: {
      selector: "//li[text()='Assistant Professor']",
      locateStrategy: 'xpath'
    },
    startDatePastAcademic: {
      selector: 'input[name="start"]'
    },
    endDatePastAcademic:{
      selector: 'input[name="end"]'
    },
    removeOnePastAcademicButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
    cancelPastAcademicButton: {
      selector: 'input[value="Cancel"]'
    },
    savePastAcademicButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddPastAcademicButton: {
      selector: 'input[value="Save & Add Another"]'
    },


  }
};
