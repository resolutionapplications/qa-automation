const pageCommands = {
  clickDemographicInfo(){
    return this
      .waitForElementVisible('@demographicFirstLabel')
      .click('@demographicFirstLabel')
  },
  fillDemographicInfo() {
     this
      .waitForElementVisible('@demographicInformationQuestion1')
      .click('@demographicInformationQuestion1')
      .waitForElementVisible('@demographicInformationQuestion1Yes')
      .click('@demographicInformationQuestion1Yes')
      .click('@demographicInformationQuestion2')
      .waitForElementVisible('@demographicInformationQuestion2No')
      .click('@demographicInformationQuestion2No')
      .click('@demographicInformationQuestion3')
      .waitForElementVisible('@demographicInformationQuestion3No')
      .click('@demographicInformationQuestion3No')
      this.api.pause(4000);
      this.click('@demographicInformationQuestion4')
      .waitForElementVisible('@demographicInformationQuestion4No')
      .click('@demographicInformationQuestion4No')
      .click('@demographicInformationQuestion4No')
      .waitForElementVisible('@demographicInformationQuestion5')
      .click('@demographicInformationQuestion5')
      .click('@demographicInformationQuestion5')
      .waitForElementVisible('@demographicInformationQuestion5No')
      .click('@demographicInformationQuestion5No')
      .waitForElementVisible('@saveContinueDemographicInfoButton')
      .click('@saveContinueDemographicInfoButton')
      return this;
  },
  checkIsDemographicOpen() {
    return this
      .waitForElementVisible('@appDemographicLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    demographicFirstLabel: {
      selector: "a[href='self_disclosure.html?id=9002']"
    },
    appDemographicLabel: {
      selector: "//h2[text()='Demographic Information']",
      locateStrategy: 'xpath'
    },
    demographicInformationQuestion1: {
      selector: '#answer_chosen'
    },
    demographicInformationQuestion1Yes: {
      selector: "//li[text()='Yes']",
      locateStrategy: 'xpath'
    },
    demographicInformationQuestion2: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/fieldset/div[2]',
      locateStrategy:'xpath'
    },
    demographicInformationQuestion2No: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/fieldset/div[2]/div/ul/li[2]',
      locateStrategy: 'xpath'
    },
    demographicInformationQuestion3: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/fieldset/div[3]',
      locateStrategy:'xpath'
    },
    demographicInformationQuestion3No: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/fieldset/div[3]/div/ul/li[2]',
      locateStrategy: 'xpath'
    },
    demographicInformationQuestion4: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/fieldset/div[4]',
      locateStrategy:'xpath'
    },
    demographicInformationQuestion4No: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/fieldset/div[4]/div/ul/li[2]',
      locateStrategy: 'xpath'
    },
    demographicInformationQuestion5: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/fieldset/div[5]',
      locateStrategy:'xpath'
    },
    demographicInformationQuestion5No: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div/div/form/fieldset/div[5]/div/ul/li[2]',
      locateStrategy: 'xpath'
    },
    saveContinueDemographicInfoButton: {
      selector: 'input[name="_continue"]'
    }

  }
};
