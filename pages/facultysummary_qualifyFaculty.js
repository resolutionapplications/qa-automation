const pageCommands = {
  facultyQualifyFaculty(){
    return this
      .waitForElementVisible('@qualifyFacultyLabel')
      .click('@qualifyFacultyLabel')
  },
  facultyQualifyFacultyAdd(){
    return this
      .waitForElementVisible('@qualifyFacultyAddButton')
      .click('@qualifyFacultyAddButton')
  },
  facultyQualifyFacultyAddContent() {
    return this
    .waitForElementVisible('@qualifyContentArea')
    .click('@qualifyContentArea')
    .waitForElementVisible('@qualifyInput')
    .setValue('@qualifyInput',"t")
    .waitForElementVisible('@qualifyInputClick')
    .click('@qualifyInputClick')
    .waitForElementVisible('@qualifyPath1')
    .click('@qualifyPath1')
  },
  removeOneQualifyFaculty(){
    return this
      .waitForElementVisible('@removeOneQualifyFacultyButton')
      .click('@removeOneQualifyFacultyButton')
  },
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    qualifyFacultyLabel: {
      selector: "a[href='qualify_faculty.html?emplid=RA102153']"
    },
    qualifyFacultyAddButton: {
      selector: "a[href='qualify_faculty_edit.html?emplid=RA102153']"
    },
    qualifyContentArea: {
      selector: '#cars_chosen'
    },
    qualifyInput: {
      selector: '#cars_chosen > div > div > input[type="text"]'
    },
    qualifyInputClick: {
      selector:'#cars_chosen > div > ul > li'
    },
    qualifyPath1: {
      selector: 'input[name="carpId"]'
    },
    removeOneQualifyFacultyButton: {
      selector:'img[src="images/icons/delete.png"]'
    }
  }
};
