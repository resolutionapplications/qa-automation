const pageCommands = {
  editBioBoards() {
    return this
      .waitForElementVisible('@boardsEditLabel')
      .click('@boardsEditLabel')
  },
  editBioBoardsAdd() {
    return this
      .waitForElementVisible('@boardsAddButton')
      .click('@boardsAddButton')
    },
  editBioBoardsAddContentFirstPart() {
     this
      .waitForElementVisible('@boardsBoard')
      .click('@boardsBoard')
      .waitForElementVisible('@boardsBoardInput')
      .click('@boardsBoardInput')
      .setValue('@boardsBoardInput',"vasc")
      .waitForElementVisible('@boardsBoardVascular')
      .click('@boardsBoardVascular')
      this.api.pause(2000);
      this.waitForElementVisible('@boardsSpecialty')
      .click('@boardsSpecialty')
      .waitForElementVisible('@boardsSpecialtyInputEndovascular')
      .click('@boardsSpecialtyInputEndovascular')
      .waitForElementVisible('@boardsCertifiedCbox')
      .click('@boardsCertifiedCbox')
      .clearValue('@boardsInitialCertificationDate')
      .setValue('@boardsInitialCertificationDate',"01/08/2000")
      .clearValue('@boardsValidThroughDate')
      .setValue('@boardsValidThroughDate',"01/08/2020")
      .click('@boardsLifetimeCbox')
      .click('@boardsPublishCbox')
      return this;
      },
  editBioBoardsSort() {
    return this
      .waitForElementVisible('@boardsSortButton')
      .click('@boardsSortButton')
    },
  editBioBoardsShowHide() {
    return this
      .waitForElementVisible('@boardsShowHideButton')
      .click('@boardsShowHideButton')
  },
  editBioBoardsShowHideSelect() {
    return this
      .waitForElementVisible('@boardsShowHideBox')
      .click('@boardsShowHideBox')
  },
  removeOneBoards() {
    return this
      .waitForElementVisible('@removeOneBoardsButton')
      .click('@removeOneBoardsButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    boardsEditLabel: {
      selector: 'a[href="index.html#boardspecialty"]'
    },
    boardsAddButton: {
      selector: 'a[href="/boardspecialty/edit.html"]'
    },
    boardsSortButton: {
      selector: 'a[href="/boardspecialty/sort.html"]'
    },
    boardsShowHideButton: {
      selector: 'a[href="/boardspecialty/visible.html"]'
    },
    boardsBoard: {
      selector: '#board_id_chosen'
    },
    boardsBoardInput: {
      selector: '#board_id_chosen > div:nth-child(2) > div:nth-child(1) > input:nth-child(1)'
    },
    boardsBoardVascular: {
      selector: "#board_id_chosen > div > ul > li:nth-child(1)"
    },
    boardsSpecialty: {
      selector: "#specialty_id_chosen"
    },
    // boardsSpecialtyInput: {
    //   selector: '#specialty_id_chosen > div:nth-child(2) > div:nth-child(1) > input:nth-child(1)'
    // },
    boardsSpecialtyInputEndovascular: {
      selector: '//li[text()="Endovascular Medicine"]',
      locateStrategy:'xpath'
    },
    boardsCertifiedCbox: {
      selector: '#certified1'
    },
    boardsInitialCertificationDate: {
      selector: "#certifiedDate"
    },
    boardsValidThroughDate: {
      selector: '#end'
    },
    boardsLifetimeCbox: {
      selector: '#lifetime1'
    },
    boardsPublishCbox: {
      selector: '#displayed1'
    },
    boardsShowHideBox: {
      selector: 'input[name="list[1].displayed"]'
    },
    removeOneBoardsButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    }

  }
};
