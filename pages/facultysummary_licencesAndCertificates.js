const pageCommands = {
  facultyLicencesCertificates(){
    return this
      .waitForElementVisible('@licencesCertificatesLabel')
      .click('@licencesCertificatesLabel')
  },
  facultyLicencesCertificatesAdd() {
    return this
    .waitForElementVisible('@licencesCertificatesAddButton')
    .click('@licencesCertificatesAddButton')
  },
  facultyLicencesCertificatesAddContent() {
    return this
    .waitForElementVisible('@typeLicences')
    .click('@typeLicences')
    .waitForElementVisible('@typeLicencesCertificates')
    .click('@typeLicencesCertificates')
    .clearValue('@titleLicences')
    .setValue('@titleLicences',"My licence title")
    .clearValue('@issuerLicences')
    .setValue('@issuerLicences',"My issuer")
    .clearValue('@stateLicences')
    .setValue('@stateLicences',"My state")
    .click('@countryLicences')
    .waitForElementVisible('@countryLicencesUS')
    .click('@countryLicencesUS')
    .clearValue('@licenceNumber')
    .setValue('@licenceNumber',"4564846")
    .clearValue('@dateIssuedLicences')
    .setValue('@dateIssuedLicences',"11/01/2010")
    .clearValue('@expirationDateLicences')
    .setValue('@expirationDateLicences',"11/01/2019")
    .click('@licenseStatusLicences')
    .waitForElementVisible('@licenseStatusLicencesCurrent')
    .click('@licenseStatusLicencesCurrent')
    .click('@fieldLicences')
    .waitForElementVisible('@fieldLicencesAcu')
    .click('@fieldLicencesAcu')
  },
   removeOneLicencesCertificates() {
    return this
      .waitForElementVisible('@removeOneLicencesCertificatesButton')
      .click('@removeOneLicencesCertificatesButton')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    licencesCertificatesLabel: {
      selector: "a[href='licenses_and_certs.html?emplid=RA102153']"
    },
    licencesCertificatesAddButton: {
      selector: 'a[href="license_and_cert_edit.html?emplid=RA102153"]'
    },
    typeLicences: {
      selector: '#type_id_chosen'
    },
    typeLicencesCertificates: {
      selector: '//li[text()="ABLS Instructor"]',
      locateStrategy: 'xpath'
    },
    titleLicences: {
      selector: '#title'
    },
    issuerLicences: {
      selector: '#issuer'
    },
    stateLicences: {
      selector: '#state'
    },
    countryLicences: {
      selector: '#country_id_chosen'
    },
    countryLicencesUS: {
      selector: "//li[text()='United States']",
      locateStrategy: 'xpath'
    },
    licenceNumber: {
      selector: '#number'
    },
    dateIssuedLicences: {
      selector: '#issued'
    },
    expirationDateLicences: {
      selector: '#expired'
    },
    licenseStatusLicences: {
      selector: '#status_id_chosen'
    },
    licenseStatusLicencesCurrent: {
      selector: "//li[text()='Current']",
      locateStrategy: 'xpath'
    },
    fieldLicences: {
      selector: '#field_id_chosen'
    },
    fieldLicencesAcu: {
      selector: "//li[text()='Acupuncturist']",
      locateStrategy: 'xpath'
    },
    verifiedLicences: {
      selector: "input[id='verified1']"
    },
     removeOneLicencesCertificatesButton: {
       selector: 'img[src="images/icons/delete.png"]'
     }
  }
};
