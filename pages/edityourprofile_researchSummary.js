const pageCommands = {
  editResSummary() {
    return this
      .waitForElementVisible('@resSummaryEditLabel')
      .click('@resSummaryEditLabel')
  },
  editResSummaryAdd() {
    return this
      .waitForElementVisible('@resSummaryAddButton')
      .click('@resSummaryAddButton')
  },
  editResSummaryAddContent() {
    return this
      .waitForElementVisible('@resSummaryContent')
      .clearValue('@resSummaryContent')
      .setValue('@resSummaryContent',"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
  },
  editResSummaryDelete() {
    return this
      .waitForElementVisible('@resSummaryDeleteButton')
      .click('@resSummaryDeleteButton')
    },
    editResSummaryCancel() {
    return this
      .waitForElementVisible('@resSummaryCancelButton')
      .click('@resSummaryCancelButton')
    },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    resSummaryEditLabel: {
      selector: 'a[href="index.html#summary"]'
    },
    resSummaryAddButton: {
      selector: 'a[href="/summary/edit.html"]'
    },
    resSummaryDeleteButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    resSummaryContent: {
      selector: '#paragraph'
    },
     resSummaryCancelButton: {
      selector: 'input[name="_cancel"]'
    }
    // paragraph
  }
};
