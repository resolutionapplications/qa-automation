const pageCommands = {
  editTeaProfessionalEducation() {
    return this
      .waitForElementVisible('@professionalEducationEditLabel')
      .click('@professionalEducationEditLabel')
  },
  editTeaProfessionalEducationAdd() {
    return this
      .waitForElementVisible('@professionalEducationAddButton')
      .click('@professionalEducationAddButton')
  },
  editTeaProfessionalEducationAddContent() {
      return this
        .waitForElementVisible('@professionalEducationClassName')
        .clearValue('@professionalEducationClassName')
        .setValue('@professionalEducationClassName',"Class Class")
        .click('@professionalEducationClassType')
        .waitForElementVisible('@professionalEducationClassTypeLecture')
        .click('@professionalEducationClassTypeLecture')
        .clearValue('@professionalEducationNumberOfHours')
        .setValue('@professionalEducationNumberOfHours',"40")
        .clearValue('@professionalEducationStartDate')
        .setValue('@professionalEducationStartDate',"01/01/2001")
        .clearValue('@professionalEducationEndDate')
        .setValue('@professionalEducationEndDate',"01/12/2005")
        .clearValue('@professionalEducationDescription')
        .setValue('@professionalEducationDescription',"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac interdum enim. Proin ac magna porta odio lacinia feugiat. Aenean varius ex at iaculis euismod.")
        .click('@professionalEducationPublishCbox')
  },
  editTeaProfessionalEducationSort() {
    return this
      .waitForElementVisible('@professionalEducationSortButton')
      .click('@professionalEducationSortButton')
    },
  editTeaProfessionalEducationShowHide() {
    return this
      .waitForElementVisible('@professionalEducationShowHideButton')
      .click('@professionalEducationShowHideButton')
  },
  editTeaProfessionalEducationCancel() {
    return this
      .waitForElementVisible('@professionalEducationCancelButton')
      .click('@professionalEducationCancelButton')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    professionalEducationEditLabel: {
      selector: 'a[href="index.html#professional_education"]'
    },
    professionalEducationAddButton: {
      selector: 'a[href="/professional_education/edit.html"]'
    },
    professionalEducationSortButton: {
      selector: 'a[href="/professional_education/sort.html"]'
    },
    professionalEducationShowHideButton: {
      selector: 'a[href="/professional_education/visible.html"]'
    },
    //
    professionalEducationClassName: {
      selector: '#name'
    },
    professionalEducationClassType: {
      selector: '#classType'
    },
    professionalEducationClassTypeLecture: {
      selector: "//option[text()='Lecture']",
      locateStrategy: 'xpath'
    },
    professionalEducationNumberOfHours: {
      selector: '#numHours'
    },
    professionalEducationStartDate: {
      selector: '#startDate'
    },
    professionalEducationEndDate: {
      selector: '#endDate'
    },
    professionalEducationDescription: {
      selector: '#description'
    },
    professionalEducationPublishCbox: {
      selector: '#displayed1'
    },
    professionalEducationCancelButton: {
      selector: 'input[name="_cancel"]'
    },

  }
};
