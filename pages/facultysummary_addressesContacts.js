const pageCommands = {
  facultyAddressesContacts() {
    return this
      .waitForElementVisible('@addressesContactsLabel')
      .click('@addressesContactsLabel')
  },
  facultyAddressesContactsAddAddress() {
    return this
    .waitForElementVisible('@addressesContactsAddAddress')
    .click('@addressesContactsAddAddress')
  },
  facultyAddressesContactsAddContact() {
    return this
    .waitForElementVisible('@addressContactsAddContact')
    .click('@addressContactsAddContact')
  },
  facultyAddressesContactsAddAddressContent() {
      return this
      .waitForElementVisible('@addressesContactsType')
      .click( '@addressesContactsType')
      .waitForElementVisible( '@addressesContactsOther')
      .click( '@addressesContactsOther')
      .clearValue( '@addressesContactsAddress1')
      .setValue( '@addressesContactsAddress1', "Street name")
      .clearValue( '@addressesContactsAddress2')
      .setValue( '@addressesContactsAddress2', "Street name second")
      .clearValue( '@addressesContactsAddress3')
      .setValue( '@addressesContactsAddress3', "Street name third")
      .clearValue( '@addressesContactsCity')
      .setValue( '@addressesContactsCity', "Citycity")
      .clearValue( '@addressesContactsState')
      .setValue( '@addressesContactsState', "BH")
      .click('@addressesContactsCountry')
      .waitForElementVisible('@addressesContactsCountryUS')
      .click( '@addressesContactsCountryUS')
      .clearValue('@addressesContactsZip')
      .setValue('@addressesContactsZip',"71000")
  },
  facultyAddressesContactsAddEmailContent() {
      return this
      .waitForElementVisible('@addressesContactsType')
      .click( '@addressesContactsType')
      .waitForElementVisible('@addressesContactsTypeEmail')
      .click('@addressesContactsTypeEmail')
      .waitForElementVisible('@addressesContactsDescription')
      .clearValue('@addressesContactsDescription')
      .setValue('@addressesContactsDescription',"Email")
      .waitForElementVisible('@addressesContactsEmail')
      .clearValue('@addressesContactsEmail')
      .setValue('@addressesContactsEmail',"my@gmail.com")
  },
  facultyAddressesContactsAddPhoneContent() {
      return this
      .waitForElementVisible('@addressesContactsType')
      .click( '@addressesContactsType')
      .waitForElementVisible('@addressesContactsTypePhone')
      .click('@addressesContactsTypePhone')
      .waitForElementVisible('@addressesContactsDescription')
      .clearValue('@addressesContactsDescription')
      .setValue('@addressesContactsDescription',"Office")
      .waitForElementVisible('@addressesContactsPhone')
      .clearValue('@addressesContactsPhone')
      .setValue('@addressesContactsPhone',"777-888-9999")
  },
  facultyAddressesContactsAddMobileContent() {
      return this
      .waitForElementVisible('@addressesContactsType')
      .click( '@addressesContactsType')
      .waitForElementVisible('@addressesContactsTypeMobile')
      .click('@addressesContactsTypeMobile')
      .waitForElementVisible('@addressesContactsDescription')
      .clearValue('@addressesContactsDescription')
      .setValue('@addressesContactsDescription',"Mobile")
      .waitForElementVisible('@addressesContactsPhone')
      .clearValue('@addressesContactsPhone')
      .setValue('@addressesContactsPhone',"061-061-0610")
  },
  removeOneAddressesContacts() {
    return this
      .waitForElementVisible('@removeOneAddressesContactsButton')
      .click('@removeOneAddressesContactsButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    addressesContactsLabel: {
      selector: 'a[href="address.html?emplid=RA102153"]'
    },
    addressesContactsAddAddress: {
      selector: 'a[href="address_edit.html?emplid=RA102153"]'
    },
    addressContactsAddContact: {
     selector:'a[href="contact_edit.html?emplid=RA102153&entity=100044"]'
    },
    //
    addressesContactsType: {
      selector: '#type_id_chosen'
    },
    addressesContactsOther: {
      selector: "//li[text()='Other']",
      locateStrategy: 'xpath'
    },
    addressesContactsAddress1: {
      selector: '#line1'
    },
    addressesContactsAddress2: {
      selector: '#line2'
    },
    addressesContactsAddress3: {
      selector: '#line3'
    },
     addressesContactsCity: {
      selector: '#city'
    },
    addressesContactsState: {
      selector: '#state'
    },
    addressesContactsCountry: {
      selector: '#country_chosen'
    },
    addressesContactsCountryUS: {
      selector: "//li[text()='United States']",
      locateStrategy: 'xpath'
    },
    addressesContactsZip: {
      selector: '#zip'
    },
     addressesContactsTypeEmail: {
      selector: "//li[text()='Email']",
      locateStrategy: 'xpath'
    },
    addressesContactsTypePhone: {
      selector: "//li[text()='Phone']",
      locateStrategy: 'xpath'
    },
    addressesContactsTypeMobile: {
      selector: "//li[text()='Mobile']",
      locateStrategy: 'xpath'
    },
    addressesContactsDescription: {
      selector: '#description'
    },
    addressesContactsEmail: {
      selector: '#validEmail'
    },
    addressesContactsPhone: {
      selector: '#validPhone'
    },
     removeOneAddressesContactsButton: {
      selector: 'img[src="images/icons/delete.png"]'
    },

  }
};
