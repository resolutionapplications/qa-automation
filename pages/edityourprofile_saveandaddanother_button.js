const pageCommands = {
  editProfileSaveAndAddAnother() {
    return this
      .waitForElementVisible('@saveAndAddAnotherButton')
      .click('@saveAndAddAnotherButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    saveAndAddAnotherButton: {
      selector: 'input[name="apply"]'
      //input.submitBtn:nth-child(9)
    },
  }
};
