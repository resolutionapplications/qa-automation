const pageCommands = {
  facultyLeadershipRoles(){
    return this
      .waitForElementVisible('@leadershipRolesLabel')
      .click('@leadershipRolesLabel')
  },
  facultyLeadershipRolesAdd() {
    return this
    .waitForElementVisible('@leadershipRolesAddButton')
    .click('@leadershipRolesAddButton')
  },
  facultyLeadershipRolesAddContent() {
    return this
      .waitForElementVisible('@serviceTypeLeadership')
      .click('@serviceTypeLeadership')
      .waitForElementVisible('@serviceTypeLeadershipWMed')
      .click('@serviceTypeLeadershipWMed')
      .click('@categoryLeadership')
      .waitForElementVisible('@categoryLeadershipAffiliated')
      .click('@categoryLeadershipAffiliated')
      .click('@roleLeadership')
      .waitForElementVisible('@roleLeadershipChair')
      .click('@roleLeadershipChair')
      .clearValue('@entityLeadership')
      .setValue('@entityLeadership',"My Entity")
      .clearValue('@startLeadership')
      .setValue('@startLeadership',"11/01/2000")
      .clearValue('@endLeadership')
      .setValue('@endLeadership',"11/08/2016")
      .clearValue('@hoursLeadership')
      .setValue('@hoursLeadership',"6")
      .click('@unitLeadership')
      .waitForElementVisible('@unitLeadershipDay')
      .click('@unitLeadershipDay')
      .clearValue('@descripitionLeadership')
      .setValue('@descripitionLeadership',`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam ac sem id dapibus. Donec ut vehicula leo. Nulla auctor mi velit, non suscipit justo semper at. Nunc dolor tellus, varius at urna nec, gravida luctus felis.`)


  },
  removeOneLeadershipRoles() {
    return this
      .waitForElementVisible('@removeOneLeadershiRolesButton')
      .click('@removeOneLeadershiRolesButton')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    leadershipRolesLabel: {
      selector: "a[href='service.html?emplid=RA102153']"
    },
    leadershipRolesAddButton: {
      selector: 'a[href="service_edit.html?emplid=RA102153"]'
    },
    serviceTypeLeadership: {
      selector: '#type_chosen'
    },
    serviceTypeLeadershipWMed: {
      selector: "//li[text()='WMed']",
      locateStrategy: 'xpath'
    },
    categoryLeadership: {
      selector: '#category_id_chosen'
    },
    categoryLeadershipAffiliated: {
      selector: "//li[text()='Affiliated Institutions']",
      locateStrategy: 'xpath'
    },
    roleLeadership: {
      selector: '#role_id_chosen'
    },
    roleLeadershipChair: {
      selector: "//li[text()='Chair']",
      locateStrategy: 'xpath'
    },
    entityLeadership: {
      selector: '#entity'
    },
    startLeadership: {
      selector: '#start'
    },
    endLeadership: {
      selector: '#end'
    },
    hoursLeadership: {
      selector: '#hours'
    },
    unitLeadership: {
      selector: '#unit_id_chosen'
    },
    unitLeadershipDay: {
      selector: "//li[text()='Day']",
      locateStrategy: 'xpath'
    },
    descripitionLeadership: {
      selector: "#description"
    },
    removeOneLeadershiRolesButton: {
      selector: 'img[src="images/icons/delete.png"]'
    }


  }
};
