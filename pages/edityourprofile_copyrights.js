const pageCommands = {
  ResCopyrights() {
    return this
      .waitForElementVisible('@copyrightsEditLabel')
      .click('@copyrightsEditLabel')
  },
  ResCopyrightsAdd() {
    return this
    .waitForElementVisible('@copyrightsAddButton')
    .click('@copyrightsAddButton')
  },
  ResCopyrightsAddContent() {
      return this
      .waitForElementVisible('@copyrightsWork')
      .clearValue( '@copyrightsWork')
      .setValue( '@copyrightsWork', "Copyrigthed work title")
      .clearValue( '@copyrightsDate')
      .setValue( '@copyrightsDate', "01/01/2007")
      .click( '@copyrightsMoreCopyrightHoldersButton')
      .waitForElementVisible('@copyrightsCopyrightHolder1')
      .clearValue( '@copyrightsCopyrightHolder1')
      .setValue( '@copyrightsCopyrightHolder1', "Name1 Surname1")
      .waitForElementVisible('@copyrightsCopyrightHolder2')
      .clearValue( '@copyrightsCopyrightHolder2')
      .setValue( '@copyrightsCopyrightHolder2', "Name2 Surname2")
      .clearValue( '@copyrightsComments')
      .setValue( '@copyrightsComments', "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ")
      .click('@copyrightsInternationalCbox')
      .waitForElementVisible('@copyrightsInternationalCboxCountry')
      .clearValue( '@copyrightsInternationalCboxCountry')
      .setValue( '@copyrightsInternationalCboxCountry', "Country")
      .click('@copyrightsInterdisciplinaryCbox')
      .click('@copyrightsCommunityEngagementCbox')
      .click('@copyrightsPublishCbox')
      .click('@copyrightsPublishCbox')
  },
  ResCopyrightsSort() {
    return this
      .waitForElementVisible('@copyrightsSortButton')
      .click('@copyrightsSortButton')
    },
  ResCopyrightsShowHide() {
    return this
      .waitForElementVisible('@copyrightsShowHideButton')
      .click('@copyrightsShowHideButton')
  },
  ResCopyrightsShowHideSelect() {
    return this
      .waitForElementVisible('@copyrightsShowHideBox')
      .click('@copyrightsShowHideBox')
    },
  removeOneCopyrights() {
    return this
      .waitForElementVisible('@removeOneCopyrightsButton')
      .click('@removeOneCopyrightsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    copyrightsEditLabel: {
      selector: 'a[href="index.html#cpyrght"]'
    },
    copyrightsAddButton: {
      selector: 'a[href="/copyright/edit.html"]'
    },
    copyrightsSortButton: {
      selector: 'a[href="/copyright/sort.html"]'
    },
    copyrightsShowHideButton: {
      selector: 'a[href="/copyright/visible.html"]'
    },
    removeOneCopyrightsButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    //
    copyrightsWork: {
      selector: '#title'
    },
    copyrightsDate: {
      selector: '#date'
    },
    copyrightsMoreCopyrightHoldersButton: {
      selector: '#applybtn'
    },
    copyrightsCopyrightHolder1: {
      selector: '#holders0'
    },
    copyrightsCopyrightHolder2: {
      selector: '#holders1'
    },
    copyrightsComments: {
      selector: '#comments'
    },
    //
    copyrightsInternationalCbox: {
      selector: '#checked_0'
    },
    copyrightsInternationalCboxCountry: {
      selector: '#value_0'
    },
    copyrightsInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    copyrightsCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    copyrightsPublishCbox: {
      selector: '#displayed1'
    },
    copyrightsShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    }


  }
};
