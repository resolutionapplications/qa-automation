const pageCommands = {
  clickEducationHistory(){
    return this
      .waitForElementVisible('@educationHistoryFirstLabel')
      .click('@educationHistoryFirstLabel')
  },
  plusEducationHistory() {
    return this
    .waitForElementVisible('@educationEditButton')
    .click('@educationEditButton')
  },
  fillEducationHistory() {
   this
    .waitForElementVisible('@degreeType')
    .click('@degreeType')
    .waitForElementVisible('@degreeTypeDEGREE')
    .click('@degreeTypeDEGREE')
    this.api.pause(2000);
    this.waitForElementVisible('@degreeID')
    .click('@degreeID')
    .waitForElementVisible('@degreeIDAA')
    .click('@degreeIDAA')
    .waitForElementVisible('@subjectSpecialty')
    .click('@subjectSpecialty')
    .clearValue('@subjectSpecialtyInput')
    .setValue('@subjectSpecialtyInput',"c")
    this.api.pause(2000);
    this.waitForElementVisible('@subjectSpecialtyInputClick')
    .click('@subjectSpecialtyInputClick')
    .waitForElementVisible('@institutionID')
    .click('@institutionID')
    .waitForElementVisible('@institutionIDInput')
    .setValue('@institutionIDInput',"univ")
    this.api.pause(2000);
    this.waitForElementVisible('@institutionIDInputAalborg')
    .click('@institutionIDInputAalborg')
    .waitForElementVisible('@collegeName')
    .clearValue('@collegeName')
    .setValue('@collegeName',"Mycollege")
    .click('@startedMonth')
    .waitForElementVisible('@startedMonthJan')
    .click('@startedMonthJan')
    .clearValue('@startedYear')
    .setValue('@startedYear',"2000")
    .click('@issuedMonth')
    .waitForElementVisible('@issuedMonthFeb')
    .click('@issuedMonthFeb')
    .clearValue('@issuedYear')
    .setValue('@issuedYear',"2004")
    .click('@highestDegreeCheckbox')
    return this;
  },
  cancelEducationHistory() {
    return this
      .waitForElementVisible('@cancelEducationHistoryButton')
      .click('@cancelEducationHistoryButton')
  },
  saveEducationHistory() {
    return this
      .waitForElementVisible('@saveEducationHistoryButton')
      .click('@saveEducationHistoryButton')
  },
  saveAndAddEducationHistory() {
    return this
      .waitForElementVisible('@saveAndAddEducationHistoryButton')
      .click('@saveAndAddEducationHistoryButton')
  },

  removeOneEducationHistory() {
    return this
      .waitForElementVisible('@removeOneEducationHistoryButton')
      .click('@removeOneEducationHistoryButton')
  },

  checkIsEducationHistoryOpen() {
    return this
      .waitForElementVisible('@appEducationHistoryLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    educationHistoryFirstLabel: {
      selector: "a[href='education.html']"
    },
    appEducationHistoryLabel: {
      selector: "//h2[text()='Education History']",
      locateStrategy: 'xpath'
    },
    educationEditButton: {
      selector: 'a[href="education_edit.html"]'
    },
    degreeType: {
      selector: '#degree_type_chosen'
    },
    degreeTypeDEGREE: {
      selector: "//li[text()='DEGREE']",
      locateStrategy: 'xpath'
    },
    degreeID: {
      selector: '#degree_id_chosen'
    },
    degreeIDAA: {
      selector: "//li[text()='AB']",
      locateStrategy: 'xpath'
    },
    subjectSpecialty: {
      selector: '//*[@id="s2id_subject.id"]',
      locateStrategy:'xpath'
    },
    subjectSpecialtyInput: {
      selector: '#s2id_autogen2_search'
    },
    subjectSpecialtyInputClick: {
      selector: '//ul[@id="select2-results-2"]/li[1]',
      locateStrategy:'xpath'
    },
    institutionID: {
      selector: '//*[@id="s2id_institution.id"]',
      locateStrategy:'xpath'
    },
    institutionIDInput: {
      selector: 'input[id="s2id_autogen1_search"]'
    },
    institutionIDInputAalborg: {
    //  selector: '#select2-results-lable-104'
      selector: '//*[@id="select2-results-1"]/li[2]',
      locateStrategy:'xpath'
    },
    collegeName: {
      selector: 'input[name="collegeName"]'
    },
    startedMonth:{
      selector: 'select[name="startedMonth"]'
    },
    startedMonthJan: {
      selector: 'select[name="startedMonth"] option[value="1"]'
    },
    startedYear: {
      selector: 'input[name="startedYear"]'
    },
    issuedMonth:{
      selector: 'select[name="issuedMonth"]'
    },
    issuedMonthFeb: {
      selector: 'select[name="issuedMonth"] option[value="2"]'
    },
    issuedYear: {
      selector: 'input[name="issuedYear"]'
    },
    highestDegreeCheckbox:{
      selector: 'input[id="highestDegree1"]'
    },
    removeOneEducationHistoryButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
    cancelEducationHistoryButton: {
      selector: 'input[value="Cancel"]'
    },
    saveEducationHistoryButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddEducationHistoryButton: {
      selector: 'input[value="Save & Add Another"]'
    },


  }
};
