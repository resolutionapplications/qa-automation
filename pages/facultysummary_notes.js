const pageCommands = {
  facultyNotes() {
    return this
      .waitForElementVisible('@facultyNotesLabel')
      .click('@facultyNotesLabel')
  },
  facultyNotesAdd() {
    return this
      .waitForElementVisible('@facultyNotesAddButton')
      .click('@facultyNotesAddButton')
  },
  facultyNotesAddContent() {
    return this
      .waitForElementVisible('@facultyNotesType')
      .click('@facultyNotesType')
      .waitForElementVisible('@facultyNotesTypeMedical')
      .click('@facultyNotesTypeMedical')
      .waitForElementVisible('@facultyNotesComment')
      .clearValue('@facultyNotesComment')
      .setValue('@facultyNotesComment',"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.")
      .click('@facultyNotesPrivate')
      .click('@facultyNotesPrivate')
  },
  facultyNotesAddContentUser() {
    return this
      .waitForElementVisible('@facultyNotesType')
      .click('@facultyNotesType')
      .waitForElementVisible('@facultyNotesTypeUserID')
      .click('@facultyNotesTypeUserID')
      .waitForElementVisible('@facultyNotesComment')
      .clearValue('@facultyNotesComment')
      .setValue('@facultyNotesComment',"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
      .click('@facultyNotesPrivate')
  },
  facultyNotesFilter() {
    return this
      .waitForElementVisible('@facultyNotesType2')
      .click('@facultyNotesType2')
      .waitForElementVisible('@facultyNotesTypeMedical')
      .click('@facultyNotesTypeMedical')
      .waitForElementVisible('@facultyFilterButton')
      .click('@facultyFilterButton')
  },
  facultyNotesSearch() {
    return this
      .waitForElementVisible('@facultyNotesNote')
      .setValue('@facultyNotesNote',"Duis aute")
      .waitForElementVisible('@facultyFilterButton')
      .click('@facultyFilterButton')
  },
   facultyClearSearch() {
    return this
      .waitForElementVisible('@facultyClearButton')
      .click('@facultyClearButton')
  },
  removeOneNotes() {
    return this
      .waitForElementVisible('@removeOneNotesButton')
      .click('@removeOneNotesButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    facultyNotesLabel: {
      selector: 'a[href="notes.html?emplid=RA102153"]'
    },
    facultyNotesAddButton: {
      selector: 'a[href="notes_edit.html?emplid=RA102153&entity=100037"]'
    },
    facultyNotesType: {
      selector: '#type_id_chosen'
    },
    facultyNotesType2: {
      selector: '#type_chosen'
    },
    facultyNotesTypeMedical: {
      selector: "//li[text()='Medical License']",
      locateStrategy: 'xpath'
    },
    facultyNotesTypeUserID: {
      selector: "//li[text()='UserIDs']",
      locateStrategy: 'xpath'
    },
    facultyNotesComment: {
      selector: '#description'
    },
    facultyNotesPrivate: {
      selector: '#privateComment1'
    },
    removeOneNotesButton: {
      selector: 'img[src="images/icons/delete.png"]'
    },
    facultyFilterButton: {
      selector:'input[value="Filter"]'
    },
    facultyClearButton: {
      selector:'input[value="Clear Search Criteria"]'
    },
    facultyNotesNote: {
      selector: '#note'
    }

  }
};
