const pageCommands = {
  editResExpertise() {
    return this
      .waitForElementVisible('@expertiseEditLabel')
      .click('@expertiseEditLabel')
  },
  editResExpertiseAdd() {
    return this
      .waitForElementVisible('@expertiseAddButton')
      .click('@expertiseAddButton')
  },
  editResExpertiseAddContent() {
      return this
        .waitForElementVisible('@expertiseKeyword')
        .click('@expertiseKeyword')
        .setValue('@expertiseKeyword',"Exobiology")
  },
  editResExpertiseSort() {
    return this
      .waitForElementVisible('@expertiseSortButton')
      .click('@expertiseSortButton')
    },
  editResExpertiseShowHide() {
    return this
      .waitForElementVisible('@expertiseShowHideButton')
      .click('@expertiseShowHideButton')
  },
  editResExpertiseShowHideSelect() {
    return this
      .waitForElementVisible('@expertiseShowHideBox')
      .click('@expertiseShowHideBox')
    },
  editResExpertiseDelete() {
    return this
      .waitForElementVisible('@expertiseDeleteButton')
      .click('@expertiseDeleteButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    expertiseEditLabel: {
      selector: 'a[href="index.html#keywords"]'
    },
    expertiseAddButton: {
      selector: 'a[href="/keyword/edit.html"]'
    },
    expertiseSortButton: {
      selector: 'a[href="/keyword/sort.html"]'
    },
    expertiseShowHideButton: {
      selector: 'a[href="/keyword/visible.html"]'
    },
    expertiseDeleteButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    expertiseKeyword: {
      selector: '#topic_input'
    },
    expertiseShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    }
    //


  }
};
