const pageCommands = {
  clickTranscript(){
    return this
      .waitForElementVisible('@transcriptsRequiredFirstLabel')
      .click('@transcriptsRequiredFirstLabel')
  }, 
  fillTranscriptRequired(){
    return this
      .waitForElementVisible('@transcriptsRequiredYes')
      .click('@transcriptsRequiredYes')
  },

  checkIsTranscriptOpen() {
    return this
      .waitForElementVisible('@appTranscriptsRequiredLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    transcriptsRequiredFirstLabel: {
      selector: "a[href='self_disclosure.html?id=9003']"
    },
    appTranscriptsRequiredLabel: {
      selector: "//h2[text()='Transcripts Required?']",
      locateStrategy: 'xpath'
    },
    transcriptsRequiredYes: {
      selector: "input[name='questions[0].value']"
    }


  }
};
