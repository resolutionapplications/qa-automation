const pageCommands = {
  facultySaveAndAddAnother() {
    return this
      .waitForElementVisible('@saveAndAddAnotherButton')
      .click('@saveAndAddAnotherButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    saveAndAddAnotherButton: {
      selector: 'input[name="_add"]'
    },
  }
};
