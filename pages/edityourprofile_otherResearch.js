const pageCommands = {
  editResOther() {
    return this
      .waitForElementVisible('@resOtherEditLabel')
      .click('@resOtherEditLabel')
  },
  editResOtherAdd() {
    return this
      .waitForElementVisible('@resOtherEditContentButton')
      .click('@resOtherEditContentButton')
    },
  editResOtherAddContent() {
      return this
        .waitForElementVisible('@resOtherTitle')
        .clearValue('@resOtherTitle')
        .setValue('@resOtherTitle',"Other (Research)")
  },
  editResOtherDelete() {
    return this
      .waitForElementVisible('@resOtherDeleteContentButton')
      .click('@resOtherDeleteContentButton')
    },

};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
   resOtherEditLabel: {
      selector: 'a[href="index.html#profileotherresearch"]'
    },
    resOtherEditContentButton: {
      selector: 'a[href="/profileotherresearch/edit.html"]'
    },
    resOtherDeleteContentButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },

    resOtherTitle: {
      selector: '#title'
    },
    resOtherContent: {
      selector: '#cke_paragraph'
    },
  }
};
