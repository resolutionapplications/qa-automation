const pageCommands = {
  facultySummarySave() {
    return this
      .waitForElementVisible('@saveButton')
      .click('@saveButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    saveButton: {
      selector: 'input[type=submit]'
    }
  }
};
