const pageCommands = {
  clickAttestSignature(){
    return this
      .waitForElementVisible('@attestSignatureFirstLabel')
      .click('@attestSignatureFirstLabel')
  },
  fillAttestSignature(){
    return this
      .waitForElementVisible('@signature')
      .clearValue('@signature')
      .setValue('@signature',"Namen Surnames")

  },
  checkIsAttestSignatureOpen() {
    return this
      .waitForElementVisible('@appAttestSignatureLabel')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    attestSignatureFirstLabel: {
      selector: 'a[href="signature.html"]'
    },
    appAttestSignatureLabel: {
      selector: "//h2[text()='Attestation and Signature']",
      locateStrategy: 'xpath'
    },
    signature:{
      selector: '#sig'
    }
  }
};
