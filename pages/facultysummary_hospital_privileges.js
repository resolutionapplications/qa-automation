const pageCommands = {
  facultyHospitalPrivileges(){
    return this
      .waitForElementVisible('@hospitalPrivilegesLabel')
      .click('@hospitalPrivilegesLabel')
  },
  facultyHospitalPrivilegesAdd() {
    return this
    .waitForElementVisible('@hospitalPrivilegesAddButton')
    .click('@hospitalPrivilegesAddButton')
  },
  facultyHospitalPrivilegesAddContent() {
    return this
    .waitForElementVisible('@hospital')
    .click('@hospital')
    .waitForElementVisible('@hospitalBorgess')
    .click('@hospitalBorgess')
    .clearValue('@cityHospital')
    .setValue('@cityHospital',"Hospitalcity")
    .clearValue('@stateHospital')
    .setValue('@stateHospital',"My")
  },
  removeOneHospitalPrivileges() {
    return this
      .waitForElementVisible('@removeOneHospitalPrivilegesButton')
      .click('@removeOneHospitalPrivilegesButton')
  },

  
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    hospitalPrivilegesLabel: {
      selector: "a[href='hospital_privileges.html?emplid=RA102153']"
    },
    hospitalPrivilegesAddButton: {
      selector: 'a[href="hospital_privilege_edit.html?emplid=RA102153"]'
    },
    hospital: {
      selector: '#hospital_type_chosen'
    },
    hospitalBorgess: {
      selector: "//li[text()='Borgess']",
      locateStrategy: 'xpath'
    },
    cityHospital: {
      selector: '#city'
    },
    stateHospital: {
      selector: '#state'
    },
    removeOneHospitalPrivilegesButton: {
      selector: 'img[src="images/icons/delete.png"]'
    },
    


  }
};
