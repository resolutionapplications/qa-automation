const pageCommands = {
  editBioOther() {
    return this
      .waitForElementVisible('@otherEditLabel')
      .click('@otherEditLabel')
  },
  editBioOtherAdd() {
    return this
      .waitForElementVisible('@otherEditContentButton')
      .click('@otherEditContentButton')
    },
  editBioOtherAddContent() {
      return this
        .waitForElementVisible('@otherTitle')
        .clearValue('@otherTitle')
        .setValue('@otherTitle',"Other (Biography)")
  },
  editBioOtherDelete() {
    return this
      .waitForElementVisible('@otherDeleteButton')
      .click('@otherDeleteButton')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    otherEditLabel: {
      selector: 'a[href="index.html#profileotherbio"]'
    },
    otherEditContentButton: {
      selector: 'a[href="/profileotherbio/edit.html"]'
    },
    otherDeleteButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    otherTitle: {
      selector: '#title'
    },
    otherContent: {
      selector: '#cke_paragraph'
    },
  }
};
