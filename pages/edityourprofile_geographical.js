const pageCommands = {
  editBioGeographical() {
    return this
      .waitForElementVisible('@geographicalEditLabel')
      .click('@geographicalEditLabel')
  },
  editBioGeographicalAdd() {
    return this
      .waitForElementVisible('@geographicalAddButton')
      .click('@geographicalAddButton')
    },
    editBioGeographicalAddContentFirst(downArrow, enter) {
      return this
        .waitForElementVisible('@geographicalRegion')
        .click('@geographicalRegion')
        .sendKeys('@geographicalRegion',downArrow)
        .sendKeys('@geographicalRegion',downArrow)
        .sendKeys('@geographicalRegion',downArrow)
        .sendKeys('@geographicalRegion',enter)
        // for (var i = 0; i < 4; i++) {
        //    this.sendKeys('@geographicalRegion',downArrow)
        // }
        .waitForElementVisible('@geographicalComments')
        .clearValue('@geographicalComments')
        .setValue('@geographicalComments',"Comment on region one")
        .click('@geographicalPublish')
        .click('@geographicalPublish')
      },
      editBioGeographicalAddContentSecond(downArrow, enter) {
        return this
          .waitForElementVisible('@geographicalRegion')
          .click('@geographicalRegion')
          .sendKeys('@geographicalRegion',downArrow)
          .sendKeys('@geographicalRegion',downArrow)
          .sendKeys('@geographicalRegion',downArrow)
          .sendKeys('@geographicalRegion',downArrow)
          .sendKeys('@geographicalRegion',downArrow)
          .sendKeys('@geographicalRegion',downArrow)
          .sendKeys('@geographicalRegion',enter)
          // .waitForElementVisible('@geographicalRegionGhana')
          // .click('@geographicalRegionGhana')
          .waitForElementVisible('@geographicalComments')
          .clearValue('@geographicalComments')
          .setValue('@geographicalComments',"Comment on region two")
          .click('@geographicalPublish')
          .click('@geographicalPublish')
        },
  editBioGeographicalSort() {
    return this
      .waitForElementVisible('@geographicalSortButton')
      .click('@geographicalSortButton')
    },
  editBioGeographicalShowHide() {
    return this
      .waitForElementVisible('@geographicalShowHideButton')
      .click('@geographicalShowHideButton')
  },
  editBioGeographicalShowHideSelect() {
    return this
      .waitForElementVisible('@geographicalShowHideBox')
      .click('@geographicalShowHideBox')
  },
  removeOneGeographical() {
    return this
      .waitForElementVisible('@removeOneGeographicalButton')
      .click('@removeOneGeographicalButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    geographicalEditLabel: {
      selector: 'a[href="index.html#region"]'
    },
    geographicalAddButton: {
      selector: 'a[href="/region/edit.html"]'
    },
    geographicalSortButton: {
      selector: 'a[href="/region/sort.html"]'
    },
    geographicalShowHideButton: {
      selector: 'a[href="/region/visible.html"]'
    },
    geographicalRegion: {
      selector: 'select[name="region.keywordId"]'
    },
    geographicalRegionCapeVerde: {
      // selector: "//*[text()='            Cape Verde']",
      // locateStrategy: 'xpath'
      selector: '#region\.keywordId > option:nth-child(5)'
    },
    geographicalRegionGhana: {
      // selector: "//option[text()='            Ghana']",
      // locateStrategy: 'xpath'
      selector: '#region\.keywordId > option:nth-child(8)'
    },
    geographicalComments: {
      selector: '#comments'
    },
    geographicalPublish: {
      selector: '#displayed1'
    },
    geographicalShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
    removeOneGeographicalButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
