const pageCommands = {
  editCV() {
    return this
      .waitForElementVisible('@cvEditButton')
      .click('@cvEditButton')
  },
  editCVNoCV() {
    return this
      .waitForElementVisible('@noCVCbox')
      .click('@noCVCbox')
  },
  editCVWithOptionalPublicCV() {
    return this
      .waitForElementVisible('@optionalPublicCVCbox')
      .click('@optionalPublicCVCbox')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    cvEditButton: {
      selector: 'a[href="/cv/edit.html?cvedit="]'
    },
    noCVCbox: {
      selector: '#displayType3'
    },
    optionalPublicCVCbox: {
      selector: '#displayType2'
    },
    browseCVButton: {
      selector: 'input[type="file"]'
    },

  }
};
