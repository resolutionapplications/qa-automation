const pageCommands = {
  facultyMemberships(){
    return this
      .waitForElementVisible('@membershipsLabel')
      .click('@membershipsLabel')
  },
  facultyMembershipsAdd() {
    return this
    .waitForElementVisible('@membershipsAddButton')
    .click('@membershipsAddButton')
  },
  facultyMembershipsAddContent() {
    return this
      .waitForElementVisible('@organizationMembership')
      .click('@organizationMembership')
      .waitForElementVisible('@organizationMembershipOther')
      .click('@organizationMembershipOther')
      .clearValue('@organizationDescriptionMembership')
      .setValue('@organizationDescriptionMembership',"Description")
      .clearValue('@startMembership')
      .setValue('@startMembership',"11/01/2000")
      .clearValue('@endMembership')
      .setValue('@endMembership',"11/01/2012")
  },
  removeOneMembership() {
    return this
      .waitForElementVisible('@removeOneMembershipButton')
      .click('@removeOneMembershipButton')
  }

};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    membershipsLabel: {
      selector: "a[href='memberships.html?emplid=RA102153']"
    },
    membershipsAddButton: {
      selector: "a[href='membership_edit.html?emplid=RA102153']"
    },
    organizationMembership: {
      selector: '#organization_id_chosen'
    },
    organizationMembershipOther: {
      selector: "//li[text()='Other']",
      locateStrategy: 'xpath'
    },
    organizationDescriptionMembership: {
      selector: '#organizationDescription'
    },
    startMembership: {
      selector: '#start'
    },
    endMembership: {
      selector: '#end'
    },
    removeOneMembershipButton: {
      selector: 'img[src="images/icons/delete.png"]'
    },
   


  }
};
