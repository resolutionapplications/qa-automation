const pageCommands = {
  editBioEducation() {
    return this
      .waitForElementVisible('@educationEditLabel')
      .click('@educationEditLabel')
  },
  editBioEducationAdd() {
    return this
      .waitForElementVisible('@educationAddButton')
      .click('@educationAddButton')
    },
  editBioEducationAddContentFirstPart() {
     this
        .waitForElementVisible('@educationType')
        .click('@educationType')
        .waitForElementVisible('@educationTypeDEGREE')
        .click('@educationTypeDEGREE')
        this.api.pause(2000);
        this.waitForElementVisible('@educationDegree')
        .click('@educationDegree')
        .waitForElementVisible('@educationDegreeAA')
        .click('@educationDegreeAA')
        .clearValue('@educationSubject')
        .setValue('@educationSubject',"Subject")
        .waitForElementVisible('@educationInstitution')
        .click('@educationInstitution')
        .waitForElementVisible('@educationInstitutionInput')
        .setValue('@educationInstitutionInput',"univ")
        this.api.pause(2000);
        this.waitForElementVisible('@educationInstitutionInputClick')
        .click('@educationInstitutionInputClick')
        .clearValue('@educationCollege')
        .setValue('@educationCollege',"College")
        .clearValue('@educationStartedYear')
        .setValue('@educationStartedYear',"1991")
        .clearValue('@educationIssuedYear')
        .setValue('@educationIssuedYear',"1995")
        .click('@educationStartedMonth')
        .waitForElementVisible('@educationStartedMonthJanuary')
        .click('@educationStartedMonthJanuary')
        .click('@educationIssuedMonth')
        .waitForElementVisible('@educationIssuedMonthFeb')
        .click('@educationIssuedMonthFeb')
        .click('@educationPublishCbox')
        .click('@educationPublishCbox')
        return this;
      },

  editBioEducationSort() {
    return this
      .waitForElementVisible('@educationSortButton')
      .click('@educationSortButton')
    },
  editBioEducationShowHide() {
    return this
      .waitForElementVisible('@educationShowHideButton')
      .click('@educationShowHideButton')
  },
  editBioEducationShowHideSelect() {
    return this
      .waitForElementVisible('@educationShowHideBox')
      .click('@educationShowHideBox')
  },
  removeOneEducation() {
    return this
      .waitForElementVisible('@removeOneEducationButton')
      .click('@removeOneEducationButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    educationEditLabel: {
      selector: 'a[href="index.html#education"]'
    },
    educationAddButton: {
      selector: 'a[href="/education/edit.html"]'
    },
    educationSortButton: {
      selector: 'a[href="/education/sort.html"]'
    },
    educationShowHideButton: {
      selector: 'a[href="/education/visible.html"]'
    },
    educationType: {
      selector: '#type_id_chosen'
    },
    educationTypeDEGREE: {
      selector: "//li[text()='DEGREE']",
      locateStrategy: 'xpath'
    },
    educationDegree: {
      selector: '#degree_id_chosen'
    },
    educationDegreeAA: {
      selector: "//li[text()='AA']",
      locateStrategy: 'xpath'
    },
    educationInstitution: {
      selector: '//*[@id="s2id_institution.id"]',
      locateStrategy:'xpath'
    },
    educationInstitutionInput: {
      selector: '#s2id_autogen1_search'
    },
    educationInstitutionInputClick: {
      selector: '#select2-result-label-2'
    },
    educationSubject: {
      selector: 'input[name="subject.description"]'
    },
    educationCollege: {
      selector: '#collegeName'
    },
    educationStartedYear: {
      selector: '#startedYear'
    },
    educationIssuedYear: {
      selector: '#issuedYear'
    },
    educationStartedMonth: {
      selector: 'div.columnNarrow:nth-child(21) > div:nth-child(2)'
    },
    educationIssuedMonth: {
      selector: 'div.columnNarrow:nth-child(24) > div:nth-child(2)'
    },
    educationStartedMonthJanuary: {
      selector: "//li[text()='Jan.']",
      locateStrategy: 'xpath'
    },
    educationIssuedMonthFeb: {
      selector: 'div.columnNarrow:nth-child(24) > div:nth-child(2) > div.chosen-drop > ul.chosen-results > li:nth-child(3)'
    },
    educationPublishCbox: {
      selector: '#displayed1'
    },
    educationShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
    removeOneEducationButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    }

  }
};
