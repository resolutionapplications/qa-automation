const pageCommands = {
  clickPublication(){
    return this
      .waitForElementVisible('@publicationFirstLabel')
      .click('@publicationFirstLabel')
  },
  plusPublication() {
    return this
    .waitForElementVisible('@publicationEditButton')
    .click('@publicationEditButton')
  },
  fillPublication() {
    return this
    .waitForElementVisible('@titlePublication')
    .clearValue('@titlePublication')
    .setValue('@titlePublication',"My title")
    .clearValue('@publisherPublication')
    .setValue('@publisherPublication',"My publisher")
    .waitForElementVisible('@typePublication')
    .click('@typePublication')
    .waitForElementVisible('@typePublicationBook')
    .click('@typePublicationBook')
    .waitForElementVisible('@statusPublication')
    .click('@statusPublication')
    .waitForElementVisible('@statusPublicationSubmitted')
    .click('@statusPublicationSubmitted')
    .clearValue('@datePublication')
    .setValue('@datePublication',"2010/11/01")
    .clearValue('@volumePublication')
    .setValue('@volumePublication',"My volume")
    .clearValue('@leadAuthorPublication')
    .setValue('@leadAuthorPublication',"My lead author")
    .clearValue('@coAuthorsPublication')
    .setValue('@coAuthorsPublication',"My co-authors name")
    .click('@addCoAuthorsButton')
    .waitForElementVisible('@coAuthors1Publication')
    .clearValue('@coAuthors1Publication')
    .setValue('@coAuthors1Publication',"My co-authors name2")
    .clearValue('@pagesPublication')
    .setValue('@pagesPublication',"250")
    .clearValue('@abstractPublication')
    .setValue('@abstractPublication',"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam ac sem id dapibus. Donec ut vehicula leo. Nulla auctor mi velit, non suscipit justo semper at.")
    .clearValue('@urlPublication')
    .setValue('@urlPublication',"http://www.url.com")
    .clearValue('@placePublication')
    .setValue('@placePublication',"My place")
    .clearValue('@yearPublication')
    .setValue('@yearPublication',"2010")
  },
  removeOnePublication() {
    return this
    .waitForElementVisible('@removeOnePublicationButton')
    .click('@removeOnePublicationButton')
  },
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    publicationFirstLabel: {
      selector: "a[href='publication.html?emplid=RA102153']"
    },
    publicationEditButton: {
      selector: 'a[href="publication_edit.html?emplid=RA102153"]'
    },
    typePublication: {
      selector: '#type_id_chosen'
    },
    typePublicationBook: {
      selector: "//li[text()='Book']",
      locateStrategy: 'xpath'
    },
    titlePublication: {
      selector: '#title'
    },
    publisherPublication: {
      selector: '#publisher'
    },
    statusPublication: {
        selector: '#status_id_chosen'
    },
    statusPublicationSubmitted: {
      selector: "//li[text()='Submitted']",
      locateStrategy: 'xpath'
    },
    datePublication: {
      selector:  '#date'
    },
    volumePublication: {
      selector: '#volume'
    },
    leadAuthorPublication: {
      selector: '#leadAuthorEditor'
    },
    coAuthorsPublication: {
      selector: '#coAuthorEditors0'
    },
    coAuthors1Publication: {
      selector: '#coAuthorEditors1'
    },
    pagesPublication: {
      selector: '#pages'
    },
    abstractPublication: {
      selector: '#abstractText'
    },
    urlPublication: {
      selector: '#url'
    },
    placePublication: {
      selector: '#place'
    },
    yearPublication: {
      selector: '#year'
    },
    addCoAuthorsButton: {
      selector: 'input[name="addco"]'
    },
    removeOnePublicationButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
  }
};
