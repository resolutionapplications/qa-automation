const pageCommands = {
  editBioHonours() {
    return this
      .waitForElementVisible('@honoursEditLabel')
      .click('@honoursEditLabel')
  },
  editBioHonoursAdd() {
    return this
      .waitForElementVisible('@honoursAddButton')
      .click('@honoursAddButton')
    },
    editBioHonoursAddContent() {
      return this
        .waitForElementVisible('@honoursOrganization')
        .clearValue('@honoursOrganization')
        .setValue('@honoursOrganization',"Name of the organization")
        .clearValue('@honoursDescription')
        .setValue('@honoursDescription',"Lorem ipsum dolor sit amet.")
        .clearValue('@honoursDate')
        .setValue('@honoursDate',"01/07/2000")
        .click('@honoursInternationalCbox')
        .waitForElementVisible('@honoursInternationalCountry')
        .clearValue('@honoursInternationalCountry')
        .setValue('@honoursInternationalCountry',"C Name")
        .click('@honoursInterdisciplinaryCbox')
        .click('@honoursCommunityCbox')
        .click('@honoursPublish')
        .click('@honoursPublish')
      },
  editBioHonoursSort() {
    return this
      .waitForElementVisible('@honoursSortButton')
      .click('@honoursSortButton')
    },
  editBioHonoursShowHide() {
    return this
      .waitForElementVisible('@honoursShowHideButton')
      .click('@honoursShowHideButton')
  },
  editBioHonoursShowHideSelect() {
    return this
      .waitForElementVisible('@honoursShowHideBox')
      .click('@honoursShowHideBox')
  },
  removeOneHonours() {
    return this
      .waitForElementVisible('@removeOneHonoursButton')
      .click('@removeOneHonoursButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    honoursEditLabel: {
      selector: 'a[href="index.html#accolade"]'
    },
    honoursAddButton: {
      selector: 'a[href="/accolade/edit.html"]'
    },
    honoursSortButton: {
      selector: 'a[href="/accolade/sort.html"]'
    },
    honoursShowHideButton: {
      selector: 'a[href="/accolade/visible.html"]'
    },

    honoursOrganization: {
      selector: '#organization'
    },
    honoursDescription: {
      selector: '#description'
    },
    honoursDate: {
      selector: '#preciseDateReceived'
    },
    honoursInternationalCbox: {
      selector: '#checked_0'
    },
    honoursInternationalCountry: {
      selector: '#value_0'
    },
    honoursInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    honoursCommunityCbox: {
      selector: '#checked_2'
    },
    honoursPublish: {
      selector: '#displayed1'
    },
    honoursShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
    removeOneHonoursButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    }

  }
};
