const pageCommands = {
  editFacPatientSatisfaction() {
    return this
      .waitForElementVisible('@patientSatisfactionEditLabel')
      .click('@patientSatisfactionEditLabel')
  },
  editFacPatientSatisfactionAdd() {
    return this
    .waitForElementVisible('@patientSatisfactionAddButton')
    .click('@patientSatisfactionAddButton')
  },
  editFacPatientSatisfactionAddContent() {
      return this
      .waitForElementVisible('@patientSatisfactionDescription')
      .clearValue( '@patientSatisfactionDescription')
      .setValue( '@patientSatisfactionDescription', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.")
      .clearValue( '@patientSatisfactionDate')
      .setValue( '@patientSatisfactionDate', "01/01/2000")
      .clearValue( '@patientSatisfactionURL')
      .setValue( '@patientSatisfactionURL', "http://www.url.com")
      .click('@patientSatisfactionInternationalCbox')
      .waitForElementVisible('@patientSatisfactionInternationalCboxCountry')
      .clearValue( '@patientSatisfactionInternationalCboxCountry')
      .setValue( '@patientSatisfactionInternationalCboxCountry', "Country")
      .click('@patientSatisfactionCommunityEngagementCbox')
  },
  editFacFacPatientSatisfactionSort() {
    return this
      .waitForElementVisible('@patientSatisfactionSortButton')
      .click('@patientSatisfactionSortButton')
    },
  editFacFacPatientSatisfactionShowHide() {
    return this
      .waitForElementVisible('@patientSatisfactionShowHideButton')
      .click('@patientSatisfactionShowHideButton')
  },
  editFacFacPatientSatisfactionShowHideSelect() {
    return this
      .waitForElementVisible('@patientSatisfactionShowHideBox')
      .click('@patientSatisfactionShowHideBox')
    },
    removeOnePatientSatisfaction() {
    return this
      .waitForElementVisible('@removeOnePatientSatisfactionButton')
      .click('@removeOnePatientSatisfactionButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    patientSatisfactionEditLabel: {
      selector: 'a[href="index.html#patientsatisfaction"]'
    },
    patientSatisfactionAddButton: {
      selector: 'a[href="/patientsatisfaction/edit.html"]'
    },
    patientSatisfactionSortButton: {
      selector: 'a[href="/patientsatisfaction/sort.html"]'
    },
    patientSatisfactionShowHideButton: {
      selector: 'a[href="/patientsatisfaction/visible.html"]'
    },
    //
    patientSatisfactionDescription: {
      selector: '#description'
    },
    patientSatisfactionURL: {
      selector: '#url'
    },
    patientSatisfactionDate: {
      selector: '#date'
    },
    //
    patientSatisfactionInternationalCbox: {
      selector: '#checked_0'
    },
    patientSatisfactionInternationalCboxCountry: {
      selector: '#value_0'
    },
    patientSatisfactionCommunityEngagementCbox: {
      selector: '#checked_1'
    },
     patientSatisfactionShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
    removeOnePatientSatisfactionButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }

  }
};
