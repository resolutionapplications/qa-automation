const pageCommands = {
  clickAttachments(){
    return this
      .waitForElementVisible('@attachmentsFirstLabel')
      .click('@attachmentsFirstLabel')
  },
    ClickAddLetter(){
    return this
      .waitForElementVisible('@addLetterButton')
      .click('@addLetterButton')
  },
    ClickAddAdditionalDocument(){
    return this
      .waitForElementVisible('@addAdditionalDocumentButton')
      .click('@addAdditionalDocumentButton')
  },

  checkIsAttachmentsOpen() {
    return this
      .waitForElementVisible('@appAttachmentsLabel')
  },
  removeOneAttachments() {
    return this
      .waitForElementVisible('@removeOneAttachmentsButton')
      .click('@removeOneAttachmentsButton')
  },
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    attachmentsFirstLabel: {
      selector: "a[href='attachments.html']"
    },
    appAttachmentsLabel: {
      selector: "//h1[text()='Required Attachments']",
      locateStrategy: 'xpath'
    },
    removeOneAttachmentsButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
    addLetterButton: {
      selector: '#add_letter'
    },
    addAdditionalDocumentButton: {
      selector: '#add_additional_doc'
    },


  }
};
