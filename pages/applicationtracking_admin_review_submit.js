const pageCommands = {
  clickReviewSubmit(){
    return this
      .waitForElementVisible('@reviewSubmitFirstLabel')
      .click('@reviewSubmitFirstLabel')
  },
  fillReviewSubmit(){
    return this


  },
  checkIsReviewSubmitOpen() {
    return this
      .waitForElementVisible('@appReviewSubmitLabel')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    reviewSubmitFirstLabel: {
      selector: "a[href='review_submit.html']"
    },
    appReviewSubmitLabel: {
      selector: "//h1[text()='Review and Submit']",
      locateStrategy: 'xpath'
    },

  }
};
