const pageCommands = {
  clickBoardCertifications(){
    return this
      .waitForElementVisible('@boardCertificationsFirstLabel')
      .click('@boardCertificationsFirstLabel')
  },
  plusBoardCertifications() {
    return this
    .waitForElementVisible('@boardCertificationsEditButton')
    .click('@boardCertificationsEditButton')
  },
  fillBoardCertifications() {
     this
    .waitForElementVisible('@board')
    .click('@board')
    .waitForElementVisible('@boardChosen')
    .click('@boardChosen')
    this.api.pause(2000);
    this.waitForElementVisible('@boardsSpecialty')
    .click('@boardsSpecialty')
    .waitForElementVisible('@boardsSpecialtyEndovascular')
    .click('@boardsSpecialtyEndovascular')
    .click('@boardCertified')
    .click('@lifetime')
    .clearValue('@initialCertificationDate')
    .setValue('@initialCertificationDate',"24/10/2000")
    .clearValue('@validThrough')
    .setValue('@validThrough',"24/10/2016")
    return this;
  },
  cancelBoardCertifications() {
    return this
      .waitForElementVisible('@cancelBoardCertificationsButton')
      .click('@cancelBoardCertificationsButton')
  },
  saveBoardCertifications() {
    return this
      .waitForElementVisible('@saveBoardCertificationsButton')
      .click('@saveBoardCertificationsButton')
  },
  saveAndAddBoardCertifications() {
    return this
      .waitForElementVisible('@saveAndAddBoardCertificationsButton')
      .click('@saveAndAddBoardCertificationsButton')
  },
  removeOneBoardCertifications() {
    return this
      .waitForElementVisible('@removeOneBoardCertificationsButton')
      .click('@removeOneBoardCertificationsButton')
  },

  checkIsBoardCertificationsOpen() {
    return this
      .waitForElementVisible('@appBoardCertificationssLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    boardCertificationsFirstLabel: {
      selector: "a[href='board_certifications.html']"
    },
    appBoardCertificationssLabel: {
      selector: "//h2[text()='Board Certifications']",
      locateStrategy: 'xpath'
    },
    boardCertificationsEditButton: {
      selector: 'a[href="board_certifications_edit.html"]'
    },
    board: {
      selector: '#boards_chosen'
    },
    boardChosen: {
      selector: "//li[text()='Amercian Board of Vascular Medicine']",
      locateStrategy: 'xpath'
    },
    boardsSpecialty: {
      selector: "#specialty_id_chosen"
    },
    boardsSpecialtyEndovascular: {
      selector: "//li[text()='Endovascular Medicine']",
      locateStrategy: 'xpath'
    },
    boardCertified: {
      selector: '#certified1'
    },
    lifetime: {
      selector: '#lifetime1'
    },
    initialCertificationDate: {
      selector: '#certifiedDate'
    },
    validThrough: {
      selector: '#end'
    },
    removeOneBoardCertificationsButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
    cancelBoardCertificationsButton: {
      selector: 'input[value="Cancel"]'
    },
    saveBoardCertificationsButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddBoardCertificationsButton: {
      selector: 'input[value="Save & Add Another"]'
    },


  }
};
