const pageCommands = {
  confirmLogin() {
    return this
      .waitForElementPresent('@logout')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    logout: {
        selector:  "//a[text()='Logout']",
         locateStrategy: 'xpath'
    }
  }
};
