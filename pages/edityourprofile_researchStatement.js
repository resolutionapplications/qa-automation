const pageCommands = {
  editResStatement() {
    return this
      .waitForElementVisible('@resStatementEditLabel')
      .click('@resStatementEditLabel')
  },
  editResStatementAdd() {
    return this
      .waitForElementVisible('@resStatementAddButton')
      .click('@resStatementAddButton')
  },
  editResStatementDelete() {
    return this
      .waitForElementVisible('@resStatementDeleteButton')
      .click('@resStatementDeleteButton')
    },
    editResStatementCancel() {
    return this
      .waitForElementVisible('@resStatementCancelButton')
      .click('@resStatementCancelButton')
    },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    resStatementEditLabel: {
      selector: 'a[href="index.html#statement"]'
    },
    resStatementAddButton: {
      selector: 'a[href="/statement/edit.html"]'
    },
    resStatementDeleteButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
     resStatementCancelButton: {
      selector: 'input[name="_cancel"]'
    },
    // paragraph
  }
};
