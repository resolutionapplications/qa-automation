const pageCommands = {
  clickAreasOfInterest(){
    return this
      .waitForElementVisible('@areasOfInterestFirstLabel')
      .click('@areasOfInterestFirstLabel')
  },
  plusAreasOfInterest() {
    return this
    .waitForElementVisible('@areasOfInterestEditButton')
    .click('@areasOfInterestEditButton')
  },
  fillAreasOfInterest() {
    return this
    .waitForElementVisible('@keyword')
    .click('@keyword')
    .waitForElementVisible('@keywordInput')
    .setValue('@keywordInput',"v")
    .waitForElementVisible('@keywordClick')
    .click('@keywordClick')
    .waitForElementVisible('@classificationClinical')
    .click('@classificationClinical')
    .click('@classificationConditions')
    .click('@classificationResearch')
    .click('@classificationTeaching')
    .click('@classificationService')

  },
  cancelAreasOfInterest() {
    return this
      .waitForElementVisible('@cancelAreasOfInterestButton')
      .click('@cancelAreasOfInterestButton')
  },
  saveAreasOfInterest() {
    return this
      .waitForElementVisible('@saveAreasOfInterestButton')
      .click('@saveAreasOfInterestButton')
  },
  saveAndAddAreasOfInterest() {
    return this
      .waitForElementVisible('@saveAndAddAreasOfInterestButton')
      .click('@saveAndAddAreasOfInterestButton')
  },
  removeOneAreasOfInterest() {
    return this
      .waitForElementVisible('@removeOneAreasOfInterestButton')
      .click('@removeOneAreasOfInterestButton')
  },

  checkIsAreasOfInterestOpen() {
    return this
      .waitForElementVisible('@appAreasOfInterestLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    areasOfInterestFirstLabel: {
      selector: "a[href='research_interest.html']"
    },
    appAreasOfInterestLabel: {
      selector: "//h2[text()='Areas of Expertise']",
      locateStrategy: 'xpath'
    },
    areasOfInterestEditButton: {
      selector: 'a[href="research_interest_edit.html"]'
    },
    keyword: {
      selector: '#s2id_id'
    },
    keywordInput: {
      selector: '#s2id_autogen1_search'
    },
    keywordClick: {
      selector: '#select2-result-label-3'
    },
    classificationClinical: {
      selector: '#updatedClassifications1'
    },
    classificationConditions: {
      selector: '#updatedClassifications2'
    },
    classificationResearch: {
      selector: '#updatedClassifications3'
    },
    classificationTeaching: {
      selector: '#updatedClassifications4'
    },
    classificationService: {
      selector: '#updatedClassifications5'
    },

    removeOneAreasOfInterestButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
    cancelAreasOfInterestButton: {
      selector: 'input[value="Cancel"]'
    },
    saveAreasOfInterestButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddAreasOfInterestButton: {
      selector: 'input[value="Save & Add Another"]'
    },


  }
};
