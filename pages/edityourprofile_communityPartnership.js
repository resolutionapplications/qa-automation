const pageCommands = {
  editCommunityPartnership() {
    return this
    .waitForElementVisible('@communityPartnershipEditLabel')
    .click('@communityPartnershipEditLabel')
  },
  editCommunityPartnershipAdd() {
    return this
    .waitForElementVisible('@communityPartnershipAddButton')
    .click('@communityPartnershipAddButton')
  },
  editCommunityPartnershipAddContent() {
      return this
      .waitForElementVisible('@communityPartnership')
      .clearValue('@communityPartnership')
      .setValue('@communityPartnership',"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.")
      .clearValue( '@communityPartnershipStartDate')
      .setValue( '@communityPartnershipStartDate', "01/01/2000")
      .clearValue( '@communityPartnershipEndDate')
      .setValue( '@communityPartnershipEndDate', "01/01/2020")
      .clearValue('@communityPartnershipHours')
      .setValue('@communityPartnershipHours',"6")
      .click('@communityPartnershipInternationalCbox')
      .waitForElementVisible('@communityPartnershipInternationalCboxCountry')
      .clearValue( '@communityPartnershipInternationalCboxCountry')
      .setValue( '@communityPartnershipInternationalCboxCountry', "Country")
      .click('@communityPartnershipInterdisciplinaryCbox')
      .click('@communityPartnershipCommunityEngagementCbox')
      .click('@communityPartnershipPublishCbox')
      .click('@communityPartnershipPublishCbox')

  },
  editCommunityPartnershipSort() {
    return this
      .waitForElementVisible('@communityPartnershipSortButton')
      .click('@communityPartnershipSortButton')
    },
 editCommunityPartnershipShowHide() {
    return this
      .waitForElementVisible('@communityPartnershipShowHideButton')
      .click('@communityPartnershipShowHideButton')
  },
  editCommunityPartnershipShowHideSelect() {
    return this
      .waitForElementVisible('@communityPartnershipShowHideBox')
      .click('@communityPartnershipShowHideBox')
    },
  removeOneCommunityPartnership() {
    return this
      .waitForElementVisible('@removeOneCommunityPartnershipButton')
      .click('@removeOneCommunityPartnershipButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    communityPartnershipEditLabel: {
      selector:'a[href="index.html#comservice"]'
    },
    communityPartnershipAddButton: {
      selector: 'a[href="/comservice/edit.html"]'
    },
    communityPartnershipSortButton: {
      selector: 'a[href="/comservice/sort.html"]'
    },
    communityPartnershipShowHideButton: {
      selector: 'a[href="/comservice/visible.html"]'
    },
    communityPartnership: {
      selector: "#servicePerformed"
    },
    communityPartnershipStartDate: {
      selector: '#startDate'
    },
    communityPartnershipEndDate: {
      selector: '#endDate'
    },
    communityPartnershipHours: {
      selector: '#hours'
    },
    communityPartnershipInternationalCbox: {
      selector: '#checked_0'
    },
    communityPartnershipInternationalCboxCountry: {
      selector: '#value_0'
    },
    communityPartnershipInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    communityPartnershipCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    communityPartnershipPublishCbox: {
      selector: '#displayed1'
    },
    communityPartnershipShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
   removeOneCommunityPartnershipButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
