const pageCommands = {
  facultyExpertise() {
    return this
      .waitForElementVisible('@expertiseLabel')
      .click('@expertiseLabel')
  },
  facultyExpertiseAdd() {
    return this
      .waitForElementVisible('@expertiseAddButton')
      .click('@expertiseAddButton')
  },
 facultyExpertiseAddContent() {
      return this
        .waitForElementVisible('@expertiseKeyword')
        .click('@expertiseKeyword')
        .waitForElementVisible('@expertiseKeywordInput')
        .setValue('@expertiseKeywordInput',"univ")
        .waitForElementVisible('@expertiseKeywordUnivClick')
        .click('@expertiseKeywordUnivClick')
        .waitForElementVisible('@expertiseClinical')
        .click('@expertiseClinical')
        
  },
  removeOneExpertise() {
    return this
      .waitForElementVisible('@removeOneExpertiseButton')
      .click('@removeOneExpertiseButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    expertiseLabel: {
      selector: 'a[href="research_interest.html?emplid=RA102153"]'
    },
    expertiseAddButton: {
      selector: 'a[href="research_interest_edit.html?emplid=RA102153"]'
    },
    removeOneExpertiseButton: {
      selector: 'img[src="images/icons/delete.png"]'
    },
    expertiseKeyword: {
      selector: '#s2id_id'
    },
    expertiseKeywordInput: {
      selector: '#s2id_autogen1_search'
    },
    expertiseKeywordUnivClick: {
      selector:'#select2-result-label-2'
    },
    expertiseClinical: {
      selector: '#updatedClassifications1'
    }
   


  }
};
