const pageCommands = {
  clickProffesionalExperience(){
    return this
      .waitForElementVisible('@proffesionalExperienceFirstLabel')
      .click('@proffesionalExperienceFirstLabel')
  },
  plusProffesionalExperience() {
    return this
    .waitForElementVisible('@proffesionalExperienceEditButton')
    .click('@proffesionalExperienceEditButton')
  },
  fillProffesionalExperience() {
    return this
    .waitForElementVisible('@startDateProffesional')
    .clearValue('@startDateProffesional')
    .setValue('@startDateProffesional',"11/01/2010")
    .clearValue('@endDateProffesional')
    .setValue('@endDateProffesional',"11/01/2019")
    .clearValue('@organizationProffesional')
    .setValue('@organizationProffesional',"My organization")
    .clearValue('@titleProffesional')
    .setValue('@titleProffesional',"My proffesional title")
    .clearValue('@departmentProffesional')
    .setValue('@departmentProffesional',"My department")
    .clearValue('@emailProffesional')
    .setValue('@emailProffesional',"My@email.com")
    .clearValue('@supervisorProffesional')
    .setValue('@supervisorProffesional',"My supervisor")
    .clearValue('@supervisorTitleProffesional')
    .setValue('@supervisorTitleProffesional',"My supervisor title")
    .clearValue('@supervisorPhoneProffesional')
    .setValue('@supervisorPhoneProffesional',"062-062-0622")
    .clearValue('@addressProffesional')
    .setValue('@addressProffesional',"My address")
    .clearValue('@cityProffesional')
    .setValue('@cityProffesional',"My city")
    .clearValue('@stateProffesional')
    .setValue('@stateProffesional',"BH")
    .clearValue('@zipProffesional')
    .setValue('@zipProffesional',"71000")
    .waitForElementVisible('@countryProffesional')
    .click('@countryProffesional')
    .waitForElementVisible('@countryProffesionalUS')
    .click('@countryProffesionalUS')
    .waitForElementVisible('@typeProffesional')
    .click('@typeProffesional')
    .waitForElementVisible('@typeProffesionalFull')
    .click('@typeProffesionalFull')
    .waitForElementVisible('@classificationProffesional')
    .click('@classificationProffesional')
    .waitForElementVisible('@classificationProffesionalClinical')
    .click('@classificationProffesionalClinical')
    .clearValue('@responsibilitiesProffesional')
    .setValue('@responsibilitiesProffesional',`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam ac sem id dapibus. Donec ut vehicula leo. Nulla auctor mi velit, non suscipit justo semper at. Nunc dolor tellus, varius at urna nec, gravida luctus felis. Donec neque ex, gravida eu lacus in, eleifend varius purus. Aliquam consequat, quam eu pharetra fringilla, justo nisl lacinia risus, ac lobortis purus tortor ac enim. Mauris sit amet laoreet arcu. Mauris venenatis egestas imperdiet. In semper pellentesque elit et scelerisque. Nulla euismod sapien fermentum odio lacinia interdum. Morbi rhoncus iaculis consequat. Fusce et fermentum augue, sit amet aliquam dui. Integer placerat sapien in felis convallis, a feugiat dui maximus. Maecenas a condimentum dui, eget fermentum velit. Fusce ante metus, varius ac justo id, aliquam mattis ex. Cras volutpat velit diam.`)

  },
  cancelProffesionalExperience() {
    return this
      .waitForElementVisible('@cancelProffesionalExperienceButton')
      .click('@cancelProffesionalExperienceButton')
  },
   removeOneProffesionalExperience() {
    return this
      .waitForElementVisible('@removeOneProffesionalExperienceButton')
      .click('@removeOneProffesionalExperienceButton')
  },
  saveProffesionalExperience() {
    return this
      .waitForElementVisible('@saveProffesionalExperienceButton')
      .click('@saveProffesionalExperienceButton')
  },
  saveAndAddProffesionalExperience() {
    return this
      .waitForElementVisible('@saveAndAddProffesionalExperienceButton')
      .click('@saveAndAddProffesionalExperienceButton')
  },

  checkIsProffesionalExperienceOpen() {
    return this
      .waitForElementVisible('@appProffesionalExperienceLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    proffesionalExperienceFirstLabel: {
      selector: 'a[href="professional_experience.html"]'
    },
    appProffesionalExperienceLabel: {
      selector: "//h2[text()='Professional Experience']",
      locateStrategy: 'xpath'
    },
    proffesionalExperienceEditButton: {
      selector: 'a[href="professional_experience_edit.html"]'
    },
    startDateProffesional: {
      selector: '#startDate'
    },
    endDateProffesional: {
      selector: '#endDate'
    },
    organizationProffesional: {
      selector: '#organizationName'
    },
    titleProffesional: {
      selector: '#title'
    },
    departmentProffesional: {
      selector: '#department'
    },
    emailProffesional: {
      selector: '#email'
    },
    supervisorProffesional: {
      selector: '#supervisorName'
    },
    supervisorTitleProffesional: {
      selector: '#supervisorTitle'
    },
    supervisorPhoneProffesional: {
      selector: '#supervisorPhone'
    },
    addressProffesional: {
      selector: '#address'
    },
     cityProffesional: {
      selector: '#city'
    },
    stateProffesional: {
      selector: '#state'
    },
     zipProffesional: {
      selector: '#zip'
    },
    countryProffesional: {
      selector: '#country_id_chosen'
    },
    countryProffesionalUS: {
      selector: '//li[text()="United States"]',
      locateStrategy: 'xpath'
    },
    typeProffesional: {
      selector: '#type_id_chosen'
    },
    typeProffesionalFull: {
      selector: '//li[text()="Full-time"]',
      locateStrategy: 'xpath'
    },
    classificationProffesional : {
      selector: '#classification_id_chosen'
    },
    classificationProffesionalClinical : {
      selector: "//li[text()='Clinical']",
      locateStrategy: 'xpath'
    },
     responsibilitiesProffesional : {
      selector: '#text'
    },
     removeOneProffesionalExperienceButton: {
       selector: 'img[src="../images/icons/delete.png"]'
     },
    cancelProffesionalExperienceButton: {
      selector: 'input[value="Cancel"]'
    },
    saveProffesionalExperienceButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddProffesionalExperienceButton: {
      selector: 'input[value="Save & Add Another"]'
    },


  }
};
