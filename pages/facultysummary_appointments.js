const pageCommands = {
  facultyAppointments() {
    return this
      .waitForElementVisible('@appointmentsLabel')
      .click('@appointmentsLabel')
  },
  facultyAppointmentsAdd() {
    return this
    .waitForElementVisible('@appointmentsAddButton')
    .click('@appointmentsAddButton')
  },
  facultyAppointmentsAddContent() {
     this
      .waitForElementVisible('@appointmentsTitlesDepartment')
      .click( '@appointmentsTitlesDepartment')
      .waitForElementVisible( '@appointmentsTitlesDepartmentDoA')
      .click( '@appointmentsTitlesDepartmentDoA')
      this.api.pause(2000);
      this.waitForElementVisible( '@appointmentsType')
      .click( '@appointmentsType')
      .waitForElementVisible( '@appointmentsTypeEmeritus')
      .click( '@appointmentsTypeEmeritus')
      this.api.pause(2000);
      this.waitForElementVisible( '@appointmentsTrack')
      .click( '@appointmentsTrack')
      .waitForElementVisible( '@appointmentsTrackClinical')
      .click( '@appointmentsTrackClinical')
      .click( '@appointmentsTrackClinical')
      .click( '@appointmentsTrackClinical')
      this.api.pause(2000);
      this.waitForElementVisible( '@appointmentsRank')
      .click( '@appointmentsRank')
      .waitForElementVisible( '@appointmentsRankProffesor')
      .click( '@appointmentsRankProffesor')
      .click( '@appointmentsRankProffesor')
      .clearValue( '@appointmentsEffective')
      .setValue( '@appointmentsEffective', "01/01/2000")
      .clearValue( '@appointmentsHire')
      .setValue('@appointmentsHire',"01/01/2010")
      .click('@appointmentsPrimary')
      .click('@appointmentsPrimary')
      .click('@appointmentsNewHire')
      .click('@appointmentsNewHire')
      return this;
  },
  removeOneAppointments() {
    return this
      .waitForElementVisible('@removeOneAppointmentsButton')
      .click('@removeOneAppointmentsButton')
  },
 facultyAppointmentsExpand() {
    return this
      .waitForElementVisible('@appointmentsExpandAll')
      .click('@appointmentsExpandAll')
  },
  facultyAppointmentsColapse() {
    return this
      .waitForElementVisible('@appointmentsColapseAll')
      .click('@appointmentsColapseAll')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    appointmentsLabel: {
      selector: 'a[href="appointments_rpt.html?emplid=RA102153"]'
    },
    appointmentsAddButton: {
      selector: 'a[href="appointment_edit.html?emplid=RA102153"]'
    },
    appointmentsTitlesDepartment: {
      selector: '#department_id_chosen'
    },
    appointmentsTitlesDepartmentDoA: {
      selector: '//li[text()="Department of Anesthesiology"]',
      locateStrategy: 'xpath'
    },
    appointmentsType: {
      selector: '#type_id_chosen'
    },
    appointmentsTypeEmeritus: {
      selector: '//li[text()="Core"]',
      locateStrategy: 'xpath'
    },
    appointmentsTrack: {
      selector: '#track_id_chosen'
    },
    appointmentsTrackClinical: {
      selector: '//li[text()="Community"]',
      locateStrategy: 'xpath'
    },
    appointmentsRank: {
      selector: '#rank_id_chosen'
    },
    appointmentsRankProffesor: {
      selector: '//div[@id="rank_id_chosen"]/div/ul/li[1]',
      locateStrategy: 'xpath'
    },
    appointmentsEffective: {
      selector: '#effective'
    },
    appointmentsHire: {
      selector: '#hire'
    },
    appointmentsPrimary: {
      selector: '#primary1'
    },
    appointmentsNewHire: {
      selector: '#newHire1'
    },
    removeOneAppointmentsButton: {
      selector: 'img[src="images/icons/delete.png"]'
    },
    appointmentsExpandAll: {
      selector: '#content_container > div > div:nth-child(2) > div > div.content-padder > div > div.image-link.buffer-bottom > a:nth-child(4)'
    },
    appointmentsColapseAll: {
      selector: '#content_container > div > div:nth-child(2) > div > div.content-padder > div > div.image-link.buffer-bottom > a:nth-child(6)'
    }

  }
};
