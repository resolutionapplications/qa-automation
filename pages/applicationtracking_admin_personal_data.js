const pageCommands = {
  clickPersonalData(){
    return this
      .waitForElementVisible('@personalDataFirstLabel')
      .click('@personalDataFirstLabel')
  },
  fillPersonalData() {
    return this
      .waitForElementVisible('@nameFirst')
      .clearValue('@nameFirst')
      .setValue('@nameFirst',"Namen")
      .clearValue('@nameMiddle')
      .setValue('@nameMiddle',"Middlem")
      .clearValue('@nameLast')
      .setValue('@nameLast',"Surnames")
      .clearValue('@dateOfBirth')
      .setValue('@dateOfBirth',"11/06/1991")
      .click('@gender')
      .waitForElementVisible('@genderMale')
      .click('@genderMale')
      .click('@ethnicity')
      .waitForElementVisible('@ethnicityAlaska')
      .click('@ethnicityAlaska')
      .click('@highestDegree')
      .waitForElementVisible('@highestDegreeDrAcademic')
      .click('@highestDegreeDrAcademic')
      .clearValue('@addressLineOne')
      .setValue('@addressLineOne',"Street name")
      .clearValue('@addressLineTwo')
      .setValue('@addressLineTwo',"Street name second")
      .clearValue('@addressLineThree')
      .setValue('@addressLineThree',"Street name third")
      .clearValue('@city')
      .setValue('@city',"Citycity")
      .clearValue('@state')
      .setValue('@state',"BH")
      .clearValue('@zip')
      .setValue('@zip',"71000")
      .clearValue('@emailEmail')
      .setValue('@emailEmail',"aaa@gmail.com")
      .clearValue('@officePhone')
      .setValue('@officePhone',"777-888-9999")
      .clearValue('@mobilePhone')
      .setValue('@mobilePhone',"061-061-0611")
      .waitForElementVisible('@citizenship')
      .click('@citizenshipNo')
      .waitForElementVisible('#country_select_chosen')
      .click('#country_select_chosen')
      .waitForElementVisible('@citizenPassportBH')
      .click('@citizenPassportBH')
      .click('@citizenshipPerformingServices')
      .click('@visaType')
      .waitForElementVisible('@visaTypeJ1')
      .click('@visaTypeJ1')
      .clearValue('@visaStatusDate')
      .setValue('@visaStatusDate',"24/10/2016")
      .clearValue('@visaExpirationDate')
      .setValue('@visaExpirationDate',"24/04/2017")
      .waitForElementVisible('@saveContinuePersonalDataButton')
      .click('@saveContinuePersonalDataButton')

  },
  checkIsAppOpen() {
    return this
      .waitForElementVisible('@appIDLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    personalDataFirstLabel: {
      selector: "a[href='personal_data.html']"
    },
    appIDLabel: {
      selector: "//span[text()='102153']",
      locateStrategy: 'xpath'
    },
    nameFirst: {
      selector: 'input[name="name.first"]'
    },
    nameMiddle: {
      selector: 'input[name="name.middle"]'
    },
    nameLast: {
      selector: 'input[name="name.last"]'
    },
    dateOfBirth: {
      selector: 'input[name="personal.dateOfBirth"]'
    },
    gender:{
      selector: '#personal_gender_id_chosen'
    },
    genderMale: {
      selector: "//li[text()='Male']",
      locateStrategy: 'xpath'
    },
    ethnicity:{
      selector: '#personal_ethnicity_id_chosen'
    },
    ethnicityAlaska: {
      selector: "//li[text()='American Indian or Alaska Native (not Hispanic or Latino)']",
      locateStrategy: 'xpath'
    },
    highestDegree:{
      selector: '#personal_highestDegree_id_chosen'
    },
    highestDegreeDrAcademic: {
      selector: "//li[text()='Doctorate (Academic)']",
      locateStrategy: 'xpath'
    },
    addressLineOne: {
      selector: 'input[name="location.line1"]'
    },
    addressLineTwo: {
      selector: 'input[name="location.line2"]'
    },
    addressLineThree: {
      selector: 'input[name="location.line3"]'
    },
    city: {
      selector: 'input[name="location.city"]'
    },
    state: {
      selector: 'input[name="location.state"]'
    },
    zip: {
      selector: 'input[name="location.zip"]'
    },
    emailEmail: {
      selector: 'input[name="email.email"]'
    },
    officePhone: {
      selector: 'input[name="office.phone"]'
    },
    mobilePhone: {
      selector: 'input[name="mobile.phone"]'
    },
    citizenship: {
      selector: 'input[type=radio]'
    },
    citizenshipNo: {
      selector: 'input[id="citizenship.permanentResident2"]'
    },
    citizenshipPerformingServices: {
      selector: 'input[id="citizenship.performingServices1"]'
    },
    visaStatusDate: {
      selector: 'input[name="citizenship.visaStatusDate"]'
    },
    visaExpirationDate: {
      selector: 'input[name="citizenship.visaExpiration"]'
    },
    visaType:{
      selector: 'select[name="citizenship.visaType.id"]'
    },
    visaTypeJ1: {
      selector: 'select[name="citizenship.visaType.id"] option[value="100124"]'
    },
    citizenPassport:{
      selector: '#country_select_chosen'
    },
    citizenPassportBH: {
    selector: "//li[text()='Bosnia and Herzegovina']",
    locateStrategy: 'xpath'
    },
    saveContinuePersonalDataButton: {
      selector: 'input[name="_continue"]'
    }
  }
};
