const pageCommands = {
  editTeaOther() {
    return this
      .waitForElementVisible('@teaOtherEditLabel')
      .click('@teaOtherEditLabel')
  },
  editTeaOtherAdd() {
    return this
      .waitForElementVisible('@teaOtherEditContentButton')
      .click('@teaOtherEditContentButton')
    },
  editTeaOtherAddContent() {
      return this
        .waitForElementVisible('@teaOtherTitle')
        .clearValue('@teaOtherTitle')
        .setValue('@teaOtherTitle',"Other (Teaching)")
  },
  editTeaOtherDelete() {
    return this
      .waitForElementVisible('@otherDeleteButton')
      .click('@otherDeleteButton')
  },
  editTeaOtherConfirmDelete() {
    return this
      .waitForElementVisible('@otherConfirmDeleteButton')
      .click('@otherConfirmDeleteButton')
  },
  editTeaOtherCancel() {
    return this
      .waitForElementVisible('@otherCancelButton')
      .click('@otherCancelButton')
  },

};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
   teaOtherEditLabel: {
      selector: 'a[href="index.html#profileotherteaching"]'
    },
    teaOtherEditContentButton: {
      selector: 'a[href="/profileotherteaching/edit.html"]'
    },
    otherDeleteButton: {
      selector: '#maincol > div > h3:nth-child(34) > a:nth-child(3) > img'
    },
    teaOtherTitle: {
      selector: '#title'
    },
    teaOtherContent: {
      selector: '#cke_paragraph'
    },
     otherCancelButton: {
      selector: 'input[name="_cancel"]'
    },
    otherConfirmDeleteButton: {
      selector:'//*[@id="obliviate"]/input[2]',
      locateStrategy: 'xpath'
    }
  }
};
