const pageCommands = {
  editFacultyProfile() {
    return this
      .waitForElementVisible('@editYourProfileButton')
      .click('@editYourProfileButton')

  }
};
module.exports = {
  url: 'http://dev.resoapps.com',
  commands: [pageCommands],
  elements: {
    editYourProfileButton: {
      selector: 'form[action="/profile/portal/portal.html"] > input'
      // selector: 'input[src=/images/landing/login_button.png]'
    },
  }
};
