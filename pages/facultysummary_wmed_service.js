const pageCommands = {
  facultyWMedServices(){
    return this
      .waitForElementVisible('@wMedServicesLabel')
      .click('@wMedServicesLabel')
  },
  facultyWMedServicesAddContent() {
    return this
    .waitForElementVisible('@chb_AdmissionsInterviewer')
    .click('@chb_AdmissionsInterviewer')
    .click('@chb_ClinicalResearch')
    .click('@chb_CurriculumDevelopment')
    .click('@chb_EducationalResearch')
    .click('@chb_HostElectiveStudents')
    .click('@chb_LaboratoryResearch')
    .click('@chb_Mentoring')
    .click('@chb_Precepting')
    .click('@chb_TeachingResidents')
    .click('@chb_TeachingStudents')
    .click('@chb_Toxicology')
    .click('@chb_WMedCommittees')    
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    wMedServicesLabel: {
      selector: "a[href='self_disclosure.html?id=9001&emplid=RA102153']"
    },
    chb_AdmissionsInterviewer: {
      selector: 'input[name="questions[1].value"]'
    },
    chb_ClinicalResearch: {
      selector: 'input[name="questions[2].value"]'
    },
    chb_CurriculumDevelopment: {
      selector: 'input[name="questions[3].value"]'
    },
    chb_EducationalResearch: {
      selector: 'input[name="questions[4].value"]'
    },
    chb_HostElectiveStudents: {
      selector: 'input[name="questions[5].value"]'
    },
    chb_LaboratoryResearch: {
      selector: 'input[name="questions[6].value"]'
    },
    chb_Mentoring: {
      selector: 'input[name="questions[7].value"]'
    },
    chb_Precepting: {
      selector: 'input[name="questions[8].value"]'
    },
    chb_TeachingResidents: {
      selector: 'input[name="questions[9].value"]'
    },
    chb_TeachingStudents: {
      selector: 'input[name="questions[10].value"]'
    },
    chb_Toxicology: {
      selector: 'input[name="questions[11].value"]'
    },
    chb_WMedCommittees: {
      selector: 'input[name="questions[12].value"]'
    },
  }
};
