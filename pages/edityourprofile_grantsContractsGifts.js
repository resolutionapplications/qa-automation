const pageCommands = {
  editResGrants() {
    return this
      .waitForElementVisible('@grantsEditLabel')
      .click('@grantsEditLabel')
  },
  editResGrantsAdd() {
    return this
    .waitForElementVisible('@grantsAddButton')
    .click('@grantsAddButton')
  },
  editResGrantsAddContent() {
      return this
      .waitForElementVisible('@grantsProjectTitle')
      .clearValue( '@grantsProjectTitle')
      .setValue( '@grantsProjectTitle', "Title of the project")
      .clearValue( '@grantsProjectNumber')
      .setValue( '@grantsProjectNumber', "13")
      .clearValue( '@grantsPI')
      .setValue( '@grantsPI', "PI Name Surname")
      .clearValue( '@grantsCoPls')
      .setValue( '@grantsCoPls', "Co PI One, Co Pi Two")
      .click('@grantsYourRoleOnProject')
      .waitForElementVisible('@grantsYourRoleOnProjectPI')
      .click('@grantsYourRoleOnProjectPI')
      .clearValue( '@grantsNumberOfRAsSupported')
      .setValue( '@grantsNumberOfRAsSupported', "5")
      .clearValue( '@grantsURL')
      .setValue( '@grantsURL', "http://www.url.com")
      .clearValue( '@grantsSourceOfFunds')
      .setValue( '@grantsSourceOfFunds', "My source")
      .clearValue( '@grantsFundingStartDate')
      .setValue( '@grantsFundingStartDate', "01/01/2000")
      .clearValue( '@grantsFundingEndDate')
      .setValue( '@grantsFundingEndDate', "01/01/2020")
      .clearValue( '@grantsTotalProjectBudget')
      .setValue( '@grantsTotalProjectBudget', "50000")
      .clearValue( '@grantsYourshareOfTotalProjectBudget')
      .setValue( '@grantsYourshareOfTotalProjectBudget', "10000")
      .clearValue( '@grantsTotalExpendituresOfFARPeriod')
      .setValue( '@grantsTotalExpendituresOfFARPeriod', "10000")
      .clearValue( '@grantsYourShareOfExpendituresInDollars')
      .setValue( '@grantsYourShareOfExpendituresInDollars', "300")
      .clearValue( '@grantsComments')
      .setValue( '@grantsComments', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.")
      .click('@grantsInternationalCbox')
      .waitForElementVisible('@grantsInternationalCboxCountry')
      .clearValue( '@grantsInternationalCboxCountry')
      .setValue( '@grantsInternationalCboxCountry', "Country")
      .click('@grantsInterdisciplinaryCbox')
      .click('@grantsCommunityEngagementCbox')
      .click('@grantsPublishCbox')
  },
  editResGrantsSort() {
    return this
      .waitForElementVisible('@grantsSortButton')
      .click('@grantsSortButton')
    },
  editResGrantsShowHide() {
    return this
      .waitForElementVisible('@grantsShowHideButton')
      .click('@grantsShowHideButton')
  },
  editResGrantsShowHideSelect() {
    return this
      .waitForElementVisible('@grantsShowHideBox')
      .click('@grantsShowHideBox')
    },
  removeOneGrants() {
    return this
      .waitForElementVisible('@removeOneGrantsButton')
      .click('@removeOneGrantsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    grantsEditLabel: {
      selector: 'a[href="index.html#grants"]'
    },
    grantsAddButton: {
      selector: 'a[href="/grant/edit.html"]'
    },
    grantsSortButton: {
      selector: 'a[href="/grant/sort.html"]'
    },
    grantsShowHideButton: {
      selector: 'a[href="/grant/visible.html"]'
    },
    removeOneGrantsButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    grantsProjectTitle: {
      selector: '#projectTitle'
    },
    grantsProjectNumber: {
      selector: '#projectNumber'
    },
    grantsPI: {
      selector: '#principalInvestigator'
    },
    grantsCoPls: {
      selector: '#coPrincipalInvestigators'
    },
    grantsYourRoleOnProject: {
      selector: '#projectRole'
    },
    grantsYourRoleOnProjectPI: {
      selector: "//option[text()='PI']",
      locateStrategy: 'xpath'
    },
    grantsNumberOfRAsSupported: {
      selector: '#numberSupported'
    },
    grantsURL: {
      selector: '#url'
    },
    grantsSourceOfFunds: {
      selector: '#sourceOfFunds'
    },
    grantsFundingStartDate: {
      selector: '#fundingPeriodStart'
    },
    grantsFundingEndDate: {
      selector: '#fundingPeriodEnd'
    },
    grantsTotalProjectBudget: {
      selector: '#totalFunding'
    },
    grantsYourshareOfTotalProjectBudget: {
      selector: '#totalShareOfFunding'
    },
    grantsTotalExpendituresOfFARPeriod: {
      selector: '#totalExpenditureForPeriod'
    },
    grantsYourShareOfExpendituresInDollars: {
      selector: '#totalShareOfExpenditures'
    },
    grantsComments: {
      selector: '#comments'
    },
    //
    grantsInternationalCbox: {
      selector: '#checked_0'
    },
    grantsInternationalCboxCountry: {
      selector: '#value_0'
    },
    grantsInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    grantsCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    grantsPublishCbox: {
      selector: '#displayed1'
    },
    grantsShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    }


  }
};
