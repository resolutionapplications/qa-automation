const pageCommands = {
 facultyDemographicInformation(){
    return this
      .waitForElementVisible('@demographicInformationLabel')
      .click('@demographicInformationLabel')
  },
  facultyDemographicInformationAddContent() {
     this
    .waitForElementVisible('@demographicInformationQuestion1')
    .click('@demographicInformationQuestion1')
    .waitForElementVisible('@demographicInformationQuestion1Yes')
    .click('@demographicInformationQuestion1Yes')
    .click('@demographicInformationQuestion2')
    .waitForElementVisible('@demographicInformationQuestion2No')
    .click('@demographicInformationQuestion2No')
    .click('@demographicInformationQuestion3')
    .waitForElementVisible('@demographicInformationQuestion3No')
    .click('@demographicInformationQuestion3No')
    .click('@demographicInformationQuestion4')
    .waitForElementVisible('@demographicInformationQuestion4No')
    .click('@demographicInformationQuestion4No')
    this.api.pause(2000);
    this.click('@demographicInformationQuestion5')
    .click('@demographicInformationQuestion5')
    .waitForElementVisible('@demographicInformationQuestion5No')
    .click('@demographicInformationQuestion5No')
    return this;
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    demographicInformationLabel: {
      selector: "a[href='self_disclosure.html?id=9002&emplid=RA102153']"
    },
    demographicInformationQuestion1: {
      selector: '#questions0_xref_chosen'
    },
    demographicInformationQuestion1Yes: {
      selector: "//li[text()='Yes']",
      locateStrategy: 'xpath'
    },
    demographicInformationQuestion2: {
      selector: '#questions1_xref_chosen'
    },
    demographicInformationQuestion2No: {
      selector: "//*[@id='questions1_xref_chosen']/div/ul/li[2]",
      locateStrategy: 'xpath'
    },
    demographicInformationQuestion3: {
      selector: '#questions2_xref_chosen'
    },
    demographicInformationQuestion3No: {
      selector: "//*[@id='questions2_xref_chosen']/div/ul/li[2]",
      locateStrategy: 'xpath'
    },
    demographicInformationQuestion4: {
      selector: '#questions3_xref_chosen'
    },
    demographicInformationQuestion4No: {
      selector: "//*[@id='questions3_xref_chosen']/div/ul/li[2]",
      locateStrategy: 'xpath'
    },
    demographicInformationQuestion5: {
      selector: '#questions4_xref_chosen'
    },
    demographicInformationQuestion5No: {
      selector: "//*[@id='questions4_xref_chosen']/div/ul/li[2]",
      locateStrategy: 'xpath'
    }

  }
};
