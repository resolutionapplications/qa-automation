const pageCommands = {
 facultyMedicalLicenses(){
    return this
      .waitForElementVisible('@medicalLicensesLabel')
      .click('@medicalLicensesLabel')
  },
  facultyMedicalLicensesAdd() {
    return this
    .waitForElementVisible('@medicalLicensesAddButton')
    .click('@medicalLicensesAddButton')
  },
  facultyMedicalLicensesAddContent() {
    return this
    .waitForElementVisible('@typeMedical')
    .click('@typeMedical')
    .waitForElementVisible('@typeMedicalLicence')
    .click('@typeMedicalLicence')
    .clearValue('@titleMedical')
    .setValue('@titleMedical',"My medical title")
    .clearValue('@issuerMedical')
    .setValue('@issuerMedical',"My issuer")
    .clearValue('@stateMedical')
    .setValue('@stateMedical',"My state")
    .click('@countryMedical')
    .waitForElementVisible('@countryMedicalUS')
    .click('@countryMedicalUS')
    .clearValue('@licenceNumberMedical')
    .setValue('@licenceNumberMedical',"4564846")
    .clearValue('@dateIssuedMedical')
    .setValue('@dateIssuedMedical',"11/01/2010")
    .clearValue('@expirationDateMedical')
    .setValue('@expirationDateMedical',"11/01/2019")
    .click('@licenseStatusMedical')
    .waitForElementVisible('@licenseStatusMedicalCurrent')
    .click('@licenseStatusMedicalCurrent')
    .click('@fieldMedical')
    .waitForElementVisible('@fieldMedicalAcu')
    .click('@fieldMedicalAcu')
  },
  removeOneMedicalLicenses() {
    return this
      .waitForElementVisible('@removeOneMedicalLicensesButton')
      .click('@removeOneMedicalLicensesButton')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    medicalLicensesLabel: {
      selector: "a[href='medical_licenses.html?emplid=RA102153']"
    },
    medicalLicensesAddButton: {
      selector: 'a[href="medical_license_edit.html?emplid=RA102153"]'
    },
    typeMedical: {
      selector: '#type_id_chosen'
    },
    typeMedicalLicence: {
      selector: "//li[text()='Consulting Matrix National Certificate']",
      locateStrategy: 'xpath'
    },
    titleMedical: {
      selector: '#title'
    },
    issuerMedical: {
      selector: '#issuer'
    },
    stateMedical: {
      selector: '#state'
    },
    countryMedical: {
      selector: '#country_id_chosen'
    },
    countryMedicalUS: {
      selector: "//li[text()='United States']",
      locateStrategy: 'xpath'
    },
    licenceNumberMedical: {
      selector: '#number'
    },
    dateIssuedMedical: {
      selector: '#issued'
    },
    expirationDateMedical: {
      selector: '#expired'
    },
    licenseStatusMedical: {
      selector: '#status_id_chosen'
    },
    licenseStatusMedicalCurrent: {
      selector: "//li[text()='Current']",
      locateStrategy: 'xpath'
    },
    fieldMedical: {
      selector: '#field_id_chosen'
    },
    fieldMedicalAcu: {
      selector: "//li[text()='Acupuncturist']",
      locateStrategy: 'xpath'
    },
    removeOneMedicalLicensesButton: {
      selector: 'img[src="images/icons/delete.png"]'
    }


  }
};
