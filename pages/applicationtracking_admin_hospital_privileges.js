const pageCommands = {
  clickHospitalPrivileges(){
    return this
      .waitForElementVisible('@hospitalPrivilegesFirstLabel')
      .click('@hospitalPrivilegesFirstLabel')
  },
  plusHospitalPrivileges() {
    return this
    .waitForElementVisible('@hospitalPrivilegesEditButton')
    .click('@hospitalPrivilegesEditButton')
  },
  fillHospitalPrivileges() {
    return this
    .waitForElementVisible('@hospital')
    .click('@hospital')
    .waitForElementVisible('@hospitalBorgess')
    .click('@hospitalBorgess')
    .clearValue('@cityHospital')
    .setValue('@cityHospital',"Hospitalcity")
    .clearValue('@stateHospital')
    .setValue('@stateHospital',"My")
  },
  cancelHospitalPrivileges() {
    return this
      .waitForElementVisible('@cancelHospitalPrivilegesButton')
      .click('@cancelHospitalPrivilegesButton')
  },
  saveHospitalPrivileges() {
    return this
      .waitForElementVisible('@saveHospitalPrivilegesButton')
      .click('@saveHospitalPrivilegesButton')
  },
  saveAndAddHospitalPrivileges() {
    return this
      .waitForElementVisible('@saveAndAddHospitalPrivilegesButton')
      .click('@saveAndAddHospitalPrivilegesButton')
  },
  removeOneHospitalPrivileges() {
    return this
      .waitForElementVisible('@removeOneHospitalPrivilegesButton')
      .click('@removeOneHospitalPrivilegesButton')
  },

  checkIsHospitalPrivilegesOpen() {
    return this
      .waitForElementVisible('@appHospitalPrivilegesLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    hospitalPrivilegesFirstLabel: {
      selector: "a[href='hospital_privileges.html']"
    },
    appHospitalPrivilegesLabel: {
      selector: "//h2[text()='Hospital Privileges']",
      locateStrategy: 'xpath'
    },
    hospitalPrivilegesEditButton: {
      selector: 'a[href="hospital_privilege_edit.html"]'
    },
    hospital: {
      selector: '#hospital_type_chosen'
    },
    hospitalBorgess: {
      selector: "//li[text()='Borgess']",
      locateStrategy: 'xpath'
    },
    cityHospital: {
      selector: '#city'
    },
    stateHospital: {
      selector: '#state'
    },
    removeOneHospitalPrivilegesButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
    cancelHospitalPrivilegesButton: {
      selector: 'input[value="Cancel"]'
    },
    saveHospitalPrivilegesButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddHospitalPrivilegesButton: {
      selector: 'input[value="Save & Add Another"]'
    },


  }
};
