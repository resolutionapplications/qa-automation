const pageCommands = {
  editGradCurrentStudents() {
    return this
      .waitForElementVisible('@currentStudentsEditLabel')
      .click('@currentStudentsEditLabel')
  },
  editGradCurrentStudentsAdd() {
    return this
    .waitForElementVisible('@currentStudentsAddButton')
    .click('@currentStudentsAddButton')
  },
  editGradCurrentStudentsAddContent() {
      return this
      .waitForElementVisible('@currentStudentsStudentName')
      .clearValue( '@currentStudentsStudentName')
      .setValue( '@currentStudentsStudentName', "Name Surname")
      .clearValue( '@currentStudentsURL')
      .setValue( '@currentStudentsURL', "http://www.url.com")
      .click('@currentStudentsCommiteeRole')
      .click('@currentStudentsProjectType')
      .click('@currentStudentsStatus')
      .click('@currentStudentsGraduationSemester')
      .clearValue( '@currentStudentsExpectedGraduation')
      .setValue( '@currentStudentsExpectedGraduation', "2020")
      .click('@currentStudentsExpectedDegree')
      .waitForElementVisible('@currentStudentsExpectedDegreeAuD')
      .click('@currentStudentsExpectedDegreeAuD')
      .click('@currentStudentsTypeOfSupportRA')
      .click('@currentStudentsTypeOfSupportOther')
      .clearValue( '@currentStudentsSupportSource')
      .setValue( '@currentStudentsSupportSource', "My student support source")
      .clearValue( '@currentStudentsCoAdvisor')
      .setValue( '@currentStudentsCoAdvisor', "My Co-Advisor")
      .click('@currentStudentsNonUStudent1')
      .waitForElementVisible('@currentStudentsNonUStudentInstitution')
      .clearValue( '@currentStudentsNonUStudentInstitution')
      .setValue( '@currentStudentsNonUStudentInstitution', "My institution")
      .clearValue( '@currentStudentssComments')
      .setValue( '@currentStudentssComments', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.")
      .click('@currentStudentsInternationalCbox')
      .waitForElementVisible('@currentStudentsInternationalCboxCountry')
      .clearValue( '@currentStudentsInternationalCboxCountry')
      .setValue( '@currentStudentsInternationalCboxCountry', "Country")
      .click('@currentStudentsInterdisciplinaryCbox')
      .click('@currentStudentsCommunityEngagementCbox')
      .click('@currentStudentsPublishCbox')
  },
  editGradCurrentStudentsSort() {
    return this
      .waitForElementVisible('@currentStudentsSortButton')
      .click('@currentStudentsSortButton')
    },
  editGradCurrentStudentsShowHide() {
    return this
      .waitForElementVisible('@currentStudentsShowHideButton')
      .click('@currentStudentsShowHideButton')
  },
  editGradCurrentStudentsShowHideSelect() {
    return this
      .waitForElementVisible('@currentStudentsShowHideBox')
      .click('@currentStudentsShowHideBox')
    },
  removeOneCurrentStudents() {
    return this
      .waitForElementVisible('@removeOneCurrentStudentsButton')
      .click('@removeOneCurrentStudentsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    currentStudentsEditLabel: {
      selector: 'a[href="index.html#currentstudents"]'
    },
    currentStudentsAddButton: {
      selector: 'a[href="/currentstudents/edit.html"]'
    },
    currentStudentsSortButton: {
      selector: 'a[href="/currentstudents/sort.html"]'
    },
    currentStudentsShowHideButton: {
      selector: 'a[href="/currentstudents/visible.html"]'
    },
    removeOneCurrentStudentsButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    currentStudentsStudentName: {
      selector: '#name'
    },
    currentStudentsURL: {
      selector: '#url'
    },
    currentStudentsCommiteeRole: {
      selector: '#committeeRole2'
    },
    currentStudentsProjectType: {
      selector: '#projectType2'
    },
    currentStudentsStatus: {
      selector: '#status2'
    },
    currentStudentsGraduationSemester: {
      selector: "#graduationSemester3"
    },
    currentStudentsExpectedGraduation: {
      selector: '#expectedGraduation'
    },
    currentStudentsExpectedDegree: {
      selector: '#degree'
    },
    currentStudentsExpectedDegreeAuD: {
      selector: "//option[text()='Doctor of Audiology (Au.D.)']",
      locateStrategy: 'xpath'
    },
    currentStudentsTypeOfSupportRA: {
      selector: 'input[name="supportTypesList[0].selected"]'
    },
    currentStudentsTypeOfSupportOther: {
      selector: 'input[name="supportTypesList[3].selected"]'
    },
    currentStudentsSupportSource: {
      selector: '#supportSource'
    },
    currentStudentsCoAdvisor: {
      selector: '#coAdvisor'
    },
    currentStudentsNonUStudent1: {
      selector: '#nonUStudent1'
    },
    currentStudentsNonUStudentInstitution: {
      selector: '#nonUStudentInstitution'
    },
    currentStudentssComments: {
      selector: '#comment'
    },
    //
    currentStudentsInternationalCbox: {
      selector: '#checked_0'
    },
    currentStudentsInternationalCboxCountry: {
      selector: '#value_0'
    },
    currentStudentsInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    currentStudentsCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    currentStudentsPublishCbox: {
      selector: '#displayable1'
    },
    currentStudentsShowHideBox: {
      selector: 'input[name="list0.displayable1"]'
    }


  }
};
