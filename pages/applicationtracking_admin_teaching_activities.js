const pageCommands = {
  clickTeaching(){
    return this
      .waitForElementVisible('@teachingFirstLabel')
      .click('@teachingFirstLabel')
  },
  plusTeaching() {
    return this
    .waitForElementVisible('@teachingEditButton')
    .click('@teachingEditButton')
  },
  fillWithCancelTeaching() {
    return this
      .waitForElementVisible('@typeTeaching')
      .click('@typeTeaching')
      .waitForElementVisible('@typeTeachingBanner')
      .click('@typeTeachingBanner')
      .click('@cancelTeachingButton')
  },
  fillWithNextCancelTeaching() {
    return this
    .waitForElementVisible('@typeTeaching')
    .click('@typeTeaching')
    .waitForElementVisible('@typeTeachingBanner')
    .click('@typeTeachingBanner')
    .click('@nextTeachingButton')
    .waitForElementVisible('@roleTeaching')
    .click('@roleTeaching')
    .waitForElementVisible('@roleTeachingPreceptor')
    .click('@roleTeachingPreceptor')
    .clearValue('@startTeaching')
    .setValue('@startTeaching',"11/01/2000")
    .clearValue('@endTeaching')
    .setValue('@endTeaching',"11/08/2016")
    .click('@cancelTeachingButton')
  },
  fillWithNextSaveTeaching() {
    return this
    .waitForElementVisible('@typeTeaching')
    .click('@typeTeaching')
    .waitForElementVisible('@typeTeachingBanner')
    .click('@typeTeachingBanner')
    .click('@nextTeachingButton')
    .waitForElementVisible('@roleTeaching')
    .click('@roleTeaching')
    .waitForElementVisible('@roleTeachingPreceptor')
    .click('@roleTeachingPreceptor')
    .clearValue('@startTeaching')
    .setValue('@startTeaching',"11/01/2000")
    .clearValue('@endTeaching')
    .setValue('@endTeaching',"11/08/2016")
    .click('@saveTeachingButton')
  },
  removeOneTeaching() {
    return this
    .waitForElementVisible('@removeOneTeachingButton')
    .click('@removeOneTeachingButton')
  },
  checkIsTeachingOpen() {
    return this
      .waitForElementVisible('@appTeachingLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    teachingFirstLabel: {
      selector: "a[href='teaching_activities.html']"
    },
    appTeachingLabel: {
      selector: "//h2[text()='Teaching Activities']",
      locateStrategy: 'xpath'
    },
    teachingEditButton: {
      selector: 'a[href="teaching_activities_type.html"]'
    },
    typeTeaching: {
      selector: '#type_chosen'
      // selector: '#content_container > div > div:nth-child(2) > div > div > div > form > fieldset > table > tbody > tr > td > div > a'
    },
    typeTeachingBanner: {
      selector: "//li[text()='Banner-UMC Phoenix Physician']",
      locateStrategy: 'xpath'
    },
    typeTeachingBlock: {
      selector: "//li[text()='Block']",
      locateStrategy: 'xpath'
    },
    cancelTeachingButton: {
      selector: 'input[value="Cancel"]'
    },
    nextTeachingButton: {
      selector: 'input[value="Next"]'
    },
    roleTeaching: {
        selector: '#role_id_chosen'
    },
    roleTeachingPreceptor: {
      selector: "//li[text()='Preceptor']",
      locateStrategy: 'xpath'
    },
    startTeaching: {
      selector:  'input[name="start"]'
        // selector: '#start'
    },
    endTeaching: {
      selector: 'input[name="end"]'
        // selector: '#end'
    },
    saveTeachingButton: {
      selector: 'input[value="Save"]'
    },
    removeOneTeachingButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
  }
};
