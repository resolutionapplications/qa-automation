const pageCommands = {
  facultyBoards() {
    return this
      .waitForElementVisible('@boardsLabel')
      .click('@boardsLabel')
  },
 facultyBoardsAdd() {
    return this
      .waitForElementVisible('@boardsAddButton')
      .click('@boardsAddButton')
    },
  facultyBoardsAddContent() {
       this
      .waitForElementVisible('@boardsBoard')
      .click('@boardsBoard')
      .waitForElementVisible('@boardsBoardVascular')
      .click('@boardsBoardVascular')
      this.api.pause(2000);
      this.waitForElementVisible('@boardsSpecialty')
      .click('@boardsSpecialty')
      .waitForElementVisible('@boardsSpecialtyEndovascular')
      .click('@boardsSpecialtyEndovascular')
      .click('@boardsCertifiedCbox')
      .clearValue('@boardsInitialCertificationDate')
      .setValue('@boardsInitialCertificationDate',"01/08/2000")
      .clearValue('@boardsValidThroughDate')
      .setValue('@boardsValidThroughDate',"01/08/2020")
      .click('@boardsLifetimeCbox')
      return this;
      },
  removeOneBoards() {
      return this
      .waitForElementVisible('@removeOneBoardsButton')
      .click('@removeOneBoardsButton')
  },
  
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    boardsLabel: {
      selector: 'a[href="board_certifications.html?emplid=RA102153"]'
    },
    boardsAddButton: {
      selector: 'a[href="board_certifications_edit.html?emplid=RA102153"]'
    },
    boardsBoard: {
      selector: '#boards_chosen'
    },
    boardsBoardVascular: {
      selector:  "//li[text()='Amercian Board of Vascular Medicine']",
      locateStrategy: 'xpath'
    },
    boardsSpecialty: {
      selector: "#specialty_id_chosen"
    },
    boardsSpecialtyEndovascular: {
      selector: "//li[text()='Endovascular Medicine']",
      locateStrategy: 'xpath'
    },
    boardsCertifiedCbox: {
      selector: '#certified1'
    },
    boardsInitialCertificationDate: {
      selector: "#certifiedDate"
    },
    boardsValidThroughDate: {
      selector: '#end'
    },
    boardsLifetimeCbox: {
      selector: '#lifetime1'
    },
    removeOneBoardsButton: {
      selector: 'img[src="images/icons/delete.png"]'
    },

  }
};
