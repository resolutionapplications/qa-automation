const pageCommands = {
  editProfileSave() {
    return this
      .waitForElementVisible('@saveButton')
      .click('@saveButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    saveButton: {
      selector: 'input[name="done"]'
      //input.submitBtn:nth-child(9)
    },
  }
};
