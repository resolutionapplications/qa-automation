const pageCommands = {
  editPublicationsAdd() {
    return this
    .waitForElementVisible('@publicationsAddButton')
    .click('@publicationsAddButton')
  },
  editPublicationsAddContent() {
      return this
      .waitForElementVisible('@publicationsTitle')
      .clearValue( '@publicationsTitle')
      .setValue( '@publicationsTitle', "Publication title")
      .clearValue( '@publicationsPublisher')
      .setValue( '@publicationsPublisher', "Publisher")
      .click('@publicationsType')
      .waitForElementVisible('@publicationsTypeBook')
      .click('@publicationsTypeBook')
      .click('@publicationsStatus')
      .waitForElementVisible('@publicationsStatusPublished')
      .click('@publicationsStatusPublished')
      .clearValue( '@publicationsPublishedDate')
      .setValue( '@publicationsPublishedDate', "01/01/2000")
      .clearValue( '@publicationsVolume')
      .setValue( '@publicationsVolume', "Vol")
      .clearValue( '@publicationsLeadAuthor')
      .setValue( '@publicationsLeadAuthor', "Lead Name")
      .click('@publicationsMoreCoAuthorsButton')
      .clearValue( '@publicationsCoAuthor1')
      .setValue( '@publicationsCoAuthor1', "Co Author First")
      .clearValue( '@publicationsCoAuthor2')
      .setValue( '@publicationsCoAuthor2', "Co Author Second")
      .clearValue( '@publicationsPages')
      .setValue( '@publicationsPages', "500")
      .clearValue( '@publicationsAbstract')
      .setValue( '@publicationsAbstract', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.")
      .clearValue( '@publicationsISBN')
      .setValue( '@publicationsISBN', "0000-0000-0000-0000")
      .clearValue( '@publicationsURL')
      .setValue( '@publicationsURL', "http://www.url.com")
      .click('@publicationsMoreYearsCitedButton')
      .clearValue( '@publicationsYearCited1')
      .setValue( '@publicationsYearCited1', "2002")
      .clearValue( '@publicationsYearCited2')
      .setValue( '@publicationsYearCited2', "2003")
      .clearValue( '@publicationsCountCited1')
      .setValue( '@publicationsCountCited1', "8")
      .clearValue( '@publicationsCountCited2')
      .setValue( '@publicationsCountCited2', "14")
      //
      .click('@publicationsInternationalCbox')
      .waitForElementVisible('@publicationsInternationalCboxCountry')
      .clearValue( '@publicationsInternationalCboxCountry')
      .setValue( '@publicationsInternationalCboxCountry', "Country")
      .click('@publicationsInterdisciplinaryCbox')
      .click('@publicationsCommunityEngagementCbox')
      .click('@publicationsPublishCbox')
      .click('@publicationsPublishCbox')

  },
  editPublicationsSort() {
    return this
      .waitForElementVisible('@publicationsSortButton')
      .click('@publicationsSortButton')
    },
  editPublicationsShowHide() {
    return this
      .waitForElementVisible('@publicationsShowHideButton')
      .click('@publicationsShowHideButton')
  },
  editPublicationsShowHideSelect() {
    return this
      .waitForElementVisible('@publicationsShowHideBox')
      .click('@publicationsShowHideBox')
    },
  removeOnePublications() {
    return this
      .waitForElementVisible('@removeOnePublicationsButton')
      .click('@removeOnePublicationsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    publicationsAddButton: {
      selector: 'a[href="/publication/edit.html"]'
    },
    publicationsSortButton: {
      selector: 'a[href="/publication/sort.html"]'
    },
    publicationsShowHideButton: {
      selector: 'a[href="/publication/visible.html"]'
    },
    //
    publicationsTitle: {
      selector: '#title'
    },
    publicationsPublisher: {
      selector: '#publisher'
    },
    publicationsType: {
      selector: '#type'
    },
    publicationsTypeBook: {
      selector: "//option[text()='Book']",
      locateStrategy: 'xpath'
    },
    publicationsStatus: {
      selector: '#status'
    },
    publicationsStatusPublished: {
      selector: "//option[text()='Published']",
      locateStrategy: 'xpath'
    },
    publicationsPublishedDate: {
      selector: '#date'
    },
    publicationsVolume: {
      selector: '#volume'
    },
    publicationsLeadAuthor: {
      selector: '#leadAuthor'
    },
    publicationsCoAuthor1: {
      selector: '#coAuthorList0'
    },
    publicationsCoAuthor2: {
      selector: '#coAuthorList1'
    },
    publicationsMoreCoAuthorsButton: {
      selector: '#applybtncoauthor'
    },
    publicationsPages: {
      selector: '#pages'
    },
    publicationsAbstract: {
      selector: '#pubAbstract'
    },
    publicationsISBN: {
      selector: '#isbn'
    },
    publicationsURL: {
      selector: '#url'
    },
    publicationsMoreYearsCitedButton: {
      selector: '#applybtn'
    },
    publicationsYearCited1: {
      selector: 'input[name="publicationCitations[0].citedYear"]'
    },
    publicationsYearCited2: {
      selector: 'input[name="publicationCitations[1].citedYear"]'
    },
    publicationsCountCited1: {
      selector: 'input[name="publicationCitations[0].citedCount"]'
    },
    publicationsCountCited2: {
      selector: 'input[name="publicationCitations[1].citedCount"]'
    },
    //
    publicationsInternationalCbox: {
      selector: '#checked_0'
    },
    publicationsInternationalCboxCountry: {
      selector: '#value_0'
    },
    publicationsInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    publicationsCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    publicationsPublishCbox: {
      selector: '#displayable1'
    },
    publicationsShowHideBox: {
      selector: 'input[name="list[0].displayable"]'
    },
    removeOnePublicationsButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
