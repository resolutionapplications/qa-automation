const pageCommands = {
  editTeaPhilosophy() {
    return this
      .waitForElementVisible('@philosophyEditLabel')
      .click('@philosophyEditLabel')
  },
  editTeaPhilosophyAdd() {
    return this
      .waitForElementVisible('@philosophyEditContentButton')
      .click('@philosophyEditContentButton')
    },
  editTeaPhilosophyDelete() {
      return this
        .waitForElementVisible('@philosophyDeleteContentButton')
        .click('@philosophyDeleteContentButton')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    philosophyEditLabel: {
      selector: 'a[href="index.html#teachingphilosophy"]'
    },
    philosophyEditContentButton: {
      selector: 'a[href="/teachingphilosophy/edit.html"]'
    },
    philosophyDeleteContentButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    philosophyContent: {
      selector: '#cke_paragraph'
    },
  }
};
