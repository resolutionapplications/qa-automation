const pageCommands = {
  facultyPastAppointments() {
    return this
      .waitForElementVisible('@pastAppointmentsLabel')
      .click('@pastAppointmentsLabel')
  },
  facultyPastAppointmentsAdd() {
    return this
      .waitForElementVisible('@pastAppointmentsAddButton')
      .click('@pastAppointmentsAddButton')
  },
 facultyPastAppointmentsAddContent() {
      return this
        .waitForElementVisible('@pastAppointmentsInstitution')
        .click('@pastAppointmentsInstitution')
        .waitForElementVisible('@pastAppointmentsInstitutionInput')
        .setValue('@pastAppointmentsInstitutionInput',"univ")
        .waitForElementVisible('@pastAppointmentsInstitutionUnivClick')
        .click('@pastAppointmentsInstitutionUnivClick')
        .waitForElementVisible('@pastAppointmentsRank')
        .click('@pastAppointmentsRank')
        .waitForElementVisible('@pastAppointmentsRankProffesor')
        .click('@pastAppointmentsRankProffesor')
        .clearValue('@pastAppointmentsStartDate')
        .setValue('@pastAppointmentsStartDate',"01/01/2000")
        .clearValue('@pastAppointmentsEndDate')
        .setValue('@pastAppointmentsEndDate',"01/01/2010")
        
  },
  removeOnepastAppointments() {
    return this
      .waitForElementVisible('@removeOnepastAppointmentsButton')
      .click('@removeOnepastAppointmentsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    pastAppointmentsLabel: {
      selector: 'a[href="past_appointments.html?emplid=RA102153"]'
    },
    pastAppointmentsAddButton: {
      selector: 'a[href="past_appointment_edit.html?emplid=RA102153"]'
    },
    removeOnepastAppointmentsButton: {
      selector: 'img[src="images/icons/delete.png"]'
    },
    pastAppointmentsInstitution: {
      selector: '//*[@id="s2id_institution.id"]',
      locateStrategy:'xpath'
    },
    pastAppointmentsInstitutionInput: {
      selector: 'input[id="s2id_autogen1_search"]'
    },
    pastAppointmentsInstitutionUnivClick: {
      selector:'#select2-result-label-2'
    },
    pastAppointmentsRank: {
      selector: '#rank_id_chosen'
    },
    pastAppointmentsRankProffesor: {
      selector: "//li[text()='Professor']",
      locateStrategy: 'xpath'
    },
     pastAppointmentsStartDate: {
      selector: '#start'
    },
    pastAppointmentsEndDate: {
      selector: '#endDate'
    }
   


  }
};
