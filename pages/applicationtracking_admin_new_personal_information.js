const pageCommands = {
  clickNewPersonalInfo(){
    return this
      .waitForElementVisible('@newPersonalInformationFirstLabel')
      .click('@newPersonalInformationFirstLabel')
  },
  fillNewPersonalInfo() {
    return this
      .waitForElementVisible('@preferredNameFirst')
      .clearValue('@preferredNameFirst')
      .setValue('@preferredNameFirst',"Namen")
      .clearValue('@preferreedNameMiddle')
      .setValue('@preferreedNameMiddle',"Middlem")
      .clearValue('@preferredNameLast')
      .setValue('@preferredNameLast',"Surnames")
      .clearValue('@otherNameFirst')
      .setValue('@otherNameFirst',"Namen")
      .clearValue('@otherNameMiddle')
      .setValue('@otherNameMiddle',"Middlem")
      .clearValue('@otherNameLast')
      .setValue('@otherNameLast',"Surnames")
      .clearValue('@addressLineOne')
      .setValue('@addressLineOne',"Street name")
      .clearValue('@addressLineTwo')
      .setValue('@addressLineTwo',"Street name second")
      .clearValue('@addressLineThree')
      .setValue('@addressLineThree',"Street name third")
      .clearValue('@city')
      .setValue('@city',"Citycity")
      .clearValue('@state')
      .setValue('@state',"BH")
      .clearValue('@zip')
      .setValue('@zip',"71000")
      .clearValue('@workEmail')
      .setValue('@workEmail',"email@gmail.com")
      .clearValue('@emailEmail')
      .setValue('@emailEmail',"aaa@gmail.com")
      .clearValue('@officePhone')
      .setValue('@officePhone',"777-888-9999")
      .clearValue('@mobilePhone')
      .setValue('@mobilePhone',"061-061-0611")

  },
  checkIsNewPersonalOpen() {
    return this
      .waitForElementVisible('@appNewPersonalInformationLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    newPersonalInformationFirstLabel: {
      selector: "a[href='personal_info.html']"
    },
    appNewPersonalInformationLabel: {
      selector: "//span[text()='New Personal Information']",
      locateStrategy: 'xpath'
    },
    preferredNameFirst: {
      selector: 'input[name="prefName.first"]'
    },
    preferreedNameMiddle: {
      selector: 'input[name="prefName.middle"]'
    },
    preferredNameLast: {
      selector: 'input[name="prefName.last"]'
    },
    otherNameFirst: {
      selector: 'input[name="otherName.first"]'
    },
    otherNameMiddle: {
      selector: 'input[name="otherName.middle"]'
    },
    otherNameLast: {
      selector: 'input[name="otherName.last"]'
    },
    addressLineOne: {
      selector: 'input[name="location.line1"]'
    },
    addressLineTwo: {
      selector: 'input[name="location.line2"]'
    },
    addressLineThree: {
      selector: 'input[name="location.line3"]'
    },
    city: {
      selector: 'input[name="location.city"]'
    },
    state: {
      selector: 'input[name="location.state"]'
    },
    zip: {
      selector: 'input[name="location.zip"]'
    },
    workEmail: {
      selector: 'input[name="professionalEmail.email"]'
    },
    emailEmail: {
      selector: 'input[name="personalEmail.email"]'
    },
    officePhone: {
      selector: 'input[name="office.phone"]'
    },
    mobilePhone: {
      selector: 'input[name="mobile.phone"]'
    }
  }
};
