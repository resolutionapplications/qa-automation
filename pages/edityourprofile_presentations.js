const pageCommands = {
  editPresentationsAdd() {
    return this
    .waitForElementVisible('@presentationsAddButton')
    .click('@presentationsAddButton')
  },
  editPresentationsAddContent() {
      return this
      .waitForElementVisible('@presentationsType')
      .click('@presentationsType')
      .waitForElementVisible('@presentationsTypeConferencePaper')
      .click('@presentationsTypeConferencePaper')
      .click('@presentationsStatus')
      .waitForElementVisible('@presentationsStatusPresented')
      .click('@presentationsStatusPresented')
      .clearValue( '@presentationsCitation')
      .setValue( '@presentationsCitation', "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.")
      .clearValue( '@presentationsURL')
      .setValue( '@presentationsURL', "http://www.url.com")
      .clearValue( '@presentationsDate')
      .setValue( '@presentationsDate', "01/01/2010")
      //
      .click('@presentationsInternationalCbox')
      .waitForElementVisible('@presentationsInternationalCboxCountry')
      .clearValue( '@presentationsInternationalCboxCountry')
      .setValue( '@presentationsInternationalCboxCountry', "Country")
      .click('@presentationsInterdisciplinaryCbox')
      .click('@presentationsCommunityEngagementCbox')
      .click('@presentationsPublishCbox')
      .click('@presentationsPublishCbox')

  },
  editPresentationsSort() {
    return this
      .waitForElementVisible('@presentationsSortButton')
      .click('@presentationsSortButton')
    },
  editPresentationsShowHide() {
    return this
      .waitForElementVisible('@presentationsShowHideButton')
      .click('@presentationsShowHideButton')
  },
  editPresentationsShowHideSelect() {
    return this
      .waitForElementVisible('@presentationsShowHideBox')
      .click('@presentationsShowHideBox')
    },
  removeOnePresentations() {
    return this
      .waitForElementVisible('@removeOnePresentationsButton')
      .click('@removeOnePresentationsButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    presentationsAddButton: {
      selector: 'a[href="/presentation/edit.html"]'
    },
    presentationsSortButton: {
      selector: 'a[href="/presentation/sort.html"]'
    },
    presentationsShowHideButton: {
      selector: 'a[href="/presentation/visible.html"]'
    },
    //
    presentationsType: {
      selector: '#type'
    },
    presentationsTypeConferencePaper: {
      selector: "//option[text()='Conference Paper']",
      locateStrategy: 'xpath'
    },
    presentationsStatus: {
      selector: '#status'
    },
    presentationsStatusPresented: {
      selector: "//option[text()='Presented']",
      locateStrategy: 'xpath'
    },
    presentationsDate: {
      selector: '#date'
    },
    presentationsCitation: {
      selector: '#citation'
    },
    presentationsURL: {
      selector: '#url'
    },
    //
    presentationsInternationalCbox: {
      selector: '#checked_0'
    },
    presentationsInternationalCboxCountry: {
      selector: '#value_0'
    },
    presentationsInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    presentationsCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    presentationsPublishCbox: {
      selector: '#displayable1'
    },
    presentationsShowHideBox: {
      selector: 'input[name="list[0].displayable"]'
    },
    removeOnePresentationsButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
