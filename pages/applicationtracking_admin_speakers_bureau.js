const pageCommands = {
  clickSpeakersBureau(){
    return this
      .waitForElementVisible('@speakersBureauFirstLabel')
      .click('@speakersBureauFirstLabel')
  },
  plusSpeakersBureau() {
    return this
    .waitForElementVisible('@speakersBureauEditButton')
    .click('@speakersBureauEditButton')
  },
  fillSpeakersBureau() {
    return this
    .waitForElementVisible('@areaOfExpertise')
    .clearValue('@areaOfExpertise')
    .setValue('@areaOfExpertise',"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam ac sem id dapibus. Donec ut vehicula leo. Nulla auctor mi velit, non suscipit justo semper at. Nunc dolor tellus, varius at urna nec, gravida luctus felis. Donec neque ex, gravida eu lacus in, eleifend varius purus. Aliquam consequat, quam eu pharetra fringilla, justo nisl lacinia risus, ac lobortis purus tortor ac enim.")
    .waitForElementVisible('@speakingTopics')
    .clearValue('@speakingTopics')
    .setValue('@speakingTopics',"Mauris venenatis egestas imperdiet. In semper pellentesque elit et scelerisque. Nulla euismod sapien fermentum odio lacinia interdum. Morbi rhoncus iaculis consequat. Fusce et fermentum augue, sit amet aliquam dui. Integer placerat sapien in felis convallis, a feugiat dui maximus. Maecenas a condimentum dui, eget fermentum velit. Fusce ante metus, varius ac justo id, aliquam mattis ex. Cras volutpat velit diam.")
  },
  fillSpeakersBureauPart2() {
    return this
    .waitForElementVisible('@currentResearch')
    .clearValue('@currentResearch')
    .setValue('@currentResearch',"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam ac sem id dapibus. Donec ut vehicula leo. Nulla auctor mi velit, non suscipit justo semper at. Nunc dolor tellus, varius at urna nec, gravida luctus felis. Donec neque ex, gravida eu lacus in, eleifend varius purus. Aliquam consequat, quam eu pharetra fringilla, justo nisl lacinia risus, ac lobortis purus tortor ac enim.")
  },
  removeOneSpeakersBureau() {
    return this
    .waitForElementVisible('@removeOneSpeakersBureauButton')
    .click('@removeOneSpeakersBureauButton')
  },
  cancelSpeakersBureau() {
    return this
      .waitForElementVisible('@cancelSpeakersBureauButton')
      .click('@cancelSpeakersBureauButton')
  },
  saveSpeakersBureau() {
    return this
      .waitForElementVisible('@saveSpeakersBureauButton')
      .click('@saveSpeakersBureauButton')
  },
  nextSpeakersBureau() {
    return this
      .waitForElementVisible('@nextSpeakersBureauButton')
      .click('@nextSpeakersBureauButton')
  },
  checkIsSpeakersBureauOpen() {
    return this
      .waitForElementVisible('@appSpeakersBureauLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    speakersBureauFirstLabel: {
      selector: "a[href='speakers_bureau.html']"
    },
    appSpeakersBureauLabel: {
      selector: "//h2[text()='Speakers Bureau']",
      locateStrategy: 'xpath'
    },
    speakersBureauEditButton: {
      selector: 'a[href="teaching_activities_edit.html?type=101089&sb"]'
    },
    areaOfExpertise: {
      selector: '#description'
    },
    speakingTopics: {
        selector: '#description2'
    },
    currentResearch: {
        selector: '#information'
    },
    cancelSpeakersBureauButton: {
      selector: 'input[value="Cancel"]'
    },
    saveSpeakersBureauButton: {
      selector: 'input[value="Save"]'
    },
    nextSpeakersBureauButton: {
      selector: 'input[value="Next"]'
    },
    removeOneSpeakersBureauButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
  }
};
