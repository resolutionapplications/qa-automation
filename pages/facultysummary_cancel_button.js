const pageCommands = {
  facultySummaryCancel() {
    return this
      .waitForElementVisible('@cancelButton')
      .click('@cancelButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    cancelButton: {
      selector: 'input[value="Cancel"]'
    }
  }
};
