const pageCommands = {
  editBioLanguage() {
    return this
      .waitForElementVisible('@languageEditLabel')
      .click('@languageEditLabel')
  },
  editBioLanguageAdd() {
    return this
      .waitForElementVisible('@languageAddButton')
      .click('@languageAddButton')
    },
    editBioLanguageAddContent(enter) {
      return this
      .waitForElementVisible('@languageName')
      .clearValue('@languageName')
      .setValue('@languageName',"English")
      .setValue('@languageName',enter)
      .waitForElementVisible('@languageFluency')
      .click('@languageFluency')
      .waitForElementVisible('@languageFluencyFluent')
      .click('@languageFluencyFluent')
      .clearValue('@languageComments')
      .setValue('@languageComments',"Comments in English")
      .click('@languagePublish')
      .click('@languagePublish')
      },
  editBioLanguageSort() {
    return this
      .waitForElementVisible('@languageSortButton')
      .click('@languageSortButton')
    },
  editBioLanguageShowHide() {
    return this
      .waitForElementVisible('@languageShowHideButton')
      .click('@languageShowHideButton')
  },
  editBioLanguageShowHideSelect() {
    return this
      .waitForElementVisible('@languageShowHideBox')
      .click('@languageShowHideBox')
  },
  editBioLanguageDelete() {
    return this
      .waitForElementVisible('@languageDeleteButton')
      .click('@languageDeleteButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    languageEditLabel: {
      selector: 'a[href="index.html#language"]'
    },
    languageAddButton: {
      selector: 'a[href="/language/edit.html"]'
    },
    languageSortButton: {
      selector: 'a[href="/language/sort.html"]'
    },
    languageShowHideButton: {
      selector: 'a[href="/language/visible.html"]'
    },
    languageName: {
      selector: '#tongue_input'
    },
    languageFluency: {
      selector: '#fluency'
    },
    languageFluencyFluent: {
      selector: "//option[text()='fluent']",
      locateStrategy: 'xpath'
    },
    languageComments: {
      selector: '#comments'
    },
    languagePublish: {
      selector: '#displayed1'
    },
    languageShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
    languageDeleteButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    }
  }
};
