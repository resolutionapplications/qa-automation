const pageCommands = {
  clickTitleLetters(){
    return this
      .waitForElementVisible('@titleLettersFirstLabel')
      .click('@titleLettersFirstLabel')
  },
  plusTitleLetters() {
    return this
    .waitForElementVisible('@titleLettersEditButton')
    .click('@titleLettersEditButton')
  },
  fillTittleLetters() {
    return this
      .waitForElementVisible('@departmentTitleLetters')
      .click('@departmentTitleLetters')
      .waitForElementVisible('@departmentTitleLettersDoA')
      .click('@departmentTitleLettersDoA')
      .waitForElementVisible('@titleTitleLetters')
      .click('@titleTitleLetters')
      .waitForElementVisible('@titleTitleLettersAssistant')
      .click('@titleTitleLettersAssistant')
      
  },
  fillTittleLettersPart2() {
    return this
      .waitForElementVisible('@departmentTitleLetters')
      .click('@departmentTitleLetters')
      .waitForElementVisible('@departmentTitleLettersDoB')
      .click('@departmentTitleLettersDoB')
      .waitForElementVisible('@titleTitleLetters')
      .click('@titleTitleLetters')
      .waitForElementVisible('@titleTitleLettersAssociate')
      .click('@titleTitleLettersAssociate')
      .waitForElementVisible('@experienceTitleLetters')
      .click('@experienceTitleLetters')
      .waitForElementVisible('@institutionTitleLetters')
      .clearValue('@institutionTitleLetters')
      .setValue('@institutionTitleLetters',"My institution")
      
  },
  cancelTitleLetters() {
    return this
      .waitForElementVisible('@cancelTitleLettersButton')
      .click('@cancelTitleLettersButton')
  },
  saveTitleLetters() {
    return this
      .waitForElementVisible('@saveTitleLettersButton')
      .click('@saveTitleLettersButton')
  },
  saveAndAddAnotherTitleLetters() {
    return this
      .waitForElementVisible('@saveAndAddAnotherTitleLettersButton')
      .click('@saveAndAddAnotherTitleLettersButton')
  },
  removeOneTitleLetters() {
    return this
    .waitForElementVisible('@removeOneTitleLettersButton')
    .click('@removeOneTitleLettersButton')
  },
  checkIsTitleLettersOpen() {
    return this
      .waitForElementVisible('@appTitleLettersLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    titleLettersFirstLabel: {
      selector: "a[href='letters_of_recommendation.html']"
    },
    appTitleLettersLabel: {
      selector: "//h2[text()='Title & Letters of Recommendation']",
      locateStrategy: 'xpath'
    },
    titleLettersEditButton: {
      selector: 'a[href="proposed_title_edit.html"]'
    },
    departmentTitleLetters: {
      selector: '#department_chosen'
    },
    departmentTitleLettersDoA: {
      selector: "//li[text()='Department of Anesthesiology']",
      locateStrategy: 'xpath'
    },
    departmentTitleLettersDoB: {
      selector: "//li[text()='Department of Biomedical Sciences']",
      locateStrategy: 'xpath'
    },
    titleTitleLetters: {
        selector: '#title'
    },
    titleTitleLettersAssistant: {
      selector: "//option[text()='Assistant Professor']",
      locateStrategy: 'xpath'
    },
    titleTitleLettersAssociate: {
      selector: "//option[text()='Associate Professor']",
      locateStrategy: 'xpath'
    },
    experienceTitleLetters: {
        selector: '#experience1'
    },
    institutionTitleLetters: {
        selector: '#institution'
    },
    cancelTitleLettersButton: {
      selector: 'input[value="Cancel"]'
    },
    saveTitleLettersButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddAnotherTitleLettersButton: {
      selector: 'input[name="_add"]'
    },
    removeOneTitleLettersButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
  }
};
