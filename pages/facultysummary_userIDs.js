const pageCommands = {
  userIDs(){
    return this
      .waitForElementVisible('@userIDsLabel')
      .click('@userIDsLabel')
  },
  facultyUserIDsAdd() {
    return this
    .waitForElementVisible('@userIDsAddButton')
    .click('@userIDsAddButton')
  },
  facultyUserIDsAddContent() {
    return this
    .waitForElementVisible('@userIDsEmployeeID')
    .clearValue('@userIDsEmployeeID')
    .setValue('@userIDsEmployeeID',"My ID")
    .clearValue('@userIDsSingle')
    .setValue('@userIDsSingle',"Single ID")
    .clearValue('@userIDsAAMC')
    .setValue('@userIDsAAMC',"12345678")
    .clearValue('@userIDsNationalProvider')
    .setValue('@userIDsNationalProvider',"PMC1234567")
    .clearValue('@userIDsCatCard')
    .setValue('@userIDsCatCard',"Card ID")
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    userIDsLabel: {
      selector: "a[href='user_id.html?emplid=RA102153']"
    },
    userIDsAddButton: {
      selector: 'a[href="user_id_edit.html?emplid=RA102153"]'
    },
    userIDsEmployeeID: {
      selector: '#employeeId'
    },
    userIDsSingle: {
        selector: '#SSOId'
    },
    userIDsAAMC: {
        selector: '#aamcId'
    },
    userIDsNationalProvider: {
        selector: '#npiId'
    },
    userIDsCatCard: {
      selector: '#catId'
    }
  }
};
