const pageCommands = {
  clickSignificantAcc(){
    return this
      .waitForElementVisible('@significantAccFirstLabel')
      .click('@significantAccFirstLabel')
  },
  plusSignificantAcc() {
    return this
    .waitForElementVisible('@significantAccEditButton')
    .click('@significantAccEditButton')
  },
  fillSignificantAcc() {
    return this
      .waitForElementVisible('@titleSignificantAcc')
      .clearValue('@titleSignificantAcc')
      .setValue('@titleSignificantAcc',"My title")
      .clearValue('@descriptionSignificantAcc')
      .setValue('@descriptionSignificantAcc',`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam ac sem id dapibus. Donec ut vehicula leo. Nulla auctor mi velit, non suscipit justo semper at.`)
      .clearValue('@dateSignificantAcc')
      .setValue('@dateSignificantAcc',"11/01/2000")

  },
  cancelSignificantAcc() {
    return this
      .waitForElementVisible('@cancelSignificantAccButton')
      .click('@cancelSignificantAccButton')
  },
  saveSignificantAcc() {
    return this
      .waitForElementVisible('@saveSignificantAccButton')
      .click('@saveSignificantAccButton')
  },
  saveAndAddSignificantAcc() {
    return this
      .waitForElementVisible('@saveAndAddSignificantAccButton')
      .click('@saveAndAddSignificantAccButton')
  },
  removeOneSignificantAcc() {
    return this
      .waitForElementVisible('@removeOneSignificantAccButton')
      .click('@removeOneSignificantAccButton')
  },

  checkIsSignificantAccOpen() {
    return this
      .waitForElementVisible('@appSignificantAccLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    significantAccFirstLabel: {
      selector: "a[href='accomplishments.html']"
    },
    appSignificantAccLabel: {
      selector: "//h2[text()='Significant Accomplishments']",
      locateStrategy: 'xpath'
    },
    significantAccEditButton: {
      selector: 'a[href="accomplishment_edit.html"]'
    },
    titleSignificantAcc: {
      selector: '#title'
    },
    descriptionSignificantAcc: {
      selector: '#description'
    },
    dateSignificantAcc: {
      selector: '#date'
    },

    removeOneSignificantAccButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
    cancelSignificantAccButton: {
      selector: 'input[value="Cancel"]'
    },
    saveSignificantAccButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddSignificantAccButton: {
      selector: 'input[value="Save & Add Another"]'
    },


  }
};
