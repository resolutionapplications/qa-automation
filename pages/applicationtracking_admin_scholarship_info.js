const pageCommands = {
  clickScholarshipInfo(){
    return this
      .waitForElementVisible('@scholarshipInfoFirstLabel')
      .click('@scholarshipInfoFirstLabel')
  },
  fillScholarshipInfo(){
    return this
      .waitForElementVisible('@scholarshipClinic')
      .click('@scholarshipClinic')
      .waitForElementVisible('@scholarshipClinicMedicine')
      .click('@scholarshipClinicMedicine')
  },
  checkIsScholarshipInfoOpen() {
    return this
      .waitForElementVisible('@appScholarshipInfoLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    scholarshipInfoFirstLabel: {
      selector: "a[href='self_disclosure.html?id=9004']"
    },
    appScholarshipInfoLabel: {
      selector: "//h2[text()='Scholarship Info']",
      locateStrategy: 'xpath'
    },
    scholarshipClinic: {
      selector: '//*[@id="answer_chosen"]',
      locateStrategy: 'xpath'
    },
    scholarshipClinicMedicine: {
      selector: "//li[text()='Family Medicine - Oakland']",
      locateStrategy: 'xpath'
    }
  }
};
