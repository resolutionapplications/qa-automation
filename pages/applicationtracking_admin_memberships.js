const pageCommands = {
  clickMemberships(){
    return this
      .waitForElementVisible('@membershipsFirstLabel')
      .click('@membershipsFirstLabel')
  },
  plusMemberships() {
    return this
    .waitForElementVisible('@membershipsEditButton')
    .click('@membershipsEditButton')
  },
  fillMemberships() {
    return this
      .waitForElementVisible('@organizationMembership')
      .click('@organizationMembership')
      .waitForElementVisible('@organizationMembershipOther')
      .click('@organizationMembershipOther')
      .clearValue('@organizationDescriptionMembership')
      .setValue('@organizationDescriptionMembership',"Description")
      .clearValue('@startMembership')
      .setValue('@startMembership',"11/01/2000")
      .clearValue('@endMembership')
      .setValue('@endMembership',"11/01/2012")
  },
  cancelMemberships() {
    return this
      .waitForElementVisible('@cancelMembershipsButton')
      .click('@cancelMembershipsButton')
  },
  saveMemberships() {
    return this
      .waitForElementVisible('@saveMembershipsButton')
      .click('@saveMembershipsButton')
  },
  saveAndAddMemberships() {
    return this
      .waitForElementVisible('@saveAndAddMembershipsButton')
      .click('@saveAndAddMembershipsButton')
  },
  removeOneMemberships() {
    return this
      .waitForElementVisible('@removeOneMembershipsButton')
      .click('@removeOneMembershipsButton')
  },

  checkIsMembershipsOpen() {
    return this
      .waitForElementVisible('@appMembershipsLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    membershipsFirstLabel: {
      selector: "a[href='memberships.html']"
    },
    appMembershipsLabel: {
      selector: "//h2[text()='Membership in Professional Societies and Organizations']",
      locateStrategy: 'xpath'
    },
    membershipsEditButton: {
      selector: 'a[href="membership_edit.html"]'
    },
    organizationMembership: {
      selector: '#organization_id_chosen'
    },
    organizationMembershipOther: {
      selector: "//li[text()='Other']",
      locateStrategy: 'xpath'
    },
    organizationDescriptionMembership: {
      selector: '#organizationDescription'
    },
    startMembership: {
      selector: '#start'
    },
    endMembership: {
      selector: '#end'
    },
    removeOneMembershipsButton: {
      selector: 'img[src="../images/icons/delete.png"]'
    },
    cancelMembershipsButton: {
      selector: 'input[value="Cancel"]'
    },
    saveMembershipsButton: {
      selector: 'input[value="Save"]'
    },
    saveAndAddMembershipsButton: {
      selector: 'input[value="Save & Add Another"]'
    },


  }
};
