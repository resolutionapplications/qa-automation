const pageCommands = {
  editTeaPedagogical() {
    return this
      .waitForElementVisible('@pedagogicalEditLabel')
      .click('@pedagogicalEditLabel')
  },
  editTeaPedagogicalAdd() {
    return this
      .waitForElementVisible('@pedagogicalAddButton')
      .click('@pedagogicalAddButton')
  },
  editTeaPedagogicalAddContent() {
      return this
        .waitForElementVisible('@pedagogicalPublicationType')
        .click('@pedagogicalPublicationType')
        .waitForElementVisible('@pedagogicalPublicationTypePaper')
        .click('@pedagogicalPublicationTypePaper')
        .clearValue('@pedagogicalCitation')
        .setValue('@pedagogicalCitation',"Short description")
        .click('@pedagogicalStatus')
        .waitForElementVisible('@pedagogicalStatusPublished')
        .click('@pedagogicalStatusPublished')
        .clearValue('@pedagogicalDate')
        .setValue('@pedagogicalDate',"05/05/2005")
        .clearValue('@pedagogicalURL')
        .setValue('@pedagogicalURL',"http://www.url.com")
        .click('@pedagogicalPublishCbox')
        .click('@pedagogicalPublishCbox')
  },
  editTeaPedagogicalSort() {
    return this
      .waitForElementVisible('@pedagogicalSortButton')
      .click('@pedagogicalSortButton')
    },
  editTeaPedagogicalShowHide() {
    return this
      .waitForElementVisible('@pedagogicalShowHideButton')
      .click('@pedagogicalShowHideButton')
  },
   editTeaPedagogicalShowHideSelect() {
    return this
      .waitForElementVisible('@pedagogicalShowHideBox')
      .click('@pedagogicalShowHideBox')
    },
  editTeaPedagogicalDelete() {
    return this
      .waitForElementVisible('@pedagogicalDeleteButton')
      .click('@pedagogicalDeleteButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    pedagogicalEditLabel: {
      selector: 'a[href="index.html#pedagogical_publications"]'
    },
    pedagogicalAddButton: {
      selector: 'a[href="/pedagogical/edit.html"]'
    },
    pedagogicalSortButton: {
      selector: 'a[href="/pedagogical/sort.html"]'
    },
    pedagogicalShowHideButton: {
      selector: 'a[href="/pedagogical/visible.html"]'
    },
    pedagogicalDeleteButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    pedagogicalPublicationType: {
      selector: '#type'
    },
    pedagogicalPublicationTypePaper: {
      selector: "//option[text()='Paper']",
      locateStrategy: 'xpath'
    },
    pedagogicalCitation: {
      selector: '#citation'
    },
    pedagogicalStatus: {
      selector: '#status'
    },
    pedagogicalStatusPublished: {
      selector: "//option[text()='Published']",
      locateStrategy: 'xpath'
    },
    pedagogicalDate: {
      selector: '#date'
    },
    pedagogicalURL: {
      selector: '#url'
    },
    pedagogicalPublishCbox: {
      selector: '#displayable1'
    },
    pedagogicalCancelButton: {
      selector: 'input[name="_cancel"]'
    },
    pedagogicalShowHideBox: {
      selector: 'input[name="list[0].displayable"]'
    }

  }
};
