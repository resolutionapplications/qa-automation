const pageCommands = {
  editProffesionalService() {
    return this
    .waitForElementVisible('@proffesionalServiceEditLabel')
    .click('@proffesionalServiceEditLabel')
  },
  editProffesionalServiceAdd() {
    return this
    .waitForElementVisible('@proffesionalServiceAddButton')
    .click('@proffesionalServiceAddButton')
  },
  editProffesionalServiceAddContent() {
      return this
      .waitForElementVisible('@proffesionalServiceOrganization')
      .setValue('@proffesionalServiceOrganization',"My organization")
      .clearValue('@proffesionalServiceServicePerformed')
      .setValue('@proffesionalServiceServicePerformed',"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.")
      .clearValue('@proffesionalServiceRole')
      .setValue('@proffesionalServiceRole',"My role")
      .click( '@proffesionalServiceType')
      .clearValue( '@proffesionalServiceStartDate')
      .setValue( '@proffesionalServiceStartDate', "01/01/2000")
      .clearValue( '@proffesionalServiceEndDate')
      .setValue( '@proffesionalServiceEndDate', "01/01/2020")
      .clearValue('@proffesionalServiceHours')
      .setValue('@proffesionalServiceHours',"6")
      .click('@proffesionalServiceInternationalCbox')
      .waitForElementVisible('@proffesionalServiceInternationalCboxCountry')
      .clearValue( '@proffesionalServiceInternationalCboxCountry')
      .setValue( '@proffesionalServiceInternationalCboxCountry', "Country")
      .click('@proffesionalServiceInterdisciplinaryCbox')
      .click('@proffesionalServiceCommunityEngagementCbox')
      .click('@proffesionalServiceePublishCbox')
      .click('@proffesionalServiceePublishCbox')

  },
  editProffesionalServiceSort() {
    return this
      .waitForElementVisible('@proffesionalServiceSortButton')
      .click('@proffesionalServiceSortButton')
    },
 editProffesionalServiceShowHide() {
    return this
      .waitForElementVisible('@proffesionalServiceShowHideButton')
      .click('@proffesionalServiceShowHideButton')
  },
  editProffesionalServiceShowHideSelect() {
    return this
      .waitForElementVisible('@proffesionalServiceShowHideBox')
      .click('@proffesionalServiceShowHideBox')
    },
  removeOneProffesionalService() {
    return this
      .waitForElementVisible('@removeOneProffesionalServiceButton')
      .click('@removeOneProffesionalServiceButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    proffesionalServiceEditLabel: {
      selector:'a[href="index.html#profservice"]'
    },
   proffesionalServiceAddButton: {
      selector: 'a[href="/profservice/edit.html"]'
    },
    proffesionalServiceSortButton: {
      selector: 'a[href="/profservice/sort.html"]'
    },
    proffesionalServiceShowHideButton: {
      selector: 'a[href="/profservice/visible.html"]'
    },
    //
    proffesionalServiceOrganization: {
      selector: '#orgServed'
    },
    proffesionalServiceServicePerformed: {
      selector: "#servicePerformed"
    },
    proffesionalServiceRole: {
      selector: '#partPlayed'
    },
   proffesionalServiceType: {
      selector: "#scholarlyType1"
    },
    proffesionalServiceStartDate: {
      selector: '#startDate'
    },
    proffesionalServiceEndDate: {
      selector: '#endDate'
    },
    proffesionalServiceHours: {
      selector: '#hours'
    },
    proffesionalServiceInternationalCbox: {
      selector: '#checked_0'
    },
    proffesionalServiceInternationalCboxCountry: {
      selector: '#value_0'
    },
    proffesionalServiceInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    proffesionalServiceCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    proffesionalServiceePublishCbox: {
      selector: '#displayed1'
    },
    proffesionalServiceShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
   removeOneProffesionalServiceButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
