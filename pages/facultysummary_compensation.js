const pageCommands = {
  facultyCompensation() {
    return this
      .waitForElementVisible('@compensationLabel')
      .click('@compensationLabel')
  },
  facultyCompensationAdd() {
    return this
    .waitForElementVisible('@compensationAddButton')
    .click('@compensationAddButton')
  },
  facultyCompensationAddContent() {
     this
      .waitForElementVisible('@compensationStatus')
      .click( '@compensationStatus')
      .waitForElementVisible( '@compensationStatusSalaried')
      .click( '@compensationStatusSalaried')
      .clearValue('@compensationTitle')
      .setValue('@compensationTitle',"My title")
      .waitForElementVisible( '@compensationActivity')
      .click( '@compensationActivity')
      .waitForElementVisible( '@compensationActivityMentor')
      .click( '@compensationActivityMentor')
      .waitForElementVisible( '@compensationJobDescription')
      .moveToElement( '@compensationJobDescription',50,200)
      this.api.mouseButtonClick( 'left')
      this.api.keys( 'Lorem ipsum dolor sit amet')
      this.clearValue( '@compensationUAFTE')
      .setValue( '@compensationUAFTE', "0.5")
      .clearValue('@compensationBeginDate')
      .setValue('@compensationBeginDate',"01/01/2000")
      .clearValue( '@compensationEndDate')
      .setValue('@compensationEndDate',"01/01/2010")
      .clearValue('@compensationOtherFTE')
      .setValue('@compensationOtherFTE',"0.5")
      .clearValue('@compensationSalary')
      .setValue('@compensationSalary',"500")
      return this;
  },
  removeOneCompensation() {
    return this
      .waitForElementVisible('@removeOneCompensationButton')
      .click('@removeOneCompensationButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    compensationLabel: {
      selector: 'a[href="compensation.html?emplid=RA102153"]'
    },
    compensationAddButton: {
      selector: 'a[href="compensation_edit.html?emplid=RA102153"]'
    },
    compensationStatus: {
      selector: '#status_id_chosen'
    },
    compensationStatusSalaried: {
      selector: "//li[text()='Salaried']",
      locateStrategy: 'xpath'
    },
    compensationTitle: {
      selector: '#title'
    },
    compensationActivity: {
      selector: '#activity_id_chosen'
    },
    compensationActivityMentor: {
       selector: "//li[text()='Banner-UMC Phoenix Physician-Preceptor']",
      locateStrategy: 'xpath'
    },
    compensationJobDescription: {
      selector: '//*[@id="content_container"]/div/div[2]/div/div[2]/div/form/div[1]/table/tbody/tr[4]/td/div',
      locateStrategy: 'xpath'
    },
    compensationUAFTE: {
      selector: '#uaFte'
    },
    compensationBeginDate: {
      selector: '#uaBeginDate'
    },
    compensationEndDate: {
      selector: '#uaEndDate'
    },
    compensationOtherFTE: {
      selector: '#otherFte'
    },
    compensationSalary: {
      selector: '#salary'
    },
    removeOneCompensationButton: {
      selector: 'img[src="images/icons/delete.png"]'
    }

  }
};
