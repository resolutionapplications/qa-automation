const pageCommands = {
  clickCommunityClinical(){
    return this
      .waitForElementVisible('@communityClinicalFirstLabel')
      .click('@communityClinicalFirstLabel')
  },
  fillCommunityClinical() {
    return this
      .waitForElementVisible('@mobilePhone')
      .clearValue('@mobilePhone')
      .setValue('@mobilePhone',"061-061-0610")
      .clearValue('@url')
      .setValue('@url',"http://www.url.com")
      .clearValue('@year')
      .setValue('@year',"2010")
      .click('@doctorClick')
      .clearValue('@name')
      .setValue('@name',"Assistant")
      .clearValue('@assistantPhone')
      .setValue('@assistantPhone',"777-888-9999")
      .clearValue('@assistantEmail')
      .setValue('@assistantEmail',"aaa@gmail.com")
      .clearValue('@addressLineOne')
      .setValue('@addressLineOne',"Street name")
      .clearValue('@addressLineTwo')
      .setValue('@addressLineTwo',"Street name second")
      .clearValue('@addressLineThree')
      .setValue('@addressLineThree',"Street name third")
      .clearValue('@city')
      .setValue('@city',"Citycity")
      .clearValue('@state')
      .setValue('@state',"BH")
      .clearValue('@zip')
      .setValue('@zip',"71000")

  },
  checkIsCommunityClinicalOpen() {
    return this
      .waitForElementVisible('@appCommunityClinicalLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    communityClinicalFirstLabel: {
      selector: "a[href='contact_information.html']"
    },
    appCommunityClinicalLabel: {
      selector: "//h1[text()='Community Clinical Experience']",
      locateStrategy: 'xpath'
    },
    mobilePhone: {
      selector: 'input[name="preceptor.phone"]'
    },
    url: {
      selector: 'input[name="url.url"]'
    },
    year: {
      selector: 'input[name="year"]'
    },
    doctorClick: {
      selector: '#contact1'
    },
    name: {
      selector: 'input[name="assistant.description"]'
    },
    addressLineOne: {
      selector: 'input[name="address.line1"]'
    },
    addressLineTwo: {
      selector: 'input[name="address.line2"]'
    },
    addressLineThree: {
      selector: 'input[name="address.line3"]'
    },
    city: {
      selector: 'input[name="address.city"]'
    },
    state: {
      selector: 'input[name="address.state"]'
    },
    zip: {
      selector: 'input[name="address.zip"]'
    },
    assistantEmail: {
      selector: 'input[name="assistant.email"]'
    },
    assistantPhone: {
      selector: 'input[name="assistant.phone"]'
    }
  
  }
};
