const pageCommands = {
  facultySummaryAddContent() {
    return this
      .waitForElementVisible('@facultySummaryName')
      .setValue('@facultySummaryName',"surname")
      .click('@facultySummarySearchButton')
      .waitForElementVisible('@facultySummaryClick')
      .click('@facultySummaryClick')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/search.html',
  commands: [pageCommands],
  elements: {
    facultySummaryName: {
      selector: 'input[name="fieldsById[100001].value"]'
    },
     facultySummarySearchButton: {
      selector: 'input[type=submit]'
    },
    facultySummarySearchActive: {
      selector: '#active1'
    },
    facultySummaryClick: {
      selector:'a[href="load.html?emplid=RA102153"]'
    }
    // paragraph
  }
};
