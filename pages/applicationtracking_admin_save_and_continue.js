const pageCommands = {
  saveAndContinue() {
    return this
    .waitForElementVisible('@saveAndContinueButton')
    .click('@saveAndContinueButton')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    saveAndContinueButton: {
      selector: 'input[value="Save & Continue"]'
    },



  }
};
