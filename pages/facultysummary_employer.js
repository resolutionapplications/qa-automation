const pageCommands = {
  facultyEmployer() {
    return this
      .waitForElementVisible('@employerLabel')
      .click('@employerLabel')
  },
  facultyEmployerAdd() {
    return this
    .waitForElementVisible('@employerAddButton')
    .click('@employerAddButton')
  },
 facultyEmployerAddContent() {
      return this
      .waitForElementVisible('@employerInstitution')
      .click( '@employerInstitution')
      .waitForElementVisible( '@employerInstitutionOther')
      .click( '@employerInstitutionOther')
      .clearValue('@employerFTE')
      .setValue('@employerFTE',"0.0")
  },
  removeOneEmployer() {
    return this
      .waitForElementVisible('@removeOneEmployerButton')
      .click('@removeOneEmployerButton')
  }
};
module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter/summary.html?emplid=RA102153',
  commands: [pageCommands],
  elements: {
    employerLabel: {
      selector: 'a[href="affiliations.html?emplid=RA102153"]'
    },
    employerAddButton: {
      selector: 'a[href="affiliations_affiliates_edit.html?emplid=RA102153"]'
    },
   employerInstitution: {
      selector: '#institution_id_chosen'
    },
    employerInstitutionOther: {
      selector: "//li[text()='Other']",
      locateStrategy: 'xpath'
    },
      employerFTE: {
      selector: '#fte'
    },
     removeOneEmployerButton: {
      selector: 'img[src="images/icons/delete.png"]'
    }

  }
};
