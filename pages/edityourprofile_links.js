const pageCommands = {
  editHome() {
    return this
      .waitForElementVisible('@editProfileHome')
      .click('@editProfileHome')
  },
  editBiography() {
    return this
      .waitForElementVisible('@editProfileBiography')
      .click('@editProfileBiography')
  },
  editTeaching() {
    return this
      .waitForElementVisible('@editProfileTeaching')
      .click('@editProfileTeaching')
  },
  editResearch() {
    return this
      .waitForElementVisible('@editProfileResearch')
      .click('@editProfileResearch')
  },
  editPublications() {
    return this
      .waitForElementVisible('@editProfilePublications')
      .click('@editProfilePublications')
  },
  editPresentations() {
    return this
      .waitForElementVisible('@editProfilePresentations')
      .click('@editProfilePresentations')
  },
  editFacultyPractice() {
    return this
      .waitForElementVisible('@editProfileFacultyPractice')
      .click('@editProfileFacultyPractice')
  },
  editGraduateStudents() {
    return this
      .waitForElementVisible('@editProfileGraduateStudents')
      .click('@editProfileGraduateStudents')
  },
  editEntrepreneurialExperience() {
    return this
      .waitForElementVisible('@editProfileEntrepreneurialExperience')
      .click('@editProfileEntrepreneurialExperience')
  },
  editService() {
    return this
      .waitForElementVisible('@editProfileService')
      .click('@editProfileService')
  },
  editContactInformation() {
    return this
      .waitForElementVisible('@editProfileContactInformation')
      .click('@editProfileContactInformation')
  },

};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    editProfileHome: {
      selector: 'a[href="/portal/portal.html"]'
    },
    editProfileBiography: {
      selector: 'a[href="/profile/biography/index.html"]'
    },
    editProfileTeaching: {
      selector: 'a[href="/profile/teaching/index.html"]'
    },
    editProfileResearch: {
      selector: 'a[href="/profile/research/index.html"]'
    },
    editProfilePublications: {
      selector: 'a[href="/profile/bibliography/index.html"]'
    },
    editProfilePresentations: {
      selector: 'a[href="/profile/presentations/index.html"]'
    },
    editProfileFacultyPractice: {
      selector: 'a[href="/profile/practice/index.html"]'
    },
    editProfileGraduateStudents: {
      selector: 'a[href="/profile/gradstudents/index.html"]'
    },
    editProfileEntrepreneurialExperience: {
      selector: 'a[href="/profile/entrepreneurial/index.html"]'
    },
    editProfileService: {
      selector: 'a[href="/profile/philanthropy/index.html"]'
    },
    editProfileContactInformation: {
      selector: 'a[href="/profile/contact/index.html"]'
    },
  }
};
