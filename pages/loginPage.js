const pageCommands = {
  login(email, pass) {
    return this
      .waitForElementVisible('@emailInput')
      .setValue('@emailInput', email)
      .setValue('@passInput', pass)
      .waitForElementVisible('@loginButton')
      .click('@loginButton')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  // url: 'http://dev.resoapps.com',
  commands: [pageCommands],
  elements: {
    emailInput: {
      selector: 'input[name="user"]'
    },
    passInput: {
      selector: 'input[name="password"]'
    },
    loginButton: {
      selector: 'input[type=submit]'
    }
  }
};
