const pageCommands = {
  clickSiteInformation(){
    return this
      .waitForElementVisible('@siteInformationFirstLabel')
      .click('@siteInformationFirstLabel')
  },
  fillSiteInformation() {
    return this
      .waitForElementVisible('@name')
      .clearValue('@name')
      .setValue('@name',"Name")
      .clearValue('@phone')
      .setValue('@phone',"061-061-0610")
      .clearValue('@addressLineOne')
      .setValue('@addressLineOne',"Street name")
      .clearValue('@addressLineTwo')
      .setValue('@addressLineTwo',"Street name second")
      .clearValue('@addressLineThree')
      .setValue('@addressLineThree',"Street name third")
      .clearValue('@city')
      .setValue('@city',"Citycity")
      .clearValue('@state')
      .setValue('@state',"BH")
      .clearValue('@zip')
      .setValue('@zip',"71000")
      .clearValue('@information')
      .setValue('@information',"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam ac sem id dapibus. Donec ut vehicula leo. Nulla auctor mi velit, non suscipit justo semper at. Nunc dolor tellus, varius at urna nec, gravida luctus felis. Donec neque ex, gravida eu lacus in, eleifend varius purus. Aliquam consequat, quam eu pharetra fringilla, justo nisl lacinia risus, ac lobortis purus tortor ac enim. Mauris sit amet laoreet arcu. Mauris venenatis egestas imperdiet. In semper pellentesque elit et scelerisque. Nulla euismod sapien fermentum odio lacinia interdum. Morbi rhoncus iaculis consequat. Fusce et fermentum augue, sit amet aliquam dui. Integer placerat sapien in felis convallis, a feugiat dui maximus. Maecenas a condimentum dui, eget fermentum velit. Fusce ante metus, varius ac justo id, aliquam mattis ex. Cras volutpat velit diam.")
      .clearValue('@physicians')
      .setValue('@physicians',"0")
      .click('@siteApplication')
      .click('@studentID')
      .click('@patientPopulation')
      .click('@bilingualStudent')
      .click('@nutritionist')
      .click('@NP')

  },
  checkIsSiteInformationOpen() {
    return this
      .waitForElementVisible('@appSiteInformationLabel')
  }
};

module.exports = {
  url: 'http://dev.resoapps.com/appointmentcenter',
  commands: [pageCommands],
  elements: {
    siteInformationFirstLabel: {
      selector: "a[href='site_information.html']"
    },
    appSiteInformationLabel: {
      selector: "//h1[text()='Site Information']",
      locateStrategy: 'xpath'
    },
    name: {
      selector: '#name'
    },
    phone: {
      selector: 'input[name="contact.phone"]'
    },
    addressLineOne: {
      selector: 'input[name="address.line1"]'
    },
    addressLineTwo: {
      selector: 'input[name="address.line2"]'
    },
    addressLineThree: {
      selector: 'input[name="address.line3"]'
    },
    city: {
      selector: 'input[name="address.city"]'
    },
    state: {
      selector: 'input[name="address.state"]'
    },
    zip: {
      selector: 'input[name="address.zip"]'
    },
    information: {
      selector: '#affiliation'
    },
    physicians: {
      selector: '#physicians'
    },
    siteApplication: {
      selector: 'input[name="studentInformation.application"]'
    },
    studentID: {
      selector: 'input[name="studentInformation.studentId"]'
    },
    patientPopulation: {
      selector: '#underservedPatientPopulation2'
    },
    bilingualStudent: {
      selector: '#bilingualStudent1'
    },
    nutritionist: {
      selector: 'input[name="studentDisciplines.nutritionist"]'
    },
    NP: {
      selector: 'input[name="studentDisciplines.na"]'
    }

  
  }
};
