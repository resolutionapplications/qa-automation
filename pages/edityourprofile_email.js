const pageCommands = {
  editEmail() {
    return this
      .waitForElementVisible('@emailEditButton')
      .click('@emailEditButton')
  },
  editEmailWhileNoEmail() {
    return this
      .waitForElementVisible('@emailEditButtonWhileNoEmail')
      .click('@emailEditButtonWhileNoEmail')
  },
  editEmailInstitutional() {
    return this
      .waitForElementVisible('@emailEditInstitutionalRb')
      .click('@emailEditInstitutionalRb')
  },
  editEmailPreferred() {
    return this
      .waitForElementVisible('@emailEditPreferredRb')
      .click('@emailEditPreferredRb')
      .clearValue('@emailEditPreferred')
      .setValue('@emailEditPreferred',"myemail@email.com")
  },
  editEmailNoEmail() {
    return this
    .waitForElementVisible('@emailEditNoEmailRb')
    .click('@emailEditNoEmailRb')
  },
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    emailEditButton: {
      selector: 'a[href="/email/edit.html?"]'
    },
    emailEditButtonWhileNoEmail: {
      selector: 'a[href="/email/edit.html?"]'
    },
    emailEditNoEmailRb: {
      selector: '#emailDisplayType3'
    },
    emailEditInstitutionalRb: {
      selector: '#emailDisplayType1'
    },
    emailEditPreferredRb: {
      selector: '#emailDisplayType2'
    },
    emailEditPreferred: {
      selector: '#preferredEmailAddress'
    },
  }
};
