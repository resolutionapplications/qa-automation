const pageCommands = {
  ResCreative() {
    return this
      .waitForElementVisible('@creativeEditLabel')
      .click('@creativeEditLabel')
  },
  ResCreativeAdd() {
    return this
    .waitForElementVisible('@creativeAddButton')
    .click('@creativeAddButton')
  },
  ResCreativeAddContent() {
      return this
      .waitForElementVisible('@creativeType')
      .click( '@creativeType')
      .waitForElementVisible('@creativeTypePerformance')
      .click( '@creativeTypePerformance')
      .click( '@creativeRefereedCbox')
      .clearValue( '@creativeCitation')
      .setValue( '@creativeCitation', "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ")
      .click( '@creativeStatus')
      .waitForElementVisible('@creativeStatusCompleted')
      .click( '@creativeStatusCompleted')
      .clearValue( '@creativeDate')
      .setValue( '@creativeDate', "01/01/2007")
      .clearValue( '@creativeURL')
      .setValue( '@creativeURL', "http://www.url.com")
      .click('@creativePublishCbox')
      .click('@creativePublishCbox')
  },
  ResCreativeSort() {
    return this
      .waitForElementVisible('@creativeSortButton')
      .click('@creativeSortButton')
    },
  ResCreativeShowHide() {
    return this
      .waitForElementVisible('@creativeShowHideButton')
      .click('@creativeShowHideButton')
  },
 ResCreativeShowHideSelect() {
    return this
      .waitForElementVisible('@creativeShowHideBox')
      .click('@creativeShowHideBox')
    },
  removeOneCreative() {
    return this
      .waitForElementVisible('@removeOneCreativeButton')
      .click('@removeOneCreativeButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    creativeEditLabel: {
      selector: 'a[href="index.html#crtvwrks1"]'
    },
    creativeAddButton: {
      selector: 'a[href="/creativewrks/edit.html"]'
    },
    creativeSortButton: {
      selector: 'a[href="/creativewrks/sort.html"]'
    },
    creativeShowHideButton: {
      selector: 'a[href="/creativewrks/visible.html"]'
    },
    removeOneCreativeButton: {
      selector: 'img[src="/images/spotImages/xIcontrns.png"]'
    },
    //
    creativeType: {
      selector: '#type'
    },
    creativeTypePerformance: {
      selector: "//option[text()='Performance']",
      locateStrategy: 'xpath'
    },
    creativeDate: {
      selector: '#date'
    },
    creativeRefereedCbox: {
      selector: '#refereed1'
    },
    creativeCitation: {
      selector: '#citation'
    },
    creativeStatus: {
      selector: '#status'
    },
    creativeStatusCompleted: {
      selector: "//option[text()='Completed']",
      locateStrategy: 'xpath'
    },
    creativeURL: {
      selector: '#url'
    },
    creativePublishCbox: {
      selector: '#displayable1'
    },
    creativeShowHideBox: {
      selector: 'input[name="list[0].displayable"]'
    }
  }
};
