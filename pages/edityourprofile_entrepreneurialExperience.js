const pageCommands = {
  editEntreExperienceAdd() {
    return this
    .waitForElementVisible('@entreExpeirenceAddButton')
    .click('@entreExpeirenceAddButton')
  },
  editEntreExperienceAddContent() {
      return this
      .waitForElementVisible('@entreExperienceCompany')
      .setValue('@entreExperienceCompany',"My company")
      .clearValue('@entreExpeirenceRevenue')
      .setValue('@entreExpeirenceRevenue',"5000")
      .clearValue('@entreExpeirenceNumberOfEmployees')
      .setValue('@entreExpeirenceNumberOfEmployees',"5")
      .clearValue( '@entreExpeirenceHoursWorked')
      .setValue( '@entreExpeirenceHoursWorked', "15")
      .clearValue( '@entreExpeirenceStartDate')
      .setValue( '@entreExpeirenceStartDate', "01/01/2000")
      .clearValue( '@entreExpeirenceEndDate')
      .setValue( '@entreExpeirenceEndDate', "01/01/2020")
      .clearValue('@entreExpeirenceURL')
      .setValue('@entreExpeirenceURL',"http://www.url.com")
      .clearValue('@entreExpeirenceComments')
      .setValue('@entreExpeirenceComments',"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.")
      .click('@entreExpeirenceInternationalCbox')
      .waitForElementVisible('@entreExpeirenceInternationalCboxCountry')
      .clearValue( '@entreExpeirenceInternationalCboxCountry')
      .setValue( '@entreExpeirenceInternationalCboxCountry', "Country")
      .click('@entreExpeirenceInterdisciplinaryCbox')
      .click('@entreExpeirenceCommunityEngagementCbox')
      .click('@entreExpeirencePublishCbox')
      .click('@entreExpeirencePublishCbox')

  },
  editEntreExperienceSort() {
    return this
      .waitForElementVisible('@entreExpeirenceSortButton')
      .click('@entreExpeirenceSortButton')
    },
  editEntreExperienceShowHide() {
    return this
      .waitForElementVisible('@entreExpeirenceShowHideButton')
      .click('@entreExpeirenceShowHideButton')
  },
  editEntreExperienceShowHideSelect() {
    return this
      .waitForElementVisible('@entreExpeirenceShowHideBox')
      .click('@entreExpeirenceShowHideBox')
    },
  removeOneEntreExpeirence() {
    return this
      .waitForElementVisible('@removeOneEntreExpeirenceButton')
      .click('@removeOneEntreExpeirenceButton')
    }
};
module.exports = {
  url: 'http://dev.resoapps.com/profile/philanthropy/index.html',
  commands: [pageCommands],
  elements: {
    entreExpeirenceEditLabel: {
      selector:'a[href="/profile/entrepreneurial/index.html"]'
    },
   entreExpeirenceAddButton: {
      selector: 'a[href="/entrepreneurial/edit.html"]'
    },
    entreExpeirenceSortButton: {
      selector: 'a[href="/entrepreneurial/sort.html"]'
    },
    entreExpeirenceShowHideButton: {
      selector: 'a[href="/entrepreneurial/visible.html"]'
    },
    //
    entreExperienceCompany: {
      selector: '#companyName'
    },
    entreExpeirenceRevenue: {
      selector: "#revenue"
    },
    entreExpeirenceNumberOfEmployees: {
      selector: '#numberOfEmployees'
    },
   entreExpeirenceHoursWorked: {
      selector: "#hoursWorked"
    },
    entreExpeirenceStartDate: {
      selector: '#startDate'
    },
    entreExpeirenceEndDate: {
      selector: '#endDate'
    },
    entreExpeirenceURL: {
      selector: '#url'
    },
    //
    entreExpeirenceComments: {
      selector: '#comments'
    },
    entreExpeirenceInternationalCbox: {
      selector: '#checked_0'
    },
    entreExpeirenceInternationalCboxCountry: {
      selector: '#value_0'
    },
    entreExpeirenceInterdisciplinaryCbox: {
      selector: '#checked_1'
    },
    entreExpeirenceCommunityEngagementCbox: {
      selector: '#checked_2'
    },
    entreExpeirencePublishCbox: {
      selector: '#displayable1'
    },
    entreExpeirenceShowHideBox: {
      selector: 'input[name="list[0].displayed"]'
    },
   removeOneEntreExpeirenceButton: {
      selector:'img[src="/images/spotImages/xIcontrns.png"]'
    }


  }
};
