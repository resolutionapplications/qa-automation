var config = require('../tests.config');
module.exports = {
  tags: ['applicationtracking'],
    'Log in': (client) => {
      client.maximizeWindow();
      const loginPage = client.page.applicationtracking_login();
        loginPage
            .navigate()
            .login(config.username, config.password);
        client.pause(2000);
    },
    'Click on app': (client) => {
      const landingPage = client.page.applicationtracking_admin_landing();
      const personalData = client.page.applicationtracking_admin_personal_data();
      landingPage.clickApp();
      client.pause(2000);
      personalData.checkIsAppOpen();
    },
     'Personal data': (client) => {
       const personalData = client.page.applicationtracking_admin_personal_data();
       personalData.clickPersonalData();
       personalData.fillPersonalData();
       client.pause(5000);  
     },
     'New personal information': (client) => {
       const newPersonalInfo = client.page.applicationtracking_admin_new_personal_information();
       const demographicInfo = client.page.applicationtracking_admin_demographic();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();       
       newPersonalInfo.clickNewPersonalInfo();
       newPersonalInfo.fillNewPersonalInfo();
       client.pause(5000);
       saveContinue.saveAndContinue();
       client.pause(2000);
       demographicInfo.checkIsDemographicOpen();
     },
     'Demographic information': (client) => {
       const demographicInfo = client.page.applicationtracking_admin_demographic();
       const educationHistory = client.page.applicationtracking_admin_education_history();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       demographicInfo.clickDemographicInfo();
       demographicInfo.fillDemographicInfo();
       client.pause(2000);
       saveContinue.saveAndContinue();
       client.pause(5000);
       educationHistory.checkIsEducationHistoryOpen();
     },
    'Education history': (client) => {
      const educationHistory = client.page.applicationtracking_admin_education_history();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const educationHistoryTranscript = client.page.applicationtracking_admin_education_history_transcript();
      educationHistory.clickEducationHistory();
      educationHistory.plusEducationHistory();
      client.pause(2000);
      educationHistory.fillEducationHistory();
      client.pause(3000);
      educationHistory.cancelEducationHistory();
      educationHistory.plusEducationHistory();
      educationHistory.fillEducationHistory();
      client.pause(2000);
      educationHistory.saveEducationHistory();
      educationHistory.plusEducationHistory();
      educationHistory.fillEducationHistory();
      client.pause(2000);
      educationHistory.saveAndAddEducationHistory();
      educationHistory.fillEducationHistory();
      client.pause(2000);
      educationHistory.saveEducationHistory();
      for (var i = 0; i <3; i++) {
        client.elements('css selector', educationHistory.elements.removeOneEducationHistoryButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            educationHistory.removeOneEducationHistory();
            client.pause(2000).acceptAlert();
          }
        });
      }
      saveContinue.saveAndContinue();
      client.pause(5000);
      educationHistoryTranscript.checkIsEducationHistoryOpen();
    },
    'Education history transcript': (client) => {
      const educationHistoryTranscript = client.page.applicationtracking_admin_education_history_transcript();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const save = client.page.applicationtracking_admin_save();
      const pastAcademic = client.page.applicationtracking_admin_past_academic();
      educationHistoryTranscript.clickEducationHistory();
      educationHistoryTranscript.plusEducationHistory();
      client.pause(2000);
      educationHistoryTranscript.fillEducationHistory();
      client.pause(3000);
      educationHistoryTranscript.cancelEducationHistory();
      educationHistoryTranscript.plusEducationHistory();
      educationHistoryTranscript.fillEducationHistory();
      client.pause(2000);
      save.save();
      educationHistoryTranscript.plusEducationHistory();
      educationHistoryTranscript.fillEducationHistory();
      client.pause(2000);
      educationHistoryTranscript.saveAndAddEducationHistory();
      educationHistoryTranscript.fillEducationHistory();
      client.pause(2000);
      save.save();
      for (var i = 0; i <3; i++) {
        client.elements('css selector', educationHistoryTranscript.elements.removeOneEducationHistoryButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            educationHistoryTranscript.removeOneEducationHistory();
            client.pause(2000).acceptAlert();
          }
        });
      }
      saveContinue.saveAndContinue();
      client.pause(5000);
      pastAcademic.checkIsPastAcademicOpen();
    },
     'Past academic': (client) => {
       const pastAcademic = client.page.applicationtracking_admin_past_academic();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const medicalLicences = client.page.applicationtracking_admin_medical_licenses();
       pastAcademic.clickPastAcademic();
       pastAcademic.plusPastAcademic();
       pastAcademic.fillPastAcademic();
       client.pause(2000);
       pastAcademic.cancelPastAcademic();
       pastAcademic.plusPastAcademic();
       pastAcademic.fillPastAcademic();
       client.pause(2000);
       pastAcademic.saveAndAddPastAcademic();
       client.pause(2000);
       pastAcademic.fillPastAcademic();
       client.pause(2000);
       pastAcademic.savePastAcademic();
       for (var i = 0; i <2; i++) {
        client.elements('css selector', pastAcademic.elements.removeOnePastAcademicButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            pastAcademic.removeOnePastAcademic();
            client.pause(2000).acceptAlert();
          }
        });
      }
       saveContinue.saveAndContinue();
       client.pause(5000);
      medicalLicences.checkIsMedicalLicensesOpen();
     },
     'Medical licences': (client) => {
       const medicalLicences = client.page.applicationtracking_admin_medical_licenses();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const licencesCertificates=client.page.applicationtracking_admin_licences_and_certificates();
       medicalLicences.clickMedicalLicenses();
       medicalLicences.plusMedicalLicenses();
       medicalLicences.fillMedicalLicenses();
       client.pause(2000);
       medicalLicences.cancelMedicalLicenses();
       medicalLicences.plusMedicalLicenses();
       medicalLicences.fillMedicalLicenses();
       client.pause(2000);
       medicalLicences.saveAndAddMedicalLicenses();
       medicalLicences.fillMedicalLicenses();
       client.pause(2000);
       medicalLicences.saveMedicalLicenses();
       for (var i = 0; i <2; i++) {
        client.elements('css selector', medicalLicences.elements.removeOneMedicalLicensesButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            medicalLicences.removeOneMedicalLicenses();
            client.pause(2000).acceptAlert();
          }
        });
      }
      saveContinue.saveAndContinue();
      client.pause(5000);
      licencesCertificates.checkIsLicencesCertificatesOpen();
     },
    'Licences and Certificates': (client) => {
      const licencesCertificates=client.page.applicationtracking_admin_licences_and_certificates();
      const save = client.page.applicationtracking_admin_save();
      licencesCertificates.clickLicencesCertificates();
      licencesCertificates.plusLicencesCertificates();
      licencesCertificates.fillLicencesCertificates();
      licencesCertificates.cancelLicencesCertificates();
      client.pause(2000);
      licencesCertificates.plusLicencesCertificates();
      licencesCertificates.fillLicencesCertificates();
      licencesCertificates.saveAndAddLicencesCertificates();
      client.pause(2000);
      licencesCertificates.fillLicencesCertificates();
      licencesCertificates.saveLicencesCertificates();
      client.pause(2000);
      for (var i = 0; i <2; i++) {
        client.elements('css selector', licencesCertificates.elements.removeOneLicencesCertificatesButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            licencesCertificates.removeOneLicencesCertificates();
            client.pause(2000).acceptAlert();
          }
        });
      }
      save.save();
      client.pause(5000);
      
    },
     'Employers': (client) => {
       const employ = client.page.applicationtracking_admin_employers();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const boardCertifications = client.page.applicationtracking_admin_board_certification();
       employ.clickEmployers();
       employ.plusEmployers();
       employ.fillEmployers();
       client.pause(2000);
       employ.cancelEmployers();
       employ.plusEmployers();
       employ.fillEmployers();
       client.pause(2000);
       employ.saveEmployers();
       employ.removeOneEmployer();
       client.pause(2000).acceptAlert();
       saveContinue.saveAndContinue();
       client.pause(5000);
       boardCertifications.checkIsBoardCertificationsOpen();
     },
     'Board certifications': (client) => {
       const boardCertifications = client.page.applicationtracking_admin_board_certification();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const biosk = client.page.applicationtracking_admin_biosketch();
       boardCertifications.clickBoardCertifications();
       boardCertifications.plusBoardCertifications();
       boardCertifications.fillBoardCertifications();
       client.pause(2000);
       boardCertifications.cancelBoardCertifications();
       boardCertifications.plusBoardCertifications();
       boardCertifications.fillBoardCertifications();
       client.pause(2000);
       boardCertifications.saveAndAddBoardCertifications();
       boardCertifications.fillBoardCertifications();
       client.pause(2000);
       boardCertifications.saveBoardCertifications();
       for (var i = 0; i <2; i++) {
        client.elements('css selector', boardCertifications.elements.removeOneBoardCertificationsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            boardCertifications.removeOneBoardCertifications();
            client.pause(2000).acceptAlert();
          }
        });
      }
       saveContinue.saveAndContinue();
       client.pause(5000);
       biosk.checkIsBiosketchOpen();
     },
     'Biosketch': (client) => {
       const biosk = client.page.applicationtracking_admin_biosketch();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const hospitalPrivileges = client.page.applicationtracking_admin_hospital_privileges();
       biosk.clickBiosketch();
       biosk.fillBiosketch();
       client.pause(2000);
       saveContinue.saveAndContinue();
       client.pause(5000);
       hospitalPrivileges.checkIsHospitalPrivilegesOpen();
     },
     'Hospital privileges': (client) => {
       const hospitalPrivileges = client.page.applicationtracking_admin_hospital_privileges();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const leadershipRoles = client.page.applicationtracking_admin_leadership();
       hospitalPrivileges.clickHospitalPrivileges();
       hospitalPrivileges.plusHospitalPrivileges();
       hospitalPrivileges.fillHospitalPrivileges();
       client.pause(2000);
       hospitalPrivileges.cancelHospitalPrivileges();
       hospitalPrivileges.plusHospitalPrivileges();
       hospitalPrivileges.fillHospitalPrivileges();
       client.pause(2000);
       hospitalPrivileges.saveHospitalPrivileges();
       hospitalPrivileges.plusHospitalPrivileges();
       hospitalPrivileges.fillHospitalPrivileges();
       client.pause(2000);
       hospitalPrivileges.saveAndAddHospitalPrivileges();
       hospitalPrivileges.fillHospitalPrivileges();
       client.pause(2000);
       hospitalPrivileges.saveHospitalPrivileges();
       for (var i = 0; i <3; i++) {
        client.elements('css selector', hospitalPrivileges.elements.removeOneHospitalPrivilegesButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            hospitalPrivileges.removeOneHospitalPrivileges();
            client.pause(2000).acceptAlert();
          }
        });
      }
       saveContinue.saveAndContinue();
       client.pause(5000);
       leadershipRoles.checkIsLeadershipRolesOpen();
     },
      'Leadership roles': (client) => {
       const leadershipRoles = client.page.applicationtracking_admin_leadership();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const areasOfInterest = client.page.applicationtracking_admin_areas_of_interest();
       leadershipRoles.clickLeadershipRoles();
       leadershipRoles.plusLeadershipRoles();
       leadershipRoles.fillLeadershipRoles();
       client.pause(2000);
       leadershipRoles.cancelLeadershipRoles();
       leadershipRoles.plusLeadershipRoles();
       leadershipRoles.fillLeadershipRoles();
       client.pause(2000);
       leadershipRoles.saveLeadershipRoles();
       leadershipRoles.plusLeadershipRoles();
       leadershipRoles.fillLeadershipRoles();
       client.pause(2000);
       leadershipRoles.saveAndAddLeadershipRoles();
       leadershipRoles.fillLeadershipRoles();
       client.pause(2000);
       leadershipRoles.saveLeadershipRoles();
       for (var i = 0; i <3; i++) {
        client.elements('css selector', leadershipRoles.elements.removeOneLeadershiRolesButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            leadershipRoles.removeOneLeadershiRoles();
            client.pause(2000).acceptAlert();
          }
        });
      }
       saveContinue.saveAndContinue();
       client.pause(5000);
       areasOfInterest.checkIsAreasOfInterestOpen();
     },
     'Areas of interest': (client) => {
       const areasOfInterest = client.page.applicationtracking_admin_areas_of_interest();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const wMedService = client.page.applicationtracking_admin_wmed_service();
       areasOfInterest.clickAreasOfInterest();
       areasOfInterest.plusAreasOfInterest();
       areasOfInterest.fillAreasOfInterest();
       client.pause(2000);
       areasOfInterest.cancelAreasOfInterest();
       areasOfInterest.plusAreasOfInterest();
       areasOfInterest.fillAreasOfInterest();
       client.pause(2000);
       areasOfInterest.saveAreasOfInterest();
       areasOfInterest.plusAreasOfInterest();
       areasOfInterest.fillAreasOfInterest();
       client.pause(2000);
       areasOfInterest.saveAndAddAreasOfInterest();
       areasOfInterest.fillAreasOfInterest();
       client.pause(2000);
       areasOfInterest.saveAreasOfInterest();
          for (var i = 0; i <3; i++) {
        client.elements('css selector', areasOfInterest.elements.removeOneAreasOfInterestButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            areasOfInterest.removeOneAreasOfInterest();
            client.pause(2000).acceptAlert();
          }
        });
      }
       saveContinue.saveAndContinue();
       client.pause(5000);
       wMedService.checkIsWMedServicesOpen();
     },
     'WMed Service Interests': (client) => {
       const wMedService = client.page.applicationtracking_admin_wmed_service();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const memberships = client.page.applicationtracking_admin_memberships();
       wMedService.clickWMedServices();
       wMedService.fillWMedServices();
       saveContinue.saveAndContinue();
       client.pause(5000);
       memberships.checkIsMembershipsOpen();
     },
     'Memberships': (client) => {
       const memberships = client.page.applicationtracking_admin_memberships();
       const saveContinue = client.page.applicationtracking_admin_save_and_continue();
       const signifAcc = client.page.applicationtracking_admin_significant_accomplishments();
       memberships.clickMemberships();
       memberships.plusMemberships();
       memberships.fillMemberships();
       client.pause(2000);
       memberships.cancelMemberships();
       memberships.plusMemberships();
       memberships.fillMemberships();
       client.pause(2000);
       memberships.saveAndAddMemberships();
       memberships.fillMemberships();
       client.pause(2000);
       memberships.saveMemberships();
       for (var i = 0; i <2; i++) {
        client.elements('css selector', memberships.elements.removeOneMembershipsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            memberships.removeOneMemberships();
            client.pause(2000).acceptAlert();
          }
        });
      }
       saveContinue.saveAndContinue();
       client.pause(5000);
       signifAcc.checkIsSignificantAccOpen();
     },
    'Significant accomplishments': (client) => {
      const signifAcc = client.page.applicationtracking_admin_significant_accomplishments();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const lettersOfSupport = client.page.applicationtracking_admin_letters_of_support();
      signifAcc.clickSignificantAcc();
      signifAcc.plusSignificantAcc();
      signifAcc.fillSignificantAcc();
      client.pause(2000);
      signifAcc.cancelSignificantAcc();
      signifAcc.plusSignificantAcc();
      signifAcc.fillSignificantAcc();
      client.pause(2000);
      signifAcc.saveSignificantAcc();
      signifAcc.plusSignificantAcc();
      signifAcc.fillSignificantAcc();
      client.pause(2000);
      signifAcc.saveAndAddSignificantAcc();
      signifAcc.fillSignificantAcc();
      client.pause(2000);
      signifAcc.saveSignificantAcc();
      for (var i = 0; i <3; i++) {
        client.elements('css selector', signifAcc.elements.removeOneSignificantAccButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            signifAcc.removeOneSignificantAcc();
            client.pause(2000).acceptAlert();
          }
        });
      }
      saveContinue.saveAndContinue();
      client.pause(5000);
      lettersOfSupport.checkIsLetterOfSupportOpen();
    },
    'Letters of Support': (client) => {
      const lettersOfSupport = client.page.applicationtracking_admin_letters_of_support();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const candidateStatement = client.page.applicationtracking_admin_candidate_satementOfInterest();
      lettersOfSupport.clickLettersOfSupport();
      lettersOfSupport.fillLettersOfSupportReviewer1();
      client.pause(2000);
      lettersOfSupport.fillLettersOfSupportReviewer2();
      client.pause(2000);
      lettersOfSupport.fillLettersOfSupportReviewer3();
      client.pause(2000);
      saveContinue.saveAndContinue();
      client.pause(5000);
      candidateStatement.checkIsCandidateStatementOpen();
    },
    'Candidate statement of interest': (client) => {
      const candidateStatement = client.page.applicationtracking_admin_candidate_satementOfInterest();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const teachingActivities = client.page.applicationtracking_admin_teaching_activities();
      candidateStatement.clickCandidateStatement();
      candidateStatement.fillCandidateStatement();
      saveContinue.saveAndContinue();
      client.pause(5000);
      teachingActivities.checkIsTeachingOpen();
    },
    'Teaching Activities': (client) => {
      const teachingActivities = client.page.applicationtracking_admin_teaching_activities();
      const save = client.page.applicationtracking_admin_save();
      teachingActivities.clickTeaching();
      teachingActivities.plusTeaching();
      teachingActivities.fillWithCancelTeaching();
      client.pause(2000);
      teachingActivities.plusTeaching();
      teachingActivities.fillWithNextCancelTeaching();
      client.pause(2000);
      teachingActivities.plusTeaching();
      teachingActivities.fillWithNextSaveTeaching();
      client.pause(2000);
      // teachingActivities.removeOneTeaching();
      // client.pause(2000).acceptAlert();
      save.save();
      client.pause(5000);
      
    },
    'Speakers Bureau': (client) => {
      const speakersBureau = client.page.applicationtracking_admin_speakers_bureau();
      const save = client.page.applicationtracking_admin_save();
      speakersBureau.clickSpeakersBureau();
      speakersBureau.plusSpeakersBureau();
      speakersBureau.fillSpeakersBureau();
      speakersBureau.saveSpeakersBureau();
      client.pause(2000);
      speakersBureau.plusSpeakersBureau();
      speakersBureau.fillSpeakersBureau();
      speakersBureau.cancelSpeakersBureau();
      client.pause(2000);
      speakersBureau.fillSpeakersBureauPart2();
      client.pause(2000);
      // speakersBureau.removeOneSpeakersBureau();
      // client.pause(2000).acceptAlert();
      // client.pause(2000);
      save.save();
      client.pause(5000);
      speakersBureau.nextSpeakersBureau();
      client.pause(2000);
    },
    'Required attachments': (client) => {
      const requiredAttachments = client.page.applicationtracking_admin_required_attachments();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const attestSignature = client.page.applicationtracking_admin_attestation_signature();
      requiredAttachments.clickAttachments();
      client.pause(2000);
      for (var i = 0; i <6; i++) {
        client.elements('css selector', requiredAttachments.elements.removeOneAttachmentsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            requiredAttachments.removeOneAttachments();
            client.pause(2000).acceptAlert();
          }
        });
      }
      client.pause(2000);
      client.setValue('input#cvFile', __dirname + '\\test.docx')
      client.pause(2000);
      client.setValue('input[name="teachingEvalFiles"]', __dirname + '\\test.docx')
      client.pause(2000);
      requiredAttachments.ClickAddLetter();
      client.pause(2000);
      client.setValue('input[name="letterFiles"]', __dirname + '\\test.docx')
      client.pause(2000);
      requiredAttachments.ClickAddAdditionalDocument();
      client.pause(2000);
      client.setValue('input[name="additionalDocDescriptions"]','My additional document')
      client.pause(2000);
      client.setValue('input[name="additionalDocFiles"]', __dirname + '\\test.docx')
      client.pause(2000);
      client.setValue('input#dynamicDocuments_9142134', __dirname + '\\test.docx')
      client.pause(2000);
      client.setValue('input#dynamicDocuments_9142135', __dirname + '\\test.docx')
      client.pause(2000);
      
      // client.setValue('#candidatePhoto', __dirname + '\\random.png')
      // client.pause(2000);

      saveContinue.saveAndContinue();
      client.pause(5000);
    },
    'Attestation and signature': (client) => {
      const attestSignature = client.page.applicationtracking_admin_attestation_signature();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const transcriptRequired = client.page.applicationtracking_admin_transcripts_required();
      attestSignature.clickAttestSignature();
      attestSignature.fillAttestSignature();
      client.pause(2000);
      saveContinue.saveAndContinue();
      client.pause(5000);
      transcriptRequired.checkIsTranscriptOpen();
    },
    
    'Transcripts Required': (client) => {
      const transcriptRequired = client.page.applicationtracking_admin_transcripts_required();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const scholarshipInfo = client.page.applicationtracking_admin_scholarship_info();     
      transcriptRequired.fillTranscriptRequired();
      client.pause(2000);
      saveContinue.saveAndContinue();
      client.pause(5000);
      scholarshipInfo.checkIsScholarshipInfoOpen();
    },
    'Scholarship Info': (client) => {
      const scholarshipInfo = client.page.applicationtracking_admin_scholarship_info();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const proffesionalExperience=client.page.applicationtracking_admin_proffesional_experience();
      scholarshipInfo.fillScholarshipInfo();
      client.pause(2000);
      saveContinue.saveAndContinue();
      client.pause(5000);
      proffesionalExperience.checkIsProffesionalExperienceOpen();
    },
    'Professional Experience ': (client) => {
      const proffesionalExperience=client.page.applicationtracking_admin_proffesional_experience();
      const save = client.page.applicationtracking_admin_save();
      proffesionalExperience.clickProffesionalExperience();
      proffesionalExperience.plusProffesionalExperience();
      proffesionalExperience.fillProffesionalExperience();
      proffesionalExperience.cancelProffesionalExperience();
      client.pause(2000);
      proffesionalExperience.plusProffesionalExperience();
      proffesionalExperience.fillProffesionalExperience();
      proffesionalExperience.saveAndAddProffesionalExperience();
      client.pause(2000);
      proffesionalExperience.fillProffesionalExperience();
      proffesionalExperience.saveProffesionalExperience();
      client.pause(2000);
      for (var i = 0; i <2; i++) {
        client.elements('css selector', proffesionalExperience.elements.removeOneProffesionalExperienceButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            proffesionalExperience.removeOneProffesionalExperience();
            client.pause(2000).acceptAlert();
          }
        });
      }
      save.save();
      client.pause(5000);
      
    },
    // 'Qualify Faculty ': (client) => {
    //   const qualifyFaculty=client.page.applicationtracking_admin_qualify_faculty();
    //   const save = client.page.applicationtracking_admin_save();
    //   qualifyFaculty.clickQualifyFaculty();
    //   qualifyFaculty.plusQualifyFaculty();
    //   qualifyFaculty.fillQualifyFaculty();
    //   qualifyFaculty.cancelQualifyFaculty();
    //   client.pause(2000);
    //   qualifyFaculty.plusQualifyFaculty();
    //   qualifyFaculty.fillQualifyFaculty();
    //   qualifyFaculty.saveAndAddQualifyFaculty();
    //   client.pause(2000);
    //   qualifyFaculty.fillQualifyFaculty();
    //   qualifyFaculty.saveQualifyFaculty();
    //   client.pause(2000);
    //     for (var i = 0; i <2; i++) {
    //     client.elements('css selector', qualifyFaculty.elements.removeOneQualifyFacultyButton.selector, (isPresent) => {
    //       if(isPresent.value.length > 0){
    //         qualifyFaculty.removeOneQualifyFaculty();
    //         client.pause(2000).acceptAlert();
    //       }
    //     });
    //   }
    //   save.save();
    //   client.pause(5000);
      
    // },
    // 'Title & Letters of Recommendation': (client) => {
    //   const titleLetters = client.page.applicationtracking_admin_title_letters();
    //   const save = client.page.applicationtracking_admin_save();
    //   titleLetters.clickTitleLetters();
    //   titleLetters.plusTitleLetters();
    //   titleLetters.fillTittleLetters();
    //   titleLetters.saveAndAddAnotherTitleLetters();
    //   client.pause(2000);
    //   titleLetters.fillTittleLettersPart2();
    //   titleLetters.saveTitleLetters();
    //   client.pause(2000);
    //   titleLetters.plusTitleLetters();
    //   titleLetters.fillTittleLetters();
    //   titleLetters.cancelTitleLetters();
    //   client.pause(2000);
    //   titleLetters.removeOneTitleLetters();
    //   client.pause(2000).acceptAlert();
    //   client.pause(2000);
    //   save.save();
    //   client.pause(5000);
    // },
    'Additional Information': (client) => {
      const additionalInfo = client.page.applicationtracking_admin_additional_info();
      const saveContinue = client.page.applicationtracking_admin_save_and_continue();
      const trackRank = client.page.applicationtracking_admin_track_rank();
      additionalInfo.clickAdditionalInfo();
      additionalInfo.fillAdditionalInfo();
      client.pause(2000);
      saveContinue.saveAndContinue();
      client.pause(5000);
      trackRank.checkIsTrackRankOpen();
    },
    'Track & Rank': (client) => {
      const trackRank = client.page.applicationtracking_admin_track_rank();
      const save = client.page.applicationtracking_admin_save();
      trackRank.clickTrackRank();
      trackRank.plusTrackRank();
      trackRank.fillTrackRank();
      trackRank.cancelTrackRank();
      client.pause(2000);
      trackRank.plusTrackRank();
      trackRank.fillTrackRank();
      trackRank.saveAndAddTrackRank();
      client.pause(2000);
      trackRank.fillTrackRank();
      trackRank.saveTrackRank();
      client.pause(2000);
      for (var i = 0; i <2; i++) {
        client.elements('css selector', trackRank.elements.removeOneTrackRankButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            trackRank.removeOneTrackRank();
            client.pause(2000).acceptAlert();
          }
        });
      }
      save.save();
      client.pause(5000);
    },
    'Publication': (client) => {
      const publication = client.page.applicationtracking_admin_publication();
      const save = client.page.applicationtracking_admin_save();
      publication.clickPublication();
      publication.plusPublication();
      publication.fillPublication();
      publication.savePublication();
      client.pause(2000);
      publication.plusPublication();
      publication.fillPublication();
      publication.cancelPublication();
      client.pause(2000);
      publication.removeOnePublication();
      client.pause(2000).acceptAlert();
      client.pause(2000);
      save.save();
      client.pause(5000);
    },
    'Review and submit': (client) => {
      const reviewSubmit = client.page.applicationtracking_admin_review_submit();
      reviewSubmit.clickReviewSubmit();
      client.pause(5000);
    },

    'End': (client) => {
        client.end();
    }
};
