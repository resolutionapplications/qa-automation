var config = require('../tests.config');
module.exports = {
  tags: ['edityourprofile'],
    'Faculty profile': (client) => {
      client.maximizeWindow();
      const facultyProfile = client.page.edityourprofile_faculty_profile();
        facultyProfile.navigate()
        facultyProfile.editFacultyProfile();
        client.pause(2000);
    },
    'Login page': (client) => {
      const loginToEditProfile = client.page.edityourprofile_login();
        loginToEditProfile.loginToEdit(config.username, config.password);
        client.pause(2000);
    },
    'Edit title': (client) => {
      const editTitle = client.page.edityourprofile_title();
      const save = client.page.edityourprofile_save_button();
      const cancel = client.page.edityourprofile_cancel_button();
      const close = client.page.edityourprofile_close_button();
        editTitle.editTitleOne();
        client.pause(2000);
        client.frame(0);
        client.pause(2000);
        editTitle.editTitleNoPhoto();
        editTitle.editTitleFill();
        client.pause(1000);
        save.editProfileSave();
        client.pause(2000);
        client.frame(null);
        client.pause(2000);
        editTitle.editTitleOne();
        client.pause(2000);
        client.frame(0);
        client.pause(2000);
        editTitle.editTitleWithPhoto();
        client.setValue('input[type="file"]', __dirname + '\\profileImage.jpg')
        client.pause(1000);
        save.editProfileSave();
        client.pause(2000);
        client.frame(null);
        client.pause(2000);
        editTitle.editTitleOne();
        client.pause(2000);
        client.frame(0);
        client.pause(2000);
        cancel.editProfileCancelButton();
        client.pause(2000);
        client.frame(null);
        client.pause(2000);
        editTitle.editTitleOne();
        client.pause(2000);
        close.editProfileClose();
        client.pause(2000);
    //  Same functionality within another two edit icons (pencils):
        editTitle.editTitleTwo();
        client.pause(2000);
        client.frame(0);
        client.pause(2000);
        cancel.editProfileCancelButton();
        client.pause(2000);
        client.frame(null);
        client.pause(2000);
        editTitle.editTitleThree();
        client.pause(2000);
        client.frame(0);
        client.pause(2000);
        cancel.editProfileCancelButton();
        client.pause(2000);
        client.frame(null);
        client.pause(2000);
    },
    'Edit CV': (client) => {
      const editCV = client.page.edityourprofile_cv();
      const save = client.page.edityourprofile_save_button();
      const cancel = client.page.edityourprofile_cancel_button();
      const close = client.page.edityourprofile_close_button();
      editCV.editCV();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      editCV.editCVNoCV();
      client.pause(1000);
      save.editProfileSave();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editCV.editCV();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      editCV.editCVWithOptionalPublicCV();
      client.setValue('input[type="file"]', __dirname + '\\AJH-Scrummaster.pdf')
      client.pause(1000);
      save.editProfileSave();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editCV.editCV();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      cancel.editProfileCancelButton();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editCV.editCV();
      client.pause(2000);
      close.editProfileClose();
      client.pause(2000);
    },
    'Edit URL': (client) => {
      const editURL = client.page.edityourprofile_url();
      const save = client.page.edityourprofile_save_button();
      const cancel = client.page.edityourprofile_cancel_button();
      const close = client.page.edityourprofile_close_button();
      editURL.editURL();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      editURL.editURLDeleteAlias();
      client.pause(1000);
      save.editProfileSave();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editURL.editURL();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      editURL.editURLAlias();
      client.pause(1000);
      save.editProfileSave();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editURL.editURL();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      cancel.editProfileCancelButton();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editURL.editURL();
      client.pause(2000);
      close.editProfileClose();
      client.pause(2000);
    },
    'Edit email': (client) => {
      const editEmail = client.page.edityourprofile_email();
      const save = client.page.edityourprofile_save_button();
      const cancel = client.page.edityourprofile_cancel_button();
      const close = client.page.edityourprofile_close_button();
      editEmail.editEmail();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      editEmail.editEmailNoEmail();
      client.pause(1000);
      save.editProfileSave();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editEmail.editEmailWhileNoEmail();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      editEmail.editEmailPreferred();
      client.pause(1000);
      save.editProfileSave();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editEmail.editEmail();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      editEmail.editEmailInstitutional();
      client.pause(1000);
      save.editProfileSave();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editEmail.editEmail();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      cancel.editProfileCancelButton();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editEmail.editEmail();
      client.pause(2000);
      close.editProfileClose();
      client.pause(2000);
    },
    'Edit Biography: Biography': (client) => {
      const editBio = client.page.edityourprofile_biography();
      const save = client.page.edityourprofile_save_button();
      const cancel = client.page.edityourprofile_cancel_button();
      const close = client.page.edityourprofile_close_button();
      const links = client.page.edityourprofile_links();
      links.editBiography();
      client.pause(2000);
      editBio.editBioBiography();
      client.pause(2000);
    // Deleting biography: all buttons tested
  // Editing new biography: all buttons tested
      editBio.editBioBiographyContentClick();
      client.pause(4000);
      client.frame(0);
      client.pause(4000);
      client.waitForElementVisible('#cke_content');
      client.moveToElement('#cke_content',50,50);
      client.mouseButtonClick('left')
      client.keys('Lorem ipsum dolor sit amet');
      client.pause(2000);
      save.editProfileSave();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editBio.editBioBiographyContentClick();
      client.pause(4000);
      client.frame(0);
      client.pause(4000);
      cancel.editProfileCancelButton();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
      editBio.editBioBiographyContentClick();
      close.editProfileClose();
      client.pause(2000);
      editBio.editBioBiographyDelete();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      cancel.editProfileCancelButton();
      client.pause(5000);
      client.frame(null);
      client.pause(2000);
      editBio.editBioBiographyDelete();
      client.pause(2000);
      close.editProfileClose();
      client.pause(2000);
      editBio.editBioBiographyDelete();
      client.pause(2000);
      client.frame(0);
      client.pause(2000);
      save.editProfileSave();
      client.pause(2000);
      client.frame(null);
      client.pause(2000);
    },
'Edit Biography: Education': (client) => {
  const editEdu = client.page.edityourprofile_education();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editBiography();
  client.pause(2000);
  editEdu.editBioEducation();
  client.pause(2000);
// Adding education: all buttons not tested
  editEdu.editBioEducationAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editEdu.editBioEducationAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editEdu.editBioEducationAddContentFirstPart();
  client.pause(2000);
 //Delete next rows after bug is resolved!!!!
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editEdu.editBioEducationAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editEdu.editBioEducationAddContentFirstPart();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editEdu.editBioEducationAddContentFirstPart();
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editEdu.editBioEducationShowHide();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editEdu.editBioEducationShowHideSelect();
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editEdu.removeOneEducation();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editEdu.removeOneEducation();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  for (var i = 0; i <2; i++) {
        client.elements('css selector', editEdu.elements.removeOneEducationButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editEdu.removeOneEducation();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
// Sorting education
// Showing/hiding education
},
'Edit Biography: In the media': (client) => {
  const editMedia = client.page.edityourprofile_inthemedia();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editBiography();
  client.pause(2000);
  editMedia.editBioMedia();
  client.pause(2000);
// Adding In the media: all buttons tested
  editMedia.editBioMediaAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  editMedia.editBioMediaAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editMedia.editBioMediaAddContent();
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editMedia.editBioMediaAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editMedia.editBioMediaAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editMedia.editBioMediaShowHide();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editMedia.editBioMediaShowHideSelect();
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editMedia.removeOneMedia();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editMedia.removeOneMedia();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  for (var i = 0; i <2; i++) {
        client.elements('css selector', editMedia.elements.removeOneMediaButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editMedia.removeOneMedia();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting In the media
// Showing/hiding In the media
},
'Edit Biography: Language': (client) => {
  const editLang = client.page.edityourprofile_language();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editBiography();
  client.pause(2000);
  editLang.editBioLanguage();
  client.pause(2000);
// Adding Language: all buttons tested
  editLang.editBioLanguageAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  editLang.editBioLanguageAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editLang.editBioLanguageAddContent(client.Keys.ENTER);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editLang.editBioLanguageAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editLang.editBioLanguageShowHide();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editLang.editBioLanguageShowHideSelect();
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editLang.editBioLanguageDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editLang.editBioLanguageDelete();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editLang.editBioLanguageDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
// Sorting Language
// Showing/hiding Language
},
'Edit Biography: Honours & Awards': (client) => {
  const editHonours = client.page.edityourprofile_honoursAwards();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editBiography();
  client.pause(2000);
  editHonours.editBioHonours();
  client.pause(2000);
// Adding Honours & Awards: all buttons tested
  editHonours.editBioHonoursAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  editHonours.editBioHonoursAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editHonours.editBioHonoursAddContent();
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editHonours.editBioHonoursAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editHonours.editBioHonoursAdd();
  client.pause(4000);
  close.editProfileClose();
  client.pause(2000);
  editHonours.editBioHonoursShowHide();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editHonours.editBioHonoursShowHideSelect();
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editHonours.removeOneHonours();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editHonours.removeOneHonours();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  for (var i = 0; i <2; i++) {
        client.elements('css selector', editHonours.elements.removeOneHonoursButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editHonours.removeOneHonours();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting Honours & Awards
// Showing/hiding Honours & Awards
},
'Edit Biography: Geographical Regions of Interest': (client) => {
  const editGeo = client.page.edityourprofile_geographical();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editBiography();
  client.pause(2000);
  editGeo.editBioGeographical();
  client.pause(2000);
// Adding Geographical: all buttons tested
  editGeo.editBioGeographicalAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  editGeo.editBioGeographicalAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editGeo.editBioGeographicalAddContentFirst(client.Keys.DOWN_ARROW, client.Keys.ENTER);
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editGeo.editBioGeographicalAddContentSecond(client.Keys.DOWN_ARROW, client.Keys.ENTER);
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editGeo.editBioGeographicalAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editGeo.editBioGeographicalShowHide();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editGeo.editBioGeographicalShowHideSelect();
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.frame(null);
  client.pause(2000);
  editGeo.removeOneGeographical();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editGeo.removeOneGeographical();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  for (var i = 0; i <2; i++) {
        client.elements('css selector', editGeo.elements.removeOneGeographicalButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editGeo.removeOneGeographical();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting Geographical
// Showing/hiding Geographical
},
'Edit Biography: Other Biography': (client) => {
  const editOther = client.page.edityourprofile_otherBiography();
  const save = client.page.edityourprofile_save_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editBiography();
  client.pause(2000);
  editOther.editBioOther();
  client.pause(2000);
// Editing new Other Biography: all buttons tested
  // Save button
  editOther.editBioOtherAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  editOther.editBioOtherAddContent();
  client.waitForElementVisible('#cke_paragraph');
  client.moveToElement('#cke_paragraph',50,100);
  client.mouseButtonClick('left')
  client.keys('Other Profile Data for Biography: Lorem ipsum dolor sit amet.');
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  // Cancel and Close buttons
  editOther.editBioOtherAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editOther.editBioOtherAdd();
  close.editProfileClose();
  client.pause(2000);
  editOther.editBioOtherDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editOther.editBioOtherDelete();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editOther.editBioOtherDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
// Delete button UNFINISHED!!!
},
'Edit Biography: Boards Specialty': (client) => {
  const editBoards = client.page.edityourprofile_boardsSpecialty();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editBiography();
  client.pause(2000);
  editBoards.editBioBoards();
  client.pause(2000);
// Adding Boards Specialty: all buttons tested
//   Cancel button : UNCOMMENT AFTER THE BUGS ARE SOLVED:
   editBoards.editBioBoardsAdd();
   client.pause(2000);
   client.frame(0);
   client.pause(2000);
   cancel.editProfileCancelButton();
   client.pause(5000);
   client.frame(null);
   client.pause(2000);
  editBoards.editBioBoardsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editBoards.editBioBoardsAddContentFirstPart();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editBoards.editBioBoardsAddContentFirstPart();
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editBoards.editBioBoardsAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editBoards.editBioBoardsShowHide();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editBoards.editBioBoardsShowHideSelect();
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.frame(null);
  client.pause(2000);
  editBoards.removeOneBoards();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editBoards.removeOneBoards();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  for (var i = 0; i <2; i++) {
        client.elements('css selector', editBoards.elements.removeOneBoardsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editBoards.removeOneBoards();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting Boards Specialty
// Showing/hiding Boards Specialty
},
// 'Edit Teaching: Courses I Teach': (client) => {
//   const editCourses = client.page.edityourprofile_coursesITeach();
//   const save = client.page.edityourprofile_save_button();
//   const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
//   const cancel = client.page.edityourprofile_cancel_button();
//   const close = client.page.edityourprofile_close_button();
//   const links = client.page.edityourprofile_links();
//   links.editTeaching();
//   client.pause(2000);
//   editCourses.editTeaCourses();
//   client.pause(2000);
// // Adding Courses I Teach: all buttons tested
//   // Cancel button
//   editCourses.editTeaCoursesAdd();
//   client.pause(2000);
//   client.frame(0);
//   client.pause(2000);
//   cancel.editProfileCancelButton();
//   client.pause(5000);
//   client.frame(null);
//   client.pause(2000);
//   // Save and SaveAndAddAnother buttons
//           // Input #1/2
//           editCourses.editTeaCoursesAdd();
//           client.pause(2000);
//           client.frame(0);
//           client.pause(2000);
//           editCourses.editTeaCoursesAddContentOne();
//           client.pause(2000);
//           saveAndAdd.editProfileSaveAndAddAnother();
//           client.pause(5000);
//           editCourses.editTeaCoursesAddContentTwo();
//           client.pause(2000);
//           // DELETE NEXT 4 ROWS AFTER DEFECT IS GONE
//           save.editProfileSave();
//           client.pause(2000);
//           client.frame(null);
//           client.pause(2000);

//   // Close Button
//   editCourses.editTeaCoursesAdd();
//   client.pause(2000);
//   close.editProfileClose();
//   client.pause(2000);
//      editCourses.editTeaCoursesShowHide();
//      client.pause(2000);
//      client.frame(0);
//      client.pause(2000);
//      editCourses.editTeaCoursesShowHideSelect();
//      client.pause(2000);
//      cancel.editProfileCancelButton();
//      client.pause(2000);
//      client.frame(null);
//      client.pause(2000);
//      for (var i = 0; i <2; i++) {
//         client.elements('css selector', editCourses.elements.removeOneCoursesButton.selector, (isPresent) => {
//           if(isPresent.value.length > 0){
//             editCourses.removeOneCourses();
//             client.pause(2000);
//             client.frame(0);
//             save.editProfileSave();
//             client.pause(2000);
//             client.frame(null);
//           }
//         });
//     }
//     client.pause(2000);
// // Sorting Courses I Teach
// // Showing/hiding Courses I Teach
// },
'Edit Teaching: Teaching Philosophy': (client) => {
  const editPhil = client.page.edityourprofile_teachingPhilosophy();
  const save = client.page.edityourprofile_save_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editTeaching();
  client.pause(2000);
  editPhil.editTeaPhilosophy();
  client.pause(2000);
// Editing new Teaching Philosophy: all buttons tested
  editPhil.editTeaPhilosophyAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  client.waitForElementVisible('#cke_paragraph');
  client.moveToElement('#cke_paragraph',50,100);
  client.mouseButtonClick('left')
  client.keys('Lorem ipsum dolor sit amet');
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editPhil.editTeaPhilosophyAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  client.waitForElementVisible('#cke_paragraph');
  client.moveToElement('#cke_paragraph',50,100);
  client.mouseButtonClick('left')
  client.keys('Lorem ipsum dolor sit amet');
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editPhil.editTeaPhilosophyAdd();
  close.editProfileClose();
  // Deleting Teaching Philosophy: all buttons tested
    // editPhil.editTeaPhilosophyDelete();
    // client.pause(2000);
    // client.frame(0);
    // client.pause(2000);
    // cancel.editProfileCancelButton();
    // client.pause(5000);
    // client.frame(null);
    // client.pause(2000);
    // editPhil.editTeaPhilosophyDelete();
    // client.pause(2000);
    // close.editProfileClose();
    // client.pause(2000);
    // DELETING
    // editPhil.editTeaPhilosophyDelete();
    // client.pause(2000);
    // client.frame(0);
    // client.pause(2000);
    // save.editProfileSave();
    // client.pause(2000);
    // client.frame(null);
    // client.pause(2000);
},
'Edit Teaching: Pedagogical Publications': (client) => {
  const editPedagog = client.page.edityourprofile_pedagogicalPublications();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editTeaching();
  client.pause(2000);
  editPedagog.editTeaPedagogical();
  client.pause(2000);
// Adding Pedagogical Publications: all buttons tested
  editPedagog.editTeaPedagogicalAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  editPedagog.editTeaPedagogicalAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editPedagog.editTeaPedagogicalAddContent();
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editPedagog.editTeaPedagogicalAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
     editPedagog.editTeaPedagogicalShowHide();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     editPedagog.editTeaPedagogicalShowHideSelect();
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editPedagog.editTeaPedagogicalDelete();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editPedagog.editTeaPedagogicalDelete();
     client.pause(2000);
     close.editProfileClose();
     client.pause(2000);
     editPedagog.editTeaPedagogicalDelete();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     save.editProfileSave();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
// Sorting Pedagogical Publications
// Showing/hiding Pedagogical Publications
},
'Edit Teaching: Scholarly Projects': (client) => {
  const editSholarly = client.page.edityourprofile_scholarlyProjects();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editTeaching();
  client.pause(2000);
  editSholarly.editTeaScholarly();
  client.pause(2000);
// Adding Scholarly Projects: all buttons tested
  editSholarly.editTeaScholarlyAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editSholarly.editTeaScholarlyCancel();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  editSholarly.editTeaScholarlyAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editSholarly.editTeaScholarlyAddContent();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editSholarly.editTeaScholarlyAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editSholarly.editTeaScholarlyAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
     editSholarly.editTeaScholarlyShowHide();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     editSholarly.editTeaScholarlyShowHideSelect();
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editSholarly.removeOneScholarly();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editSholarly.removeOneScholarly();
     client.pause(2000);
     close.editProfileClose();
     client.pause(2000);
     for (var i = 0; i <2; i++) {
        client.elements('css selector', editSholarly.elements.removeOneScholarlyButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editSholarly.removeOneScholarly();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting Scholarly Projects
// Showing/hiding Scholarly Projects
},
'Edit Teaching: Teaching Projects': (client) => {
  const editTeachingProjects = client.page.edityourprofile_teachingProjects();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editTeaching();
  client.pause(2000);
  editTeachingProjects.editTeaTeachingProjects();
  client.pause(2000);
// Adding Teaching Projects: all buttons tested
  // Cancel button
  editTeachingProjects.editTeaTeachingProjectsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editTeachingProjects.editTeaTeachingProjectsCancel();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  // INPUTS
  editTeachingProjects.editTeaTeachingProjectsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editTeachingProjects.editTeaTeachingProjectsAddContent();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editTeachingProjects.editTeaTeachingProjectsAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  //Close button
  editTeachingProjects.editTeaTeachingProjectsAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editTeachingProjects.editTeaTeachingProjectsShowHide();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editTeachingProjects.editTeaTeachingProjectsShowHideSelect();
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editTeachingProjects.removeOneTeachingProjects();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editTeachingProjects.removeOneTeachingProjects();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  for (var i = 0; i <2; i++) {
        client.elements('css selector', editTeachingProjects.elements.removeOneTeachingProjectsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editTeachingProjects.removeOneTeachingProjects();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);

// Sorting Teaching Projects
// Showing/hiding Teaching Projects
},
// 'Edit Teaching: Professional Education': (client) => {
//   const editProfessionalEducation = client.page.edityourprofile_professionalEducation();
//   const save = client.page.edityourprofile_save_button();
//   const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
//   const close = client.page.edityourprofile_close_button();
//   const links = client.page.edityourprofile_links();
//   links.editTeaching();
//   client.pause(2000);
//   editProfessionalEducation.editTeaProfessionalEducation();
//   client.pause(2000);
// // Adding Professional Education: all buttons tested
//   // Cancel button
//   editProfessionalEducation.editTeaProfessionalEducationAdd();
//   client.pause(2000);
//   client.frame(0);
//   client.pause(2000);
//   editProfessionalEducation.editTeaProfessionalEducationCancel();
//   client.pause(5000);
//   client.frame(null);
//   client.pause(2000);
//   // INPUTS
//   editProfessionalEducation.editTeaProfessionalEducationAdd();
//   client.pause(2000);
//   client.frame(0);
//   client.pause(2000);
//   editProfessionalEducation.editTeaProfessionalEducationAddContent();
//   client.pause(2000);
//   saveAndAdd.editProfileSaveAndAddAnother();
//   client.pause(5000);
//   editProfessionalEducation.editTeaProfessionalEducationAddContent();
//   save.editProfileSave();
//   // AFTER DEFECTS ARE RESOLVED: UNCOMMENT PREVIOUS 4 ROWS AND DELETE NEXT 1 ROW
//   client.frame(null);
//   client.pause(2000);
//   //Close button
//   editProfessionalEducation.editTeaProfessionalEducationAdd();
//   client.pause(2000);
//   close.editProfileClose();
//   client.pause(2000);
// // Sorting Professional Education
// // Showing/hiding Professional Education
// },
'Edit Teaching: Other Teaching': (client) => {
  const editTeaOther = client.page.edityourprofile_otherTeaching();
  const save = client.page.edityourprofile_save_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editTeaching();
  client.pause(2000);
  editTeaOther.editTeaOther();
  client.pause(2000);
// Editing new Other Teaching: all buttons tested
//   Save button
  editTeaOther.editTeaOtherAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  editTeaOther.editTeaOtherAddContent();
  client.waitForElementVisible('#cke_paragraph');
  client.moveToElement('#cke_paragraph',50,100);
  client.mouseButtonClick('left')
  client.keys('Other Profile Data for Teaching: Lorem ipsum dolor sit amet.');
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  // Cancel and Close buttons
  editTeaOther.editTeaOtherAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  editTeaOther.editTeaOtherCancel();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editTeaOther.editTeaOtherAdd();
  close.editProfileClose();
  client.pause(2000);
  editTeaOther.editTeaOtherDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editTeaOther.editTeaOtherCancel();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editTeaOther.editTeaOtherDelete();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editTeaOther.editTeaOtherDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
// Delete button UNFINISHED!!!
},
'Edit Research: Research Statement': (client) => {
  const editResStatement = client.page.edityourprofile_researchStatement();
  const save = client.page.edityourprofile_save_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editResStatement.editResStatement();
  client.pause(2000);
// Editing new Research Statement: all buttons tested
  // Save button
  editResStatement.editResStatementAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  client.waitForElementVisible('#cke_paragraph');
  client.moveToElement('#cke_paragraph',50,100);
  client.mouseButtonClick('left')
  client.keys('Research Statement for Research: Lorem ipsum dolor sit amet.');
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  // Cancel and Close buttons
  editResStatement.editResStatementAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  editResStatement.editResStatementCancel();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editResStatement.editResStatementAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  // Delete button
  editResStatement.editResStatementDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editResStatement.editResStatementCancel();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editResStatement.editResStatementDelete();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editResStatement.editResStatementDelete();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
},
'Edit Research: Research Summary': (client) => {
  const editResSummary = client.page.edityourprofile_researchSummary();
  const save = client.page.edityourprofile_save_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editResSummary.editResSummary();
  client.pause(2000);
// Editing new Research Summary: all buttons tested
  // Save button
  editResSummary.editResSummaryAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  editResSummary.editResSummaryAddContent();
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  // Cancel and Close buttons
  editResSummary.editResSummaryAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  editResSummary.editResSummaryCancel();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editResSummary.editResSummaryAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  // Delete button
  editResSummary.editResSummaryDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editResSummary.editResSummaryCancel();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editResSummary.editResSummaryDelete();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editResSummary.editResSummaryDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
},
'Edit Research: Area Of Expertise': (client) => {
  const editResExpertise = client.page.edityourprofile_areaOfExpertise();
  const save = client.page.edityourprofile_save_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editResExpertise.editResExpertise();
  client.pause(2000);
// Editing new Area Of Expertise: all buttons tested
  // Save button
  editResExpertise.editResExpertiseAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  editResExpertise.editResExpertiseAddContent();
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  // Cancel and Close buttons
  editResExpertise.editResExpertiseAdd();
  client.pause(4000);
  client.frame(0);
  client.pause(4000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editResExpertise.editResExpertiseAdd();
  close.editProfileClose();
  client.pause(2000);
     editResExpertise.editResExpertiseShowHide();
     client.pause(4000);
     client.frame(0);
     client.pause(4000);
     editResExpertise.editResExpertiseShowHideSelect();
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editResExpertise.editResExpertiseDelete();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editResExpertise.editResExpertiseDelete();
     client.pause(2000);
     close.editProfileClose();    
     client.pause(2000);
     editResExpertise.editResExpertiseDelete();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     save.editProfileSave();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
},
'Edit Research: Grants, Contracts and Research Gifts': (client) => {
  const editGrants = client.page.edityourprofile_grantsContractsGifts();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editGrants.editResGrants();
  client.pause(2000);
// Adding  Grants, Contracts and Research Gifts: all buttons tested
  // Cancel button
  editGrants.editResGrantsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  // INPUTS
  editGrants.editResGrantsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editGrants.editResGrantsAddContent();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editGrants.editResGrantsAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  //Close button
  editGrants.editResGrantsAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
     editGrants.editResGrantsShowHide();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     editGrants.editResGrantsShowHideSelect();
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editGrants.removeOneGrants();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editGrants.removeOneGrants();
     client.pause(2000);
     close.editProfileClose();    
     client.pause(2000);
     for (var i = 0; i <2; i++) {
        client.elements('css selector', editGrants.elements.removeOneGrantsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editGrants.removeOneGrants();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting  Grants, Contracts and Research Gifts
// Showing/hiding  Grants, Contracts and Research Gifts
},
'Edit Research: Research, Scholarship Projects': (client) => {
  const editResResearch = client.page.edityourprofile_researchScholarshipProjects();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editResResearch.editResResearch();
  client.pause(2000);
// Adding  Research, Scholarship Projects
  // Cancel button
  editResResearch.editResResearchAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  // INPUTS
  editResResearch.editResResearchAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editResResearch.editResResearchAddContent();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editResResearch.editResResearchAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  //Close button
  editResResearch.editResResearchAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
     editResResearch.editResResearchShowHide();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     editResResearch.editResResearchShowHideSelect();
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editResResearch.removeOneResearch();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editResResearch.removeOneResearch();
     client.pause(2000);
     close.editProfileClose();    
     client.pause(2000);
     for (var i = 0; i <2; i++) {
        client.elements('css selector', editResResearch.elements.removeOneResearchButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editResResearch.removeOneResearch();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting  Research, Scholarship Projects
// Showing/hiding  Research, Scholarship Projects
},
'Edit Research: Research Groups': (client) => {
  const editResGroups = client.page.edityourprofile_researchGroups();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editResGroups.editResResearchGroups();
  client.pause(2000);
// Adding  Research Groups
  // Cancel button
  editResGroups.editResResearchGroupsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  // INPUTS
  editResGroups.editResResearchGroupsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editResGroups.editResResearchGroupsAddContent();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editResGroups.editResResearchGroupsAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  //Close button
  editResGroups.editResResearchGroupsAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
     editResGroups.editResResearchGroupsShowHide();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     editResGroups.editResResearchGroupsShowHideSelect();
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editResGroups.removeOneResearchGroups();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
     editResGroups.removeOneResearchGroups();
     client.pause(2000);
     close.editProfileClose();    
     client.pause(2000);
     for (var i = 0; i <2; i++) {
        client.elements('css selector', editResGroups.elements.removeOneResearchGroupsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editResGroups.removeOneResearchGroups();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting  Research Groups
// Showing/hiding  Research Groups
},
'Edit Research: Equipment and Testing': (client) => {
  const editResEquipment = client.page.edityourprofile_equipmentAndTesting();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editResEquipment.editResEquipment();
  client.pause(2000);
// Adding  Equipment and Testing
  // Cancel button
  editResEquipment.editResEquipmentAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  // INPUTS
  editResEquipment.editResEquipmentAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editResEquipment.editResEquipmentAddContent();
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  //Close button
  editResEquipment.editResEquipmentAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
  editResEquipment.editResEquipmentDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editResEquipment.editResEquipmentDelete();
  client.pause(2000);
  close.editProfileClose();    
  client.pause(2000);
  editResEquipment.editResEquipmentDelete();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
// Sorting  Equipment and Testing
// Showing/hiding  Equipment and Testing
},
'Edit Research: Patents': (client) => {
  const editResPatents = client.page.edityourprofile_patents();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editResPatents.editResResearchPatents();
  client.pause(2000);
// Adding  Patents
  // Cancel button
  editResPatents.editResResearchPatentsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  // INPUTS
  editResPatents.editResResearchPatentsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editResPatents.editResResearchPatentsAddContent();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editResPatents.editResResearchPatentsAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  //Close button
  editResPatents.editResResearchPatentsAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
     editResPatents.editResResearchPatentsShowHide();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     editResPatents.editResResearchPatentsShowHideSelect();
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
  editResPatents.removeOneResearchPatents();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editResPatents.removeOneResearchPatents();
  client.pause(2000);
  close.editProfileClose();    
  client.pause(2000);
     for (var i = 0; i <2; i++) {
        client.elements('css selector', editResPatents.elements.removeOneResearchPatentsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editResPatents.removeOneResearchPatents();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting  Patents
// Showing/hiding Patents
  },
'Edit Research: Software Titles': (client) => {
  const editResSoftware = client.page.edityourprofile_softwareTitles();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editResSoftware.editResResearchSoftwareTitles();
  client.pause(2000);
// Adding  Software Titles
  // Cancel button
  editResSoftware.editResResearchSoftwareTitlesAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  // INPUTS
  editResSoftware.editResResearchSoftwareTitlesAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editResSoftware.editResResearchSoftwareTitlesAddContent();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editResSoftware.editResResearchSoftwareTitlesAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  //Close button
  editResSoftware.editResResearchSoftwareTitlesAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
     editResSoftware.editResResearchSoftwareTitlesShowHide();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     editResSoftware.editResResearchSoftwareTitlesShowHideSelect();
     client.pause(2000);
     save.editProfileSave();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
  editResSoftware.removeOneResearchSoftwareTitles();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editResSoftware.removeOneResearchSoftwareTitles();
  client.pause(2000);
  close.editProfileClose();    
  client.pause(2000);
     for (var i = 0; i <2; i++) {
        client.elements('css selector', editResSoftware.elements.removeOneResearchSoftwareTitlesButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editResSoftware.removeOneResearchSoftwareTitles();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting  Software Titles
// Showing/hiding Software Titles
  },
'Edit Research: Copyrights': (client) => {
  const editResCopyrights = client.page.edityourprofile_copyrights();
  const save = client.page.edityourprofile_save_button();
  const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
  const cancel = client.page.edityourprofile_cancel_button();
  const close = client.page.edityourprofile_close_button();
  const links = client.page.edityourprofile_links();
  links.editResearch();
  client.pause(2000);
  editResCopyrights.ResCopyrights();
  client.pause(2000);
// Adding  Copyrights
  // Cancel button
  editResCopyrights.ResCopyrightsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(5000);
  client.frame(null);
  client.pause(2000);
  // INPUTS
  editResCopyrights.ResCopyrightsAdd();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  editResCopyrights.ResCopyrightsAddContent();
  client.pause(2000);
  saveAndAdd.editProfileSaveAndAddAnother();
  client.pause(5000);
  editResCopyrights.ResCopyrightsAddContent();
  save.editProfileSave();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  //Close button
  editResCopyrights.ResCopyrightsAdd();
  client.pause(2000);
  close.editProfileClose();
  client.pause(2000);
     editResCopyrights.ResCopyrightsShowHide();
     client.pause(2000);
     client.frame(0);
     client.pause(2000);
     editResCopyrights.ResCopyrightsShowHideSelect();
     client.pause(2000);
     cancel.editProfileCancelButton();
     client.pause(2000);
     client.frame(null);
     client.pause(2000);
  editResCopyrights.removeOneCopyrights();
  client.pause(2000);
  client.frame(0);
  client.pause(2000);
  cancel.editProfileCancelButton();
  client.pause(2000);
  client.frame(null);
  client.pause(2000);
  editResCopyrights.removeOneCopyrights();
  client.pause(2000);
  close.editProfileClose();    
  client.pause(2000);
     for (var i = 0; i <2; i++) {
        client.elements('css selector', editResCopyrights.elements.removeOneCopyrightsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editResCopyrights.removeOneCopyrights();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
// Sorting  Copyrights
// Showing/hiding Copyrights
  },
  'Edit Research: Creative Works': (client) => {
    const editResCreative = client.page.edityourprofile_creative();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editResearch();
    client.pause(2000);
    editResCreative.ResCreative();
    client.pause(2000);
  //Adding  Creative Works
    //Cancel button
    editResCreative.ResCreativeAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    // INPUTS
    editResCreative.ResCreativeAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editResCreative.ResCreativeAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editResCreative.ResCreativeAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    //Close button
    editResCreative.ResCreativeAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editResCreative.ResCreativeShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editResCreative.ResCreativeShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editResCreative.removeOneCreative();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editResCreative.removeOneCreative();
    client.pause(2000);
    close.editProfileClose();    
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editResCreative.elements.removeOneCreativeButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editResCreative.removeOneCreative();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
  // Sorting  Creative Works
  // Showing/hiding Creative Works
    },
  'Edit Research: Other Research': (client) => {
    const editResOther = client.page.edityourprofile_otherResearch();
    const save = client.page.edityourprofile_save_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editResearch();
    client.pause(2000);
    editResOther.editResOther();
    client.pause(2000);
  // Editing new Research: Other: all buttons tested
    // Save button
    editResOther.editResOtherAdd();
    client.pause(4000);
    client.frame(0);
    client.pause(4000);
    client.waitForElementVisible('#cke_paragraph');
    client.moveToElement('#cke_paragraph',50,100);
    client.mouseButtonClick('left')
    client.keys('Research Statement for Research: Lorem ipsum dolor sit amet.');
    client.pause(2000);
    editResOther.editResOtherAddContent();
    client.pause(2000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    // Cancel and Close buttons
    editResOther.editResOtherAdd();
    client.pause(4000);
    client.frame(0);
    client.pause(4000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editResOther.editResOtherAdd();
    close.editProfileClose();
    client.pause(2000);
    // Delete button
    editResOther.editResOtherDelete();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editResOther.editResOtherDelete();
    client.pause(2000);
    close.editProfileClose();    
    client.pause(2000);
    editResOther.editResOtherDelete();
    client.pause(4000);
    client.frame(0);
    client.pause(4000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
   },
  'Edit Publications: Publications': (client) => {
    const editPublicationss = client.page.edityourprofile_publications();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editPublications();
    client.pause(2000);
  //Adding  Publications
    //Cancel button
    editPublicationss.editPublicationsAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    // INPUTS
    editPublicationss.editPublicationsAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editPublicationss.editPublicationsAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editPublicationss.editPublicationsAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    //Close button
    editPublicationss.editPublicationsAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editPublicationss.editPublicationsShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editPublicationss.editPublicationsShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editPublicationss.removeOnePublications();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editPublicationss.removeOnePublications();
    client.pause(2000);
    close.editProfileClose();    
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editPublicationss.elements.removeOnePublicationsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editPublicationss.removeOnePublications();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
  //Sorting  Publications
  //Showing/hiding Publications
    },
  'Edit Presentations: Presentations': (client) => {
    const editPresentations = client.page.edityourprofile_presentations();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editPresentations();
    client.pause(2000);
  //Adding  Presentations
    //Cancel button
    editPresentations.editPresentationsAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    // INPUTS
    editPresentations.editPresentationsAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editPresentations.editPresentationsAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editPresentations.editPresentationsAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    //Close button
    editPresentations.editPresentationsAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editPresentations.editPresentationsShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editPresentations.editPresentationsShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editPresentations.removeOnePresentations();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editPresentations.removeOnePresentations();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editPresentations.elements.removeOnePresentationsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editPresentations.removeOnePresentations();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
  //Sorting  Presentations
  //Showing/hiding Presentations
    },
  'Edit Faculty: Practice History': (client) => {
    const editFacPracticeHistory = client.page.edityourprofile_practiceHistory();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editFacultyPractice();
    client.pause(2000);
    editFacPracticeHistory.editFacPracticeHistory();
    client.pause(2000);
  // Adding  Practice History
    // Cancel button
    editFacPracticeHistory.editFacPracticeHistoryAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    // INPUTS
    editFacPracticeHistory.editFacPracticeHistoryAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editFacPracticeHistory.editFacPracticeHistoryAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editFacPracticeHistory.editFacPracticeHistoryAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    //Close button
    editFacPracticeHistory.editFacPracticeHistoryAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editFacPracticeHistory.editFacPracticeHistoryShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editFacPracticeHistory.editFacPracticeHistoryShowHideSelect();
    client.pause(2000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editFacPracticeHistory.removeOnePracticeHistory();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editFacPracticeHistory.removeOnePracticeHistory();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editFacPracticeHistory.elements.removeOnePracticeHistoryButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editFacPracticeHistory.removeOnePracticeHistory();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);
  //Sorting  Practice History
  //Showing/hiding Practice History
    },
    'Edit Faculty: Patient/Client Satisfaction': (client) => {
    const editFacPatientSatisfaction = client.page.edityourprofile_patientClientSatisfaction();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editFacultyPractice();
    client.pause(2000);
    editFacPatientSatisfaction.editFacPatientSatisfaction();
    client.pause(2000);
    editFacPatientSatisfaction.editFacPatientSatisfactionAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    editFacPatientSatisfaction.editFacPatientSatisfactionAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editFacPatientSatisfaction.editFacPatientSatisfactionAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editFacPatientSatisfaction.editFacPatientSatisfactionAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editFacPatientSatisfaction.editFacPatientSatisfactionAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editFacPatientSatisfaction.editFacFacPatientSatisfactionShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editFacPatientSatisfaction.editFacFacPatientSatisfactionShowHideSelect();
    client.pause(2000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editFacPatientSatisfaction.removeOnePatientSatisfaction();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editFacPatientSatisfaction.removeOnePatientSatisfaction();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editFacPatientSatisfaction.elements.removeOnePatientSatisfactionButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editFacPatientSatisfaction.removeOnePatientSatisfaction();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);

    },
    'Edit Graduate Students: Current Students': (client) => {
    const editGradCurrentStudents = client.page.edityourprofile_currentStudents();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editGraduateStudents();
    client.pause(2000);
    editGradCurrentStudents.editGradCurrentStudents();
    client.pause(2000);
    editGradCurrentStudents.editGradCurrentStudentsAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    editGradCurrentStudents.editGradCurrentStudentsAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editGradCurrentStudents.editGradCurrentStudentsAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editGradCurrentStudents.editGradCurrentStudentsAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editGradCurrentStudents.editGradCurrentStudentsAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    // editGradCurrentStudents.editGradCurrentStudentsShowHide();
    // client.pause(2000);
    // client.frame(0);
    // client.pause(2000);
    // editGradCurrentStudents.editGradCurrentStudentsShowHideSelect();
    // client.pause(2000);
    // cancel.editProfileCancelButton();
    // client.pause(2000);
    // client.frame(null);
    // client.pause(2000);
    // for (var i = 0; i <2; i++) {
    //     client.elements('css selector', editGradCurrentStudents.elements.removeOneCurrentStudentsButton.selector, (isPresent) => {
    //       if(isPresent.value.length > 0){
    //         editGradCurrentStudents.removeOneCurrentStudents();
    //         client.pause(2000);
    //         client.frame(0);
    //         save.editProfileSave();
    //         client.pause(2000);
    //         client.frame(null);
    //       }
    //     });
    // }
    // client.pause(2000);

    },
    'Edit Graduate Students: Former Students': (client) => {
    const editGradFormerStudents = client.page.edityourprofile_formerStudents();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editGraduateStudents();
    client.pause(2000);
    editGradFormerStudents.editGradFormerStudents();
    client.pause(2000);
    editGradFormerStudents.editGradFormerStudentsAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    editGradFormerStudents.editGradFormerStudentsAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editGradFormerStudents.editGradFormerStudentsAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editGradFormerStudents.editGradFormerStudentsAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editGradFormerStudents.editGradFormerStudentsAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editGradFormerStudents.editGradFormerStudentsShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editGradFormerStudents.editGradFormerStudentsShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editGradFormerStudents.removeOneFormerStudents();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editGradFormerStudents.removeOneFormerStudents();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <4; i++) {
        client.elements('css selector', editGradFormerStudents.elements.removeOneFormerStudentsButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editGradFormerStudents.removeOneFormerStudents();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);

    },
    'Edit Entrepreneurial Experience: Entrepreneurial Experience': (client) => {
    const editEntrepreneurialExperience = client.page.edityourprofile_entrepreneurialExperience();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editEntrepreneurialExperience();
    client.pause(2000);
    editEntrepreneurialExperience.editEntreExperienceAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editEntrepreneurialExperience.editEntreExperienceAddContent();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    editEntrepreneurialExperience.editEntreExperienceAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editEntrepreneurialExperience.editEntreExperienceAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editEntrepreneurialExperience.editEntreExperienceAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editEntrepreneurialExperience.editEntreExperienceAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editEntrepreneurialExperience.editEntreExperienceShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editEntrepreneurialExperience.editEntreExperienceShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editEntrepreneurialExperience.removeOneEntreExpeirence();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editEntrepreneurialExperience.removeOneEntreExpeirence();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editEntrepreneurialExperience.elements.removeOneEntreExpeirenceButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editEntrepreneurialExperience.removeOneEntreExpeirence();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);

    },
    'Edit Service : Proffesional Service': (client) => {
    const editProffesionalService = client.page.edityourprofile_proffesionalService();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editService();
    client.pause(2000);
    editProffesionalService.editProffesionalService();
    client.pause(2000);
    editProffesionalService.editProffesionalServiceAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editProffesionalService.editProffesionalServiceAddContent();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    editProffesionalService.editProffesionalServiceAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editProffesionalService.editProffesionalServiceAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editProffesionalService.editProffesionalServiceAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editProffesionalService.editProffesionalServiceAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editProffesionalService.editProffesionalServiceShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editProffesionalService.editProffesionalServiceShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editProffesionalService.removeOneProffesionalService();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editProffesionalService.removeOneProffesionalService();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editProffesionalService.elements.removeOneProffesionalServiceButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editProffesionalService.removeOneProffesionalService();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);

    },
    'Edit Service : Community Partnership': (client) => {
    const editCommunityPartnership = client.page.edityourprofile_communityPartnership();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editService();
    client.pause(2000);
    editCommunityPartnership.editCommunityPartnership();
    client.pause(2000);
    editCommunityPartnership.editCommunityPartnershipAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editCommunityPartnership.editCommunityPartnershipAddContent();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    editCommunityPartnership.editCommunityPartnershipAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editCommunityPartnership.editCommunityPartnershipAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editCommunityPartnership.editCommunityPartnershipAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editCommunityPartnership.editCommunityPartnershipAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editCommunityPartnership.editCommunityPartnershipShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editCommunityPartnership.editCommunityPartnershipShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editCommunityPartnership.removeOneCommunityPartnership();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editCommunityPartnership.removeOneCommunityPartnership();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editCommunityPartnership.elements.removeOneCommunityPartnershipButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editCommunityPartnership.removeOneCommunityPartnership();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);

    },
     'Edit Service : Mentoring': (client) => {
    const editMentoring = client.page.edityourprofile_mentoring();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editService();
    client.pause(2000);
    editMentoring.editMentoring();
    client.pause(2000);
    editMentoring.editMentoringAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editMentoring.editMentoringAddContent();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    editMentoring.editMentoringAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editMentoring.editMentoringAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editMentoring.editMentoringAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editMentoring.editMentoringAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editMentoring.editMentoringShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editMentoring.editMentoringShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editMentoring.removeOneMentoring();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editMentoring.removeOneMentoring();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editMentoring.elements.removeOneMentoringButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editMentoring.removeOneMentoring();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);

    },
    'Edit Service : Internal service': (client) => {
    const editInternalService = client.page.edityourprofile_internalService();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editService();
    client.pause(2000);
    editInternalService.editInternalService();
    client.pause(2000);
    editInternalService.editInternalServiceAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editInternalService.editInternalServiceAddContent();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    editInternalService.editInternalServiceAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editInternalService.editInternalServiceAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editInternalService.editInternalServiceAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editInternalService.editInternalServiceAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editInternalService.editInternalServiceShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editInternalService.editInternalServiceShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editInternalService.removeOneInternalService();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editInternalService.removeOneInternalService();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editInternalService.elements.removeOneInternalServiceButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editInternalService.removeOneInternalService();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);

    },
    'Edit Service : Outreach': (client) => {
    const editOutreach = client.page.edityourprofile_outreach();
    const save = client.page.edityourprofile_save_button();
    const saveAndAdd = client.page.edityourprofile_saveandaddanother_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editService();
    client.pause(2000);
    editOutreach.editOutreach();
    client.pause(2000);
    editOutreach.editOutreachAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editOutreach.editOutreachAddContent();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(5000);
    client.frame(null);
    client.pause(2000);
    editOutreach.editOutreachAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editOutreach.editOutreachAddContent();
    client.pause(2000);
    saveAndAdd.editProfileSaveAndAddAnother();
    client.pause(5000);
    editOutreach.editOutreachAddContent();
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editOutreach.editOutreachAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editOutreach.editOutreachShowHide();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editOutreach.editOutreachShowHideSelect();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editOutreach.removeOneOutreach();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editOutreach.removeOneOutreach();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    for (var i = 0; i <2; i++) {
        client.elements('css selector', editOutreach.elements.removeOneOutreachButton.selector, (isPresent) => {
          if(isPresent.value.length > 0){
            editOutreach.removeOneOutreach();
            client.pause(2000);
            client.frame(0);
            save.editProfileSave();
            client.pause(2000);
            client.frame(null);
          }
        });
    }
    client.pause(2000);

    },
    'Edit Service: Other Profile Data': (client) => {
    const editServiceOther = client.page.edityourprofile_otherProfileData();
    const save = client.page.edityourprofile_save_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    const links = client.page.edityourprofile_links();
    links.editService();
    client.pause(2000);
    editServiceOther.editServiceOther();
    client.pause(2000);
    editServiceOther.editServiceOtherAdd();
    client.pause(4000);
    client.frame(0);
    client.pause(4000);
    client.waitForElementVisible('#cke_paragraph');
    client.moveToElement('#cke_paragraph',50,100);
    client.mouseButtonClick('left')
    client.keys('Service Other Profile Data: Lorem ipsum dolor sit amet.');
    client.pause(2000);
    editServiceOther.editServiceOtherAddContent();
    client.pause(2000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editServiceOther.editServiceOtherAdd();
    client.pause(4000);
    client.frame(0);
    client.pause(4000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editServiceOther.editServiceOtherAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editServiceOther.editServiceOtherDelete();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editServiceOther.editServiceOtherDelete();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editServiceOther.editServiceOtherDelete();
    client.pause(4000);
    client.frame(0);
    client.pause(4000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
   },
  //  'Edit Contact Information: Main Office': (client) => {
  //   const editMainOffice = client.page.edityourprofile_mainOffice();
  //   const save = client.page.edityourprofile_save_button();
  //   const cancel = client.page.edityourprofile_cancel_button();
  //   const close = client.page.edityourprofile_close_button();
  //   const links = client.page.edityourprofile_links();
  //   links.editContactInformation();
  //   client.pause(2000);
  //   editMainOffice.editMainOffice();
  //   client.pause(2000);
  //   editMainOffice.editMainOfficeAdd();
  //   client.pause(2000);
  //   client.frame(0);
  //   client.pause(2000);
  //   editMainOffice.editMainOfficeAddContent();
  //   client.pause(2000);
  //   save.editProfileSave();
  //   client.pause(2000);
  //   client.frame(null);
  //   client.pause(2000);
  //   editMainOffice.editMainOfficeAdd();
  //   client.pause(4000);
  //   client.frame(0);
  //   client.pause(4000);
  //   editMainOffice.editMainOfficeAddContent();
  //   client.pause(2000);
  //   cancel.editProfileCancelButton();
  //   client.pause(2000);
  //   client.frame(null);
  //   client.pause(2000);
  //   editMainOffice.editMainOfficeAdd();
  //   client.pause(2000);
  //   close.editProfileClose();
  //   client.pause(2000);
  //  },
   'Edit more links': (client) => {
    const editMoreLinks = client.page.edityourprofile_moreLinks();
    const save = client.page.edityourprofile_save_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    editMoreLinks.editMoreLinksAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editMoreLinks.editMoreLinksAddContent();
    client.pause(2000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editMoreLinks.editMoreLinksAdd();
    client.pause(4000);
    client.frame(0);
    client.pause(4000);
    editMoreLinks.editMoreLinksAddContent();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editMoreLinks.editMoreLinksAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editMoreLinks.editMoreLinksDelete();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editMoreLinks.editMoreLinksDelete();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editMoreLinks.editMoreLinksDelete();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
   },
   'Edit social media': (client) => {
    const editSocialMedia = client.page.edityourprofile_socialMedia();
    const save = client.page.edityourprofile_save_button();
    const cancel = client.page.edityourprofile_cancel_button();
    const close = client.page.edityourprofile_close_button();
    editSocialMedia.editSocialMediaAdd();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    editSocialMedia.editSocialMediaAddContent();
    client.pause(2000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editSocialMedia.editSocialMediaAdd();
    client.pause(4000);
    client.frame(0);
    client.pause(4000);
    editSocialMedia.editSocialMediaAddContent();
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editSocialMedia.editSocialMediaAdd();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editSocialMedia.editSocialMediaDelete();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    cancel.editProfileCancelButton();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
    editSocialMedia.editSocialMediaDelete();
    client.pause(2000);
    close.editProfileClose();
    client.pause(2000);
    editSocialMedia.editSocialMediaDelete();
    client.pause(2000);
    client.frame(0);
    client.pause(2000);
    save.editProfileSave();
    client.pause(2000);
    client.frame(null);
    client.pause(2000);
   },
    'End': (client) => {
        client.end();
    }
};
