var config = require('../tests.config');
module.exports = {
  tags: ['facultysummary'],
    'Login page': (client) => {
      client.maximizeWindow();
      const loginToFacultySumary = client.page.facultysummary_login();
      loginToFacultySumary.navigate();
        loginToFacultySumary.loginToFaculty(config.username, config.password);
        client.pause(2000);
    },
    'Faculty search': (client) => {
      const facultySearch = client.page.facultysummary_facultySearch();
        facultySearch.facultySummaryAddContent();
        client.pause(2000);
    },
    'Academic summary':(client) => {
      const academicSummary=client.page.facultysummary_academicSummary();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      academicSummary.facultyAcademicSummary();
      client.pause(2000);
      academicSummary.facultyAcademicSummaryAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
    },
    'Addresses & Contacts':(client) => {
      const addressesContacts=client.page.facultysummary_addressesContacts();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      addressesContacts.facultyAddressesContacts();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddAddress();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddAddressContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddAddress();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddAddressContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddContact();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddEmailContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddContact();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddPhoneContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddContact();
      client.pause(2000);
      addressesContacts.facultyAddressesContactsAddMobileContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      for(var i=0;i<4;i++){
        client.elements('css selector', addressesContacts.elements.removeOneAddressesContactsButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              addressesContacts.removeOneAddressesContacts();
              client.pause(2000).acceptAlert();
            }
        });
      }

    },
    'Administrative titles':(client) => {
      const administrativeTitles=client.page.facultysummary_administrativeTiltes();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      administrativeTitles.facultyAdministrativeTitles();
      client.pause(2000);
      administrativeTitles.facultyAdministrativeTitlesAdd();
      client.pause(2000);
      administrativeTitles.facultyAdministrativeTitlesAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      administrativeTitles.facultyAdministrativeTitlesAdd();
      client.pause(2000);
      administrativeTitles.facultyAdministrativeTitlesAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      administrativeTitles.removeOneAdministrativeTitles();
      client.pause(2000).acceptAlert();
      client.pause(2000);
    },
    'Employer ':(client) => {
      const employer=client.page.facultysummary_employer();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      employer.facultyEmployer();
      client.pause(2000);
      employer.facultyEmployerAdd();
      client.pause(2000);
      employer.facultyEmployerAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      employer.facultyEmployerAdd();
      client.pause(2000);
      employer.facultyEmployerAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      employer.removeOneEmployer();
      client.pause(2000).acceptAlert();
      client.pause(2000);
    },
    'Appointments ':(client) => {
      const appointments=client.page.facultysummary_appointments();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      appointments.facultyAppointments();
      client.pause(2000);
      appointments.facultyAppointmentsAdd();
      client.pause(2000);
      appointments.facultyAppointmentsAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      appointments.facultyAppointmentsAdd();
      client.pause(2000);
      appointments.facultyAppointmentsAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      appointments.facultyAppointmentsExpand();
      client.pause(2000);
      appointments.facultyAppointmentsColapse();
      client.pause(2000);
      appointments.facultyAppointmentsExpand();
      client.pause(2000);
      appointments.removeOneAppointments();
      client.pause(2000).acceptAlert();
      client.pause(2000);
    },
    'Boards Specialty ':(client) => {
      const boardsSpecialty=client.page.facultysummary_boardsSpecialty();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      boardsSpecialty.facultyBoards();
      client.pause(2000);
      boardsSpecialty.facultyBoardsAdd();
      client.pause(2000);
      boardsSpecialty.facultyBoardsAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      boardsSpecialty.facultyBoardsAdd();
      client.pause(2000);
      boardsSpecialty.facultyBoardsAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      boardsSpecialty.removeOneBoards();
      client.pause(2000).acceptAlert();
      client.pause(2000);
    },
    'Compensation ':(client) => {
      const compensation=client.page.facultysummary_compensation();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      compensation.facultyCompensation();
      client.pause(2000);
      compensation.facultyCompensationAdd();
      client.pause(2000);
      compensation.facultyCompensationAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      compensation.facultyCompensationAdd();
      client.pause(2000);
      compensation.facultyCompensationAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      compensation.removeOneCompensation();
      client.pause(2000).acceptAlert();
      client.pause(2000);
    },
    'Membership ':(client) => {
      const memberships=client.page.facultysummary_memberships();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      memberships.facultyMemberships();
      client.pause(2000);
      memberships.facultyMembershipsAdd();
      client.pause(2000);
      memberships.facultyMembershipsAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      memberships.facultyMembershipsAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      memberships.facultyMembershipsAdd();
      client.pause(2000);
      memberships.facultyMembershipsAddContent();
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', memberships.elements.removeOneMembershipButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              memberships.removeOneMembership();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }
    },
    'Demographics ':(client) => {
      const demographics=client.page.facultysummary_demographics();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      demographics.facultyDemographics();
      client.pause(2000);
      demographics.facultyDemographicsAddName();
      client.pause(2000);
      demographics.facultyDemographicsAddContentName();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      demographics.facultyDemographicsBack();
      client.pause(2000);
      demographics.facultyDemographicsNames();
      client.pause(2000);
      demographics.facultyDemographicsAddName();
      client.pause(2000);
      demographics.facultyDemographicsAddContentName();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      demographics.removeOneDemographics();
      client.pause(2000).acceptAlert();
      client.pause(2000);
      demographics.facultyDemographicsBack();
      client.pause(2000);
      demographics.facultyDemographicsEditPersonal();
      client.pause(2000);
      demographics.facultyDemographicsEditPersonalContent();
      cancel.facultySummaryCancel();
      client.pause(2000);
      demographics.facultyDemographicsEditPersonal();
      client.pause(2000);
      demographics.facultyDemographicsEditPersonalContent();
      save.facultySummarySave();
      client.pause(2000);
      demographics.facultyDemographicsEditCitizenship();
      client.pause(2000);
      demographics.facultyDemographicsEditCitizenshipContent();
      client.pause(2000);
      demographics.facultyDemographicsSave();
      client.pause(2000);
      demographics.facultyDemographicsEditCitizenship();
      client.pause(2000);
      demographics.facultyDemographicsEditCitizenshipContent();
      client.pause(2000);
      demographics.facultyDemographicsCancel();
      client.pause(2000);
    },
    'Education ':(client) => {
      const education=client.page.facultysummary_education();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      education.facultyEducation();
      client.pause(2000);
      education.facultyEducationAdd();
      client.pause(2000);
      education.facultyEducationAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      education.facultyEducationAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      education.facultyEducationAdd();
      client.pause(2000);
      education.facultyEducationAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', education.elements.removeOneEducationButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              education.removeOneEducation();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }
      
    },
    'Education transcript ':(client) => {
      const educationTranscript=client.page.facultysummary_educationTranscript();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      educationTranscript.facultyEducation();
      client.pause(2000);
      educationTranscript.facultyEducationAdd();
      client.pause(2000);
      educationTranscript.facultyEducationAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      educationTranscript.facultyEducationAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      educationTranscript.facultyEducationAdd();
      client.pause(2000);
      educationTranscript.facultyEducationAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', educationTranscript.elements.removeOneEducationButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              educationTranscript.removeOneEducation();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }
    },
  'Hospital privileges ':(client) => {
      const hospitalPrivileges=client.page.facultysummary_hospital_privileges();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      hospitalPrivileges.facultyHospitalPrivileges();
      client.pause(2000);
      hospitalPrivileges.facultyHospitalPrivilegesAdd();
      client.pause(2000);
      hospitalPrivileges.facultyHospitalPrivilegesAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      hospitalPrivileges.facultyHospitalPrivilegesAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      hospitalPrivileges.facultyHospitalPrivilegesAdd();
      client.pause(2000);
      hospitalPrivileges.facultyHospitalPrivilegesAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', hospitalPrivileges.elements.removeOneHospitalPrivilegesButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              hospitalPrivileges.removeOneHospitalPrivileges();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }

    },
  'Leadership and Commite Roles ':(client) => {
      const leadershipRoles=client.page.facultysummary_leadership();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      leadershipRoles.facultyLeadershipRoles();
      client.pause(2000);
      leadershipRoles.facultyLeadershipRolesAdd();
      client.pause(2000);
      leadershipRoles.facultyLeadershipRolesAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      leadershipRoles.facultyLeadershipRolesAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      leadershipRoles.facultyLeadershipRolesAdd();
      client.pause(2000);
      leadershipRoles.facultyLeadershipRolesAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', leadershipRoles.elements.removeOneLeadershiRolesButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              leadershipRoles.removeOneLeadershipRoles();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }

    },
    'Areas of Expertise ':(client) => {
      const areaOfExpertise=client.page.facultysummary_areaOfExpertise();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      areaOfExpertise.facultyExpertise();
      client.pause(2000);
      areaOfExpertise.facultyExpertiseAdd();
      client.pause(2000);
      areaOfExpertise.facultyExpertiseAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      areaOfExpertise.facultyExpertiseAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      areaOfExpertise.facultyExpertiseAdd();
      client.pause(2000);
      areaOfExpertise.facultyExpertiseAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', areaOfExpertise.elements.removeOneExpertiseButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              areaOfExpertise.removeOneExpertise();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }

    },
    'Medical Licences ':(client) => {
      const medicalLicenses=client.page.facultysummary_medicalLicenses();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      medicalLicenses.facultyMedicalLicenses();
      client.pause(2000);
      medicalLicenses.facultyMedicalLicensesAdd();
      client.pause(2000);
      medicalLicenses.facultyMedicalLicensesAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      medicalLicenses.facultyMedicalLicensesAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      medicalLicenses.facultyMedicalLicensesAdd();
      client.pause(2000);
      medicalLicenses.facultyMedicalLicensesAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', medicalLicenses.elements.removeOneMedicalLicensesButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              medicalLicenses.removeOneMedicalLicenses();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }

    },
    'Licences and Certificates ':(client) => {
      const licencesCertificates=client.page.facultysummary_licencesAndCertificates();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      licencesCertificates.facultyLicencesCertificates();
      client.pause(2000);
      licencesCertificates.facultyLicencesCertificatesAdd();
      client.pause(2000);
      licencesCertificates.facultyLicencesCertificatesAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      licencesCertificates.facultyLicencesCertificatesAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      licencesCertificates.facultyLicencesCertificatesAdd();
      client.pause(2000);
      licencesCertificates.facultyLicencesCertificatesAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', licencesCertificates.elements.removeOneLicencesCertificatesButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              licencesCertificates.removeOneLicencesCertificates();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }

    },
      'Notes ':(client) => {
      const notes=client.page.facultysummary_notes();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      notes.facultyNotes();
      client.pause(2000);
      notes.facultyNotesAdd();
      client.pause(2000);
      notes.facultyNotesAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      notes.facultyNotesAdd();
      client.pause(2000);
      notes.facultyNotesAddContentUser();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      notes.facultyNotesAdd();
      client.pause(2000);
      notes.facultyNotesAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      notes.facultyNotesFilter();
      client.pause(3000);
      notes.facultyClearSearch();
      client.pause(2000);
      notes.facultyNotesSearch();
      client.pause(3000);
      notes.facultyClearSearch();
      for(var i=0;i<2;i++){
        client.elements('css selector', notes.elements.removeOneNotesButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              notes.removeOneNotes();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }
    },
    'Past Appointments ':(client) => {
      const pastAppointments=client.page.facultysummary_pastAppointments();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      pastAppointments.facultyPastAppointments();
      client.pause(2000);
      pastAppointments.facultyPastAppointmentsAdd();
      client.pause(2000);
      pastAppointments.facultyPastAppointmentsAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      pastAppointments.facultyPastAppointmentsAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      pastAppointments.facultyPastAppointmentsAdd();
      client.pause(2000);
      pastAppointments.facultyPastAppointmentsAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', pastAppointments.elements.removeOnepastAppointmentsButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              pastAppointments.removeOnepastAppointments();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }

    },
    'Significant Accomplishments ':(client) => {
      const significantAccomplishments=client.page.facultysummary_significantAccomplishments();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      significantAccomplishments.facultySignificantAcc();
      client.pause(2000);
      significantAccomplishments.facultySignificantAccAdd();
      client.pause(2000);
      significantAccomplishments.facultySignificantAccAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      significantAccomplishments.facultySignificantAccAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      significantAccomplishments.facultySignificantAccAdd();
      client.pause(2000);
      significantAccomplishments.facultySignificantAccAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', significantAccomplishments.elements.removeOneSignificantAccButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              significantAccomplishments.removeOneSignificantAcc();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }

    },
      'Teaching Activities ':(client) => {
      const teachingActivities=client.page.facultysummary_teachingActivities();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      teachingActivities.facultyTeachingActivities();
      client.pause(2000);
      teachingActivities.facultyTeachingActivitiesAdd();
      client.pause(2000);
      teachingActivities.facultyTeachingActivitiesAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      teachingActivities.facultyTeachingActivitiesAddBanner();
      client.pause(2000);
      teachingActivities.facultyTeachingActivitiesAddBannerContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      teachingActivities.facultyTeachingActivitiesAdd();
      client.pause(2000);
      teachingActivities.facultyTeachingActivitiesAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', teachingActivities.elements.removeOneTeachingButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              teachingActivities.removeOneTeachingActivities();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }
    },
    'User IDs ':(client) => {
      const userIDs=client.page.facultysummary_userIDs();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      userIDs.userIDs();
      client.pause(2000);
      userIDs.facultyUserIDsAdd();
      client.pause(2000);
      userIDs.facultyUserIDsAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      userIDs.facultyUserIDsAdd();
      client.pause(2000);
      userIDs.facultyUserIDsAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
    },
    'Search String ':(client) => {
      const searchString=client.page.facultysummary_searchString();
      const save = client.page.facultysummary_save_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      const cancel = client.page.facultysummary_cancel_button();
      searchString.facultySearchString();
      client.pause(2000);
      searchString.facultySearchStringAdd();
      client.pause(2000);
      searchString.facultySearchStringAddContent();
      client.pause(2000);
      searchString.saveAndAddAnother();
      client.pause(2000);
      searchString.facultySearchStringAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      searchString.facultySearchStringAdd();
      client.pause(2000);
      searchString.facultySearchStringAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', searchString.elements.removeOneSearchStringButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              searchString.removeOneSearchString();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }
    },
    'Demographic Information ':(client) => {
      const demographicInformation=client.page.facultysummary_demographicInformation();
      const save = client.page.facultysummary_save_button();
      demographicInformation.facultyDemographicInformation();
      client.pause(2000);
      demographicInformation.facultyDemographicInformationAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);

    },
    'WMed Service Interests ':(client) => {
      const wmedService=client.page.facultysummary_wmed_service();
      const save = client.page.facultysummary_save_button();
      wmedService.facultyWMedServices();
      client.pause(2000);
      wmedService.facultyWMedServicesAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);

    },
    'Proffesional Experience ':(client) => {
      const proffesionalExperience=client.page.facultySummary_proffesionalExperience();
      const save = client.page.facultysummary_save_button();
      const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
      const cancel = client.page.facultysummary_cancel_button();
      proffesionalExperience.facultyProffesionalExperience();
      client.pause(2000);
      proffesionalExperience.facultyProffesionalExperienceAdd();
      client.pause(2000);
      proffesionalExperience.facultyProffesionalExperienceAddContent();
      client.pause(2000);
      saveAndAdd.facultySaveAndAddAnother();
      client.pause(2000);
      proffesionalExperience.facultyProffesionalExperienceAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      proffesionalExperience.facultyProffesionalExperienceAdd();
      client.pause(2000);
      proffesionalExperience.facultyProffesionalExperienceAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      for(var i=0;i<2;i++){
        client.elements('css selector', proffesionalExperience.elements.removeOneProffesionalExperienceButton.selector, (isPresent) => {
            if(isPresent.value.length > 0){
              proffesionalExperience.removeOneProffesionalExperience();
              client.pause(2000).acceptAlert();
              client.pause(2000);
            }
        });
      }
    save.facultySummarySave();
    client.pause(2000);
    },
    // 'Qualify Faculty ':(client) => {
    //   const qualifyFaculty=client.page.facultysummary_qualifyFaculty();
    //   const save = client.page.facultysummary_save_button();
    //   const saveAndAdd=client.page.facultysummary_saveandaddanother_button();
    //   const cancel = client.page.facultysummary_cancel_button();
    //   qualifyFaculty.facultyQualifyFaculty();
    //   client.pause(2000);
    //   qualifyFaculty.facultyQualifyFacultyAdd();
    //   client.pause(2000);
    //   qualifyFaculty.facultyQualifyFacultyAddContent();
    //   client.pause(2000);
    //   saveAndAdd.facultySaveAndAddAnother();
    //   client.pause(2000);
    //   qualifyFaculty.facultyQualifyFacultyAddContent();
    //   client.pause(2000);
    //   save.facultySummarySave();
    //   client.pause(2000);
    //   qualifyFaculty.facultyQualifyFacultyAdd();
    //   client.pause(2000);
    //   qualifyFaculty.facultyQualifyFacultyAddContent();
    //   client.pause(2000);
    //   cancel.facultySummaryCancel();
    //   client.pause(2000);
    //   save.facultySummarySave();
    // },
    'Publication': (client) => {
      const publication = client.page.facultysummary_publication();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      publication.clickPublication();
      client.pause(2000);
      publication.plusPublication();
      client.pause(2000);
      publication.fillPublication();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      publication.plusPublication();
      client.pause(2000);
      publication.fillPublication();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
      publication.removeOnePublication();
      client.pause(2000).acceptAlert();
      client.pause(2000);
    // save.facultySummarySave();
    },
    'Configurable User IDs ':(client) => {
      const configurableUserIDs=client.page.facultysummary_configurableUserIDs();
      const save = client.page.facultysummary_save_button();
      const cancel = client.page.facultysummary_cancel_button();
      configurableUserIDs.userIDs();
      client.pause(2000);
      configurableUserIDs.facultyUserIDsAdd();
      client.pause(2000);
      configurableUserIDs.facultyUserIDsAddContent();
      client.pause(2000);
      save.facultySummarySave();
      client.pause(2000);
      configurableUserIDs.facultyUserIDsAdd();
      client.pause(2000);
      configurableUserIDs.facultyUserIDsAddContent();
      client.pause(2000);
      cancel.facultySummaryCancel();
      client.pause(2000);
    },
    'End': (client) => {
        client.end();
    }
};
