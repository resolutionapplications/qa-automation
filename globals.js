/* In nightwatch/globals.js */
var HtmlReporter = require('nightwatch-html-reporter');
var reporter = new HtmlReporter({
	openBrowser: true,
	reportsDirectory: __dirname + '/reports',
  themeName: 'cover'
});
module.exports = {
  waitForConditionTimeout: 60000,
	reporter: reporter.fn
};
